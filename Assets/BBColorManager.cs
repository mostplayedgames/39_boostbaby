﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorData
{
    public Color BaseColor;
    public Color CameraColor;
    public Color SideColor;
    public Color ParticleColor;
}

public class BBColorManager : ZSingleton<BBColorManager> {

    public Renderer m_objOutsideFloor;
    public Renderer m_objFloor;
    public Renderer m_objLineFLoorLeft;
    public Renderer m_objLineFloorRight;
    public Renderer m_extremeFloor;

    public Renderer m_objParticleLeft;
    public Renderer m_objParticleRight;
    public List<ColorData> m_listOfColorData = new List<ColorData>();
    public ColorData m_extremeModeColor;
    private int m_curIdx = 0;

    public void ChangeColor()
    {
        if (m_curIdx <= m_listOfColorData.Count -1)
        {
            m_curIdx++;
        }
        else
        {
            return;
        }

        StartCoroutine(ChangeColor(m_curIdx, 1.5f));
    }

    public void ResetGameColor()
    {
        StopCoroutine("ChangeColor");
        m_curIdx = 0;
        Camera.main.backgroundColor = m_listOfColorData[0].CameraColor;
        m_objFloor.material.color = m_listOfColorData[0].BaseColor;
        m_objOutsideFloor.material.color = m_listOfColorData[0].BaseColor;
        m_objLineFLoorLeft.material.color = m_listOfColorData[0].SideColor;
        m_objLineFloorRight.material.color = m_listOfColorData[0].SideColor;
        m_objParticleRight.material.color = m_listOfColorData[0].ParticleColor;
        m_objParticleLeft.material.color = m_listOfColorData[0].ParticleColor;


        //    ParticleSystem.MainModule rightParticle = m_objParticleRight.main;
        //rightParticle.startColor = m_listOfColorData[0].ParticleColor; 

        //    ParticleSystem.MainModule leftParticle = m_objParticleLeft.main;
        //leftParticle.startColor = m_listOfColorData[0].ParticleColor; 
    }

    public void ResetGameColorExtremMode()
    {
        StopCoroutine("ChangeColor"); 
        m_curIdx = 0;
        Camera.main.backgroundColor = m_extremeModeColor.CameraColor;
        m_objFloor.material.color = m_extremeModeColor.BaseColor;
        m_objOutsideFloor.material.color = m_listOfColorData[0].BaseColor;

        m_objLineFLoorLeft.material.color = m_extremeModeColor.SideColor;
        m_objLineFloorRight.material.color = m_extremeModeColor.SideColor;

        m_objParticleRight.material.color = m_extremeModeColor.ParticleColor;
        m_objParticleLeft.material.color = m_extremeModeColor.ParticleColor;

        //ParticleSystem.MainModule rightParticle = m_objParticleRight.main;
        //rightParticle.startColor = m_extremeModeColor.ParticleColor; 

        //ParticleSystem.MainModule leftParticle = m_objParticleLeft.main;
        //leftParticle.startColor = m_extremeModeColor.ParticleColor; 
    }
	
    private IEnumerator ChangeColor(int p_curIdx, float duration)
    {
        float timeElapsed = 0.0f;

        float t = 0.0f;
        while (t < 1.0f)
        {
            timeElapsed += Time.deltaTime;

            t = timeElapsed / duration;

            int prevIdx = m_curIdx - 1;

            if(prevIdx < 0)
            {
                prevIdx = 0;
            }

            ColorData curData = m_listOfColorData[prevIdx];
            ColorData nextData = m_listOfColorData[m_curIdx];

           // Debug.Log("TmeElapsed: " + t);

            Camera.main.backgroundColor = Color.Lerp(curData.CameraColor, nextData.CameraColor, t);
            m_objFloor.GetComponent<Renderer>().material.color = Color.Lerp(curData.BaseColor, nextData.BaseColor, t);
            m_objOutsideFloor.material.color = Color.Lerp(curData.BaseColor, nextData.BaseColor, t);

            m_objLineFLoorLeft.GetComponent<Renderer>().material.color = Color.Lerp(curData.SideColor, nextData.SideColor, t);
            m_objLineFloorRight.GetComponent<Renderer>().material.color = Color.Lerp(curData.SideColor, nextData.SideColor, t);

            m_objParticleRight.material.color = Color.Lerp(curData.ParticleColor, nextData.ParticleColor, t); 
            m_objParticleLeft.material.color = Color.Lerp(curData.ParticleColor, nextData.ParticleColor, t); 

            //ParticleSystem.MainModule rightParticle = m_objParticleRight.main;
            //rightParticle.startColor = Color.Lerp(curData.ParticleColor, nextData.ParticleColor, t); 

            //ParticleSystem.MainModule leftParticle = m_objParticleLeft.main;
            //leftParticle.startColor = Color.Lerp(curData.ParticleColor, nextData.ParticleColor, t); 
            //m_objParticle.main.startColor = CColor.Lerp(curData.ParticleColor, nextData.ParticleColor, t); olor.Lerp(curData.ParticleColor, nextData.ParticleColor, t); 


            yield return null;
        }
    }
}
