﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandTutorial : MonoBehaviour {

	public GameObject m_objHandTimer;
	public GameObject m_objOnTouchEffect;
	public GameObject m_objTutorialText;
	public Sprite m_spriteOnHold;
	public Sprite m_spriteOnRelease;

	public void OnEnable()
	{
		LeanTween.cancel (m_objOnTouchEffect.gameObject);
		LeanTween.cancel (m_objHandTimer.gameObject);
		Touch ();


	}

	public void Touch()
	{
		m_objHandTimer.GetComponent<Image>().sprite = m_spriteOnRelease;
		m_objOnTouchEffect.transform.localScale = new Vector3 (0.7f, 0.7f, 0.45f);
		m_objOnTouchEffect.gameObject.SetActive (false);
		LeanTween.alpha (m_objHandTimer.gameObject, 1, 0.5f).setOnComplete (Hold);

		//m_objHandTimer.GetComponent<Image>().sprite = m_spriteOnHold;
		//LeanTween.moveLocal (m_objHandTimer.gameObject, m_objHandTimer.transform.localPosition + new Vector3(0, 20, 0), 1).setEase(LeanTweenType.easeInExpo);
		//LeanTween.scaleY (m_objHandTimer.gameObject, m_objHandTimer.transform.localScale.y - 0.06f, 1f).setEase(LeanTweenType.easeInExpo).setOnComplete(Hold);
	}

	public void Hold()
	{
		//LeanTween.alpha (m_objHandTimer.gameObject, 1, 0.5f).setOnComplete (Release);
		m_objOnTouchEffect.gameObject.SetActive (true);
		m_objHandTimer.GetComponent<Image>().sprite = m_spriteOnHold;
		LeanTween.cancel (m_objOnTouchEffect.gameObject);
		LeanTween.scale(m_objOnTouchEffect.gameObject, m_objOnTouchEffect.transform.localScale + new Vector3(0.6f, 0.6f ,0.6f), 1f).setOnComplete(Release);//.setLoopPingPong().setLoopCount(8).setOnComplete(Release);
	}
	public void Release()
	{
		m_objHandTimer.GetComponent<Image>().sprite = m_spriteOnRelease;
		m_objOnTouchEffect.gameObject.SetActive (false);
		m_objOnTouchEffect.transform.localScale = new Vector3 (0.7f, 0.7f, 0.45f);
		LeanTween.alpha (m_objHandTimer.gameObject, 1, 0.5f).setOnComplete (Touch);
		//LeanTween.moveLocal (m_objHandTimer.gameObject, m_objHandTimer.transform.localPosition - new Vector3(0, 20, 0), 1).setEase(LeanTweenType.easeOutExpo);
		//LeanTween.scaleY (m_objHandTimer.gameObject, m_objHandTimer.transform.localScale.y + 0.06f, 1f).setEase (LeanTweenType.easeOutExpo).setOnComplete(Touch);
	}
}
