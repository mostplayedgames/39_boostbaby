﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	TITLE_SCREEN,
    IN_GAME,
	START,
	IDLE,
	SHOP,
	UNLOCK,
	AIMING,
	SHOOT,
	RESULTS,
    WATCH_TO_CONTINUE
}
	
	
public enum ShopBirdFlapAnimType
{
	FLIP,
	FLAPPY,
	REVERSEFLIP,
	FLIPPY,
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectCharacter;
	public Sprite imagePortrait;
	public string m_textCharacterName;
	public Color colorTrailStart;
	public Color colorTrailEnd;
	public Color colorFeathers;
	public bool isUpdateWeatherColors;
	public Color colorScore;
	public Color colorCameraWeather;
	public Color colorSky;
	public bool willFlat;
	public float spinRate;
	public ShopBirdFlapAnimType flipAnimType;
	public int rarity;
}
	
// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;

	public GameObject m_rootUIScore;
	public GameObject m_rootUIBestScore;
	public GameObject m_rootMain;


	public List<ShopBird> m_listShopBirds;

	public List<int> m_listItemsToUnlock;
	public int m_newBottlesToUnlock;

	public GameObject m_objectWatchVideo;
	public GameObject m_objectChangeModeBtn;

	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticlePerfect;

	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTopBar;
	public GameObject m_objectModeIndicator;
	public GameObject m_objectHitParticle;
	public GameObject m_objectEarned;

	//public GameObject m_objectTextTutorial;
	public GameObject m_objectTutorialBanner;

	public GameObject m_objectRootArrows;
	public GameObject m_objectNoInternet;
	public GameObject m_objectReadyUnlock;
	public GameObject m_objectFullBar;
	public GameObject m_objCreditsScreen;
	//public GameObject m_objCreditsBtn;
	//public GameObject m_objHomeBtn;
	public GameObject m_objTitleBall;

	public GameObject m_objectCoinRate;

    public GameObject m_objUnlockBanner;
    public GameObject m_objUnlockText;

    public GameObject m_objTutorial;
    public GameObject m_objLapsCounter;

    public GameObject m_groupBuyCoins;

	public List<GameObject> m_listButtons;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioDieBurn;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioMiss3;
	public AudioClip m_audioMiss4;
	public AudioClip m_audioMiss5;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioCatch;
	public AudioClip m_audioBuy;
	public AudioClip m_audioScore;
	public AudioClip m_audioSpecialScore;
	public AudioClip m_audioCoin;
	public AudioClip m_audioReward;
	public AudioClip m_audioThrow;

	public Text m_textScore;
	public Text m_textScoreShadow;
	public Text m_textLevel;
	public Text m_textHit;
	public Text m_textHit2;
	public Text m_textMiss;
	public Text m_textMessage;
	public Text m_textModes;
	public Text m_textModes2;
	public Text m_textTries2;
	public Text m_textBest;
	public Text m_textPerScore;

	public TextMesh m_textMeshCoin;
	public TextMesh m_textMeshCoinShadow;

	public List<int> m_listPrices;

	public List<string> m_listModes;

	public GameObject m_objectBall;
	public GameObject m_objectBallRoot;
	public GameObject m_objScoreboardScale;

	public GameObject m_objectTutorialArrow;

	public GAME_STATE m_eState;

	public GameObject m_objectCoin;

	public List<Image> m_listOfButtons;

	public List<int> m_listToRandomUnlock;

	public FDCharacter m_scriptCharacter;

	public PhysicsMaterial2D m_bottlePhysicMaterial;
	public GameObject m_objectCharacterRoot;

	public List<FDGamesceneInstance> m_listGamesceneInstance;

	public int m_birdType = 0;

	public ParticleSystem m_particleScore1;
	public ParticleSystem m_particleScore2;

	public GameObject m_objectClaimButton;
    public GameObject m_objWhiteFlash;

	public List<GameObject> m_listiPXAffectedObjects;

	public GameObject m_objBall;
	public GameObject m_objectBox;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public Color m_colorFullBarYellow;
	public Color m_colorFullBarGain;
	public Color m_colorFullBarFull;
	public Color m_colorCOinYellow;

	public AudioClip m_audioAddCoinGain;
	public AudioClip m_audioAddCoinFull;

	public GameObject m_objTitleGroup;
	public GameObject m_objTitleButtons;

	public GameObject m_prefabShadow;
	public GameObject m_prefabHitParticle;
	public GameObject m_prefabBall;
	public GameObject m_groupScore;
	public GameObject m_objShootoutTimer;

	FDGamesceneInstance m_scriptGamesceneInstance;

	int m_isWatchVideo;

	int m_currentHighscore;
	public int m_currentLevel;
	public int m_scoreAdded;
	public int m_currentScore;
	int m_currentAds;
	int m_currentHits;
	int m_currentMode;
	int m_currentItem;
	int m_currentPurchaseCount;
	int m_currentAttempts;
	int m_currentCoinAdd;


	float m_currentIncentifiedAdsTime;
	float m_currentAdsTime;
	string m_stringSuffix = "";



	void Awake()
	{

		instance = this;

		Application.targetFrameRate = 60;

		if (!PlayerPrefs.HasKey ("Hit")) 
		{
			PlayerPrefs.SetInt ("Hit", 0);
		}//PlayerPrefs.SetInt ("Hit", 0);

		if (!PlayerPrefs.HasKey ("Mode")) 
		{
			PlayerPrefs.SetInt ("Mode", 0);
		}

		if (!PlayerPrefs.HasKey ("Removeads")) {
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentBird")) 
		{	
			PlayerPrefs.SetInt ("currentBird", 0);
		}//PlayerPrefs.SetInt ("currentBird", 0);

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) 
		{	
			PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}//PlayerPrefs.SetInt ("currentPurchaseCount", 0);

		if (!PlayerPrefs.HasKey ("attempts")) {
			PlayerPrefs.SetInt ("attempts", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_onetries")) {
			PlayerPrefs.SetInt ("achievement_onetries", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_level")) {
			PlayerPrefs.SetInt ("achievement_level", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_level_count")) {
			PlayerPrefs.SetInt ("achievement_level_count", 0);
		}

		if (!PlayerPrefs.HasKey ("currentUnlockedBird")) 
		{
			PlayerPrefs.SetInt ("currentUnlockedBird", 0);
		}
		//PlayerPrefs.SetInt ("currentUnlockedBird", 0);

		for (int x = 0; x < 24; x++) 
		{
 //           PlayerPrefs.SetInt("birdbought" + x, 1);
			//if (x  < 20) {
			//	PlayerPrefs.SetInt ("birdbought" + x, 1);
			//} else 
   //         {
			//	PlayerPrefs.SetInt ("birdbought" + x, 0);
			//}
									if (x == 0) {
										if (!PlayerPrefs.HasKey ("birdbought" + x)) 
										{
											PlayerPrefs.SetInt ("birdbought" + x, 1);
										}
									} else 
									{
										if (!PlayerPrefs.HasKey ("birdbought" + x)) 
										{
											PlayerPrefs.SetInt ("birdbought" + x, 0);
										}
									}
			//PlayerPrefs.SetInt ("birdbought" + x, 0);
		}

//		PlayerPrefs.SetInt ("birdbought0", 1);

//		for (int x = 0; x < 10; x++) {
//			if (x == 0) {
//				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
//					PlayerPrefs.SetInt ("ballbought" + x, 1);
//				}
//			} else {
//				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
//					PlayerPrefs.SetInt ("ballbought" + x, 0);
//				}
//			}
//		}

		//PlayerPrefs.DeleteAll ();
		

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZAdsMgr.Instance.Init ();
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
		ZNotificationMgr.Instance.enabled = true;

	}

	void Start()
	{
		ZObjectMgr.Instance.AddNewObject (m_prefabBall, 5 , m_prefabBall.name);
		ZObjectMgr.Instance.AddNewObject (m_prefabHitParticle, 20, m_prefabHitParticle.name);
		ZObjectMgr.Instance.AddNewObject (m_prefabShadow, 5, m_prefabShadow.name);

		//ResetPlayerPrefs ();
		if (!PlayerPrefs.HasKey ("Level")) {
			PlayerPrefs.SetInt ("Level", 1);
		} 

//		int f = 0;
//		foreach (LeaderboardData data in ZGameMgr.instance.m_listLeaderboard) {
//			if (!PlayerPrefs.HasKey (data.saveName)){
//				if (m_listGamesceneInstance [f].m_isLevelBased)
//					PlayerPrefs.SetInt (data.saveName, 1);
//				else
//					PlayerPrefs.SetInt (data.saveName, 0);
//			}
//			f++;
//			//PlayerPrefs.SetInt (data.saveName, 1);
//		}
			
		if (PlayerPrefs.GetInt ("Removeads") <= 0) {
			ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);
		}

		for (int x = 0; x < GameScene.instance.m_listModes.Count; x++) 
		{
			if (!PlayerPrefs.HasKey ("Attempts" + x)) {
				PlayerPrefs.SetInt ("Attempts" + x, 0);
			}
		}

		Application.targetFrameRate = 60;

		ZPlatformCenterMgr.Instance.Login ();

		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		m_currentHits = PlayerPrefs.GetInt ("Hit");
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_currentItem = PlayerPrefs.GetInt ("currentBird");
        BBCar.Instance.SetCar(m_currentItem);

		m_currentMode = PlayerPrefs.GetInt ("Mode");
		m_currentIncentifiedAdsTime = 60;
		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];
		m_currentAttempts = m_scriptGamesceneInstance.GetTotalAttempts ();
		m_currentCoinAdd = m_scriptGamesceneInstance.GetBestScore();
		if (m_currentCoinAdd <= 0) {
			m_currentCoinAdd = 1;
		}

		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 5.38f, 1);

		m_objectWatchVideo.SetActive (false);


		int gamesceneCount = 0;
		foreach (FDGamesceneInstance instance in m_listGamesceneInstance) {
			if (gamesceneCount != m_currentMode) {
				instance.gameObject.SetActive (false);
			} else {
				instance.gameObject.SetActive (true);
			}
			gamesceneCount++;
		}
        SpawnerManager.Instance.InitializeManager();

        ShowScore(m_scriptGamesceneInstance.GetBestScore());
		m_scriptGamesceneInstance.Gamescene_Setuplevel ();
		m_eState = GAME_STATE.START;

		#if UNITY_IPHONE
		bool deviceIsIphoneX = UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneX;
		if (deviceIsIphoneX) {
			// Do something for iPhone X
			foreach (GameObject obj in m_listiPXAffectedObjects) {
				obj.transform.localPosition -= new Vector3 (0, 25, 0);
			}
		}
		#endif
		m_currentAds = 2;
		m_isWatchVideo = 0;

		RefreshMode();

		ZAdsMgr.Instance.RequestInterstitial ();
	
		m_currentAdsTime = 35;

        ContinueManager.Instance.noThanksPressed += NoThanksPressed;
        ContinueManager.Instance.continueGamePressed += ContineGamePressed;

        SetupLevel();

        m_eState = GAME_STATE.TITLE_SCREEN;
	}
		
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			if (ShopScene.instance != null)
			{
				if (ShopScene.instance.gameObject.activeSelf) {
					ShopScene.instance.Back ();
				}
			}
			else if (!m_objTitleGroup.activeSelf) {
				ShowTitleScreen ();
			} else if (m_objTitleGroup.activeSelf) {
				Application.Quit ();
			}
		}


		if (Input.GetKeyDown(KeyCode.K)) 
        {
			PlayerPrefs.DeleteAll ();
			ResetPlayerPrefs ();
		}

        if(Input.GetKeyDown(KeyCode.Z))
        {
            AddCoins(100);
        }

		m_currentAdsTime -= Time.deltaTime;
	
		m_currentIncentifiedAdsTime -= Time.deltaTime;

        if (m_eState == GAME_STATE.IN_GAME)
        {
            m_scriptGamesceneInstance.Gamescene_Update();
        }
	}


    private void NoThanksPressed()
    {
        Debug.Log("NoThanks PRessed");
        ContinueManager.Instance.ShowButtons(false);
        BBCar.Instance.m_objPhantomCar.SetActive(false);
        GameScene.instance.Die();
        m_eState = GAME_STATE.RESULTS;
    }

    private void ContineGamePressed()
    {
        GameScene.instance.ContinueWatchAds();

    }

    public void ShowWhiteFlash()
    {
        LeanTween.cancel(m_objWhiteFlash.gameObject);
        LeanTween.scale(m_objWhiteFlash.gameObject, m_objWhiteFlash.transform.localScale, 0.1f).setOnComplete(HideWhiteFlash);
        m_objWhiteFlash.gameObject.SetActive(true);
    }

    public void HideWhiteFlash()
    {
        m_objWhiteFlash.gameObject.SetActive(false);
    }

	public void ButtonDown()
	{
        if (m_eState != GAME_STATE.TITLE_SCREEN)
			return;
		
		m_scriptGamesceneInstance.Gamescene_ButtonDown();

		m_objectTopBar.SetActive (false);



		if (m_textBest.gameObject.activeSelf) 
		{
			m_objectEarned.SetActive (false);
			m_objectNoInternet.SetActive (false);
		}

        m_objectNoInternet.gameObject.SetActive(false);

		m_objectWatchVideo.SetActive (false);

        StartGame();
	}

	public void ButtonUp()
	{
		if (m_eState == GAME_STATE.RESULTS){ // || m_eState == GAME_STATE.TITLE_SCREEN) {
			return;
		}
		m_scriptGamesceneInstance.Gamescene_ButtonUp();
	}

	public int GetCurrentMode()
	{
		return m_currentMode;
	}

    public void GenericButton()
    {
        ZAudioMgr.Instance.PlaySFX(m_audioButton);
    }


	public GameObject m_objectLevelIndicator;
		

	Vector3 m_previousScorePos;
	Vector3 m_previousShadowScorePos;

	bool m_isNewBest = false;

	public List<Color> m_listMessageColors;

	public void ShowMessage(string messageData, int type=0){
		m_textMiss.gameObject.SetActive (true);
		m_textMiss.text = messageData;
		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		m_textMiss.color = m_listMessageColors [type];
		LeanTween.delayedCall (1.5f, HideMessage);

		switch (type) {
		case 2:
			ZAudioMgr.Instance.PlaySFX (m_audioReward);
			break;
		}
	}

	void HideMessage()
	{
		m_textMiss.gameObject.SetActive (false);
	}



	public void Die()
	{
		//if (m_eState == GAME_STATE.RESULTS || m_eState == GAME_STATE.UNLOCK)
			//return;

		//m_textScore.color = m_colorTextMiss;
		m_objectMiss.SetActive (true);
		m_objectWatchVideo.SetActive (false);

		StopCoroutine ("whileTest");
		ZAnalytics.Instance.SendLevelFail ("mode" + m_currentMode, m_currentLevel, m_currentScore);

		int chatvalue = Random.Range (0, 15);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "Miss!";
			break;
		case 1 : 
			m_textMiss.text = "Woops!";
			break;
		case 2 : 
			m_textMiss.text = "Oh no!";
			break;
		case 3 : 
			m_textMiss.text = "No!";
			break;
		case 4 : 
			m_textMiss.text = "Argh!";
			break;
		case 5 : 
			m_textMiss.text = "Nooo!";
			break;
		case 6 : 
			m_textMiss.text = "Ow!";
			break;
		case 7:
			m_textMiss.text = "Too Bad!";
			break;
		case 8:
			m_textMiss.text = "FLOP";
			break;
		case 9:
			m_textMiss.text = "Ouch!";
			break;
		case 10:
			m_textMiss.text = "Aww!";
			break;
		case 11:
			m_textMiss.text = "Brick!";
			break;
		case 12:
			m_textMiss.text = "Bam!";
			break;
		case 13:
			m_textMiss.text = "Pow!";
			break;
		default : 
			m_textMiss.text = "AirBall!";
			break;
		}

		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		m_textMiss.color = m_listMessageColors [0];

		m_scriptGamesceneInstance.Gamescene_Die();

		//ZCameraMgr.instance.DoShake ();
		m_eState = GAME_STATE.RESULTS;
	
		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;




		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel - m_currentScore);

		m_currentAds++;

		m_currentAttempts++;
		//PlayerPrefs.SetInt ("attempts", m_currentAttempts);
		m_scriptGamesceneInstance.UpdateAttempts ();

//		if (IsLevelBased()) 
//		{
//			//int currentAttempts = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
//			int currentAttempts = m_scriptGamesceneInstance.GetTotalAttempts();
//			PlayerPrefs.SetInt ("Attempts" + m_currentMode, currentAttempts + 1);
//			if( m_currentAttempts == 0 )
//				m_textTries2.text = "";
//			else
//				m_textTries2.text = "TRIES " + (currentAttempts);
//		} 
//		else 
//		{
//			m_textTries2.text = "";
//		}
//
//		LeanTween.delayedCall (1f, ShowResultsScreen);
		LeanTween.cancel(this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 1).setOnComplete (ShowResultsScreen);
	}

    public void ShowBuyCoins(bool p_show)
    {
        m_groupBuyCoins.gameObject.SetActive(p_show);
        ZAudioMgr.Instance.PlaySFX(m_audioButton);
    }

	public void PostScoreToLeaderboards()
	{
		int GameBestScore = m_scriptGamesceneInstance.GetBestScore ();
		if (m_currentScore >= GameBestScore) {
			PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
			PlayerPrefs.Save ();

			m_currentLevel = m_currentScore;
			//m_isNewBest = true;

			// Only submit if new high score
			#if UNITY_ANDROID
			ZPlatformCenterMgr.Instance.PostScore (m_scriptGamesceneInstance.GetBestScore(), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
			#else
			ZPlatformCenterMgr.Instance.PostScore (m_scriptGamesceneInstance.GetBestScore(), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
			#endif
		}
	}

	void DieRoutine()
	{
		ZAudioMgr.Instance.PlaySFX(m_audioButton);

		LeanTween.delayedCall (0.5f, ShowResultsScreen);
	} 
		

	public bool IsLevelBased ()
	{
		return m_scriptGamesceneInstance.GameType == GAME_TYPE.LEVEL ? true : false;
	}

	public ShopBird GetCurrentShopBird()
	{
		return m_listShopBirds [GetCurrentSelectedBird ()];
	}

	public int GetCurrentCoinAdd()
	{
		return m_currentCoinAdd;
	}

	void ShowUpgradeCoin()
	{
		m_textPerScore.gameObject.SetActive (true);
		m_textPerScore.text = "+" + GetCurrentCoinAdd () + "/SCORE";

		Vector3 currentPosition = m_textPerScore.gameObject.transform.localPosition;
		m_textPerScore.gameObject.transform.localPosition += new Vector3 (0, 20, 0);
		LeanTween.moveLocal (m_textPerScore.gameObject, currentPosition, 0.2f);

		m_currentCoinAdd = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		if (m_currentCoinAdd <= 0)
			m_currentCoinAdd = 1;

		LeanTween.delayedCall (1f, ShowUpgradeCoin2);

		ZAudioMgr.Instance.PlaySFX(m_audioButton);
	}

	void ShowUpgradeCoin2()
	{
		m_textPerScore.text = "+" + GetBestScore () + "/SCORE";

		LeanTween.cancel (m_textPerScore.gameObject);
		m_textPerScore.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		LeanTween.scale (m_textPerScore.gameObject, m_textPerScore.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


		LeanTween.delayedCall (2f, SetupLevel);

		ZAudioMgr.Instance.PlaySFX(m_audioReward);
	}

	public void ShowResultsScreen()
	{
		PostScoreToLeaderboards ();
		int totalBalls = m_prefabBall.GetComponent<SBBall> ().m_listOfBallTypes.Count - 1;
		//int curPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		if (m_currentHits >= m_listPrices [m_currentPurchaseCount] && PlayerPrefs.GetInt ("currentPurchaseCount") < 20) 
		{
            UnlockNewCar();

        } else if (m_currentLevel > 3 && m_currentAds > 7 && m_currentAdsTime <= 0 && ZAdsMgr.Instance.IsInterstitialAvailable ()) {
			m_currentAds = 0;
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAdsTime = 60;
			LeanTween.delayedCall (2f, SetupLevel);
			//LeanTween.cancel (this.gameObject);
			//LeanTween.scale (this.gameObject, this.transform.localScale, 2).setOnComplete (SetupLevel);
		} else if (m_currentLevel > 3 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			ZRewardsMgr.instance.ShowRewardSuccess ();
			ZAudioMgr.Instance.PlaySFX(m_audioReward);
			//LeanTween.cancel (this.gameObject);
			//LeanTween.scale (this.gameObject, this.transform.localScale, 2f).setOnComplete (SetupLevel);
			LeanTween.delayedCall (2f, SetupLevel);
		} else {
            LeanTween.delayedCall(1f, SetupLevel);
		}
	}


    private void UnlockNewCar()
    {
        ZAudioMgr.Instance.PlaySFX(m_audioReward);
        //m_objectReadyUnlock.SetActive (true);
        m_rootMain.gameObject.SetActive(false);
        //LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.1f, 0.1f, 0.1f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);
        //LeanTween.delayedCall (2f, ShowUnlock);
        LeanTween.cancel(this.gameObject);
        LeanTween.scale(this.gameObject, this.transform.localScale, 1f).setOnComplete(ShowBannerUnlock);
    }

    public void ShowBannerUnlock()
    {
        m_eState = GAME_STATE.UNLOCK;
        ZAudioMgr.Instance.PlaySFX(m_audioShoot);
        m_objUnlockBanner.gameObject.SetActive(true);
        m_objUnlockBanner.transform.localScale = new Vector3(m_objUnlockBanner.transform.localScale.x, 0, m_objUnlockBanner.transform.localScale.z);
        LeanTween.cancel(m_objUnlockBanner.gameObject);
        LeanTween.scaleY(m_objUnlockBanner.gameObject, 0.19f, 0.5f).setOnComplete(ShowUnlockText).setEase(LeanTweenType.easeOutExpo);
    }

    public void ShowUnlockText()
    {
        ZAudioMgr.Instance.PlaySFX(m_audioShoot);
        m_objUnlockText.gameObject.SetActive(true);
        m_objUnlockText.transform.localPosition = new Vector3(-500, m_objUnlockText.transform.localPosition.y, m_objUnlockText.transform.localPosition.z);
        LeanTween.cancel(m_objUnlockText.gameObject);
        LeanTween.moveLocalX(m_objUnlockText.gameObject, 0, 0.5f).setOnComplete(UnlockDelay).setEase(LeanTweenType.easeOutExpo);
    }

    public void UnlockDelay()
    {
        LeanTween.cancel(m_objUnlockText.gameObject);
        LeanTween.moveLocalX(m_objUnlockText.gameObject, 0, 1.5f).setOnComplete(ShowUnlock).setEase(LeanTweenType.easeOutExpo);;
    }

    public void ShowUnlockAnimation()
    {
        m_objUnlockBanner.gameObject.SetActive(false);
        m_objUnlockText.gameObject.SetActive(false);
        GameScene.instance.ShowShop(false); 
    }


	void ShowContinueRoutine(){
		ShowRewardedAdPopup ("Continue?", "Watch an ad to continue!");
		m_currentIncentifiedAdsTime = 120;
	}

	void ShowShopAvailable()
	{
		m_objectReadyUnlock.SetActive (true);
		LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 1.505f).setOnComplete (SetupLevel);
		//LeanTween.delayedCall (1.505f, SetupLevel);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

	}

	void ShowReward(){
		//LeanTween.delayedCall (2f, SetupLevel);
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 2f).setOnComplete (SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}


	public int GetMode()
	{
		return m_currentMode;
	}

	public void SpawnBallParticle(Vector3 p_position)
	{
		GameObject particleObj = ZObjectMgr.Instance.Spawn3D (m_prefabHitParticle.name, p_position);
		particleObj.gameObject.SetActive (true);
		particleObj.GetComponent<ParticleSystem> ().Stop ();
		particleObj.GetComponent<ParticleSystem> ().Play ();
	}

	public void ShowScore(int p_score)
	{
        int bestScore = m_scriptGamesceneInstance.GetBestScore();
        if(p_score > bestScore && !m_isNewBest && GetMode() != 1)
        {
            m_isNewBest = true;
            LeanTween.cancel(m_textBest.gameObject);
            LeanTween.scale(m_textBest.gameObject, new Vector3(0.8f, 0.8f, 0.8f), 0.2f).setLoopPingPong();
            m_textBest.text = "NEW HIGHSCORE";
        }
		m_textScore.text = "" + p_score.ToString("00");
		m_textScoreShadow.text = "" + p_score.ToString("00");
		//m_rootUIScore.transform.position;

		//int scoreLength = m_textScore.text.Length;
		//float scoreOffset = 150; //-24f
		//float scoreboardScale = 0f;

		/*if (scoreLength < 5) {
			scoreboardScale += (0.5f * scoreLength);
			scoreOffset += (17 * scoreLength);
		} else {
			m_textScore.text = "9999";// + p_score.ToString("00");
			scoreboardScale += (0.5f * 4);
			scoreOffset += (17 * 4);
		}*/

		//m_groupScore.GetComponent<RectTransform>().anchoredPosition = new Vector3 (scoreOffset,m_groupScore.GetComponent<RectTransform>().anchoredPosition.y, 0);
		//m_groupScore.transform.position = new Vector3 (scoreOffset, m_groupScore.transform.position.y, m_groupScore.transform.position.z);
		//m_objScoreboardScale.transform.localScale = new Vector3 (scoreboardScale, m_objScoreboardScale.transform.localScale.y, m_objScoreboardScale.transform.localScale.z);

	}

    public void WatchAdsToContinue()
    {
        m_scriptGamesceneInstance.Gamescene_ShowWatchAdsToContinue();
    }

    public void ContinueWatchAds()
    {
        m_scriptGamesceneInstance.GameScene_WatchAdsContinue();
    }

	public void Score(int score, bool p_initialScore = true)
	{
		//m_scoreAdded = score;

		/*if (IsLevelBased ()) 
		{ // SNAP score to 1
			int newScore = (score > 0) ? 1 : -1;
			m_currentScore += newScore;
			Debug.Log ("Level Based Current Score : " + m_currentScore);
		} 
		else 
		{*/
			m_currentScore += score;
			Debug.LogWarning ("Endless Current Score : " + m_currentScore);
		//}



		Debug.LogWarning ("Current Score : " + m_currentScore);

		ShowScore (m_currentScore);

		m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));
		//if (p_initialScore) 
		{
			m_scriptGamesceneInstance.UpdateScore (m_currentScore);
		}
		m_scriptGamesceneInstance.Gamescene_Score ();
		//GameScene.instance.AddCoins (1);
//		if (score == 3) {
//			GameScene.instance.AddCoins (3);
//		} else if (score == 1) {
//			GameScene.instance.AddCoins (1);
//		}
		//int shotState = m_prevBall.GetComponent<SBBall>().m_shotState;
//		if (score == 3) {
//			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Ringless,FDShootOut.instance.m_rootBasket.transform.position);
//		} else if (score == 2) {
//			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Special, FDShootOut.instance.m_rootBasket.transform.position);
//		} else if (score == 1) {
//			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Normal, FDShootOut.instance.m_rootBasket.transform.position);
//		}

		/*if (score > 1) 
		{
			ZAudioMgr.Instance.PlaySFXVolume (m_audioSpecialScore, 0.5f);
			Handheld.Vibrate();
		} 
		else 
		{
			ZAudioMgr.Instance.PlaySFXVolume (m_audioScore, 0.5f);
		}

		int multiplierIndex = Mathf.FloorToInt(Mathf.FloorToInt(GetScore() / 10f)) + 1;
		if (multiplierIndex >= 5) 
		{
			multiplierIndex = 5;
		}
		multiplierIndex-=1;*/

		//ParticleShootManager.Instance.AnimateCrowd (0.1f);
		//CourtManager.Instance.AnimateCrowd (0.1f);
		//LeanTween.cancel (m_textScore.gameObject);
		//LeanTween.scale (m_textScore.gameObject, m_textScore.transform.localScale, 1f).setOnComplete (ResetCrowdAnim);

//		m_textScore.text = m_currentScore + "";
//		m_textScoreShadow.text = m_currentScore.ToString("00") + "";

		//if (IsLevelBased()) 
		//{
		//	Success ();
		//}
	}

	private void ResetCrowdAnim()
	{
		CourtManager.Instance.AnimateCrowd (1f);
		//ParticleShootManager.Instance.AnimateCrowd (1f);
	}


	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		m_currentAds++;

		#if UNITY_ANDROID
//		ZPlatformCenterMgr.Instance.PostScore(m_scriptGamesceneInstance.GetBestScore(), m_scriptGamesceneInstance.AndroidID);
		ZPlatformCenterMgr.Instance.PostScore(m_scriptGamesceneInstance.GetBestScore(), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else
//		ZPlatformCenterMgr.Instance.PostScore(m_scriptGamesceneInstance.GetBestScore(), m_scriptGamesceneInstance.iOSID);

		ZPlatformCenterMgr.Instance.PostScore(m_scriptGamesceneInstance.GetBestScore(), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif // UNITY_ANDROID

//		m_objectMiss.SetActive (true);
		SetWinText ();
		m_scriptGamesceneInstance.UpdateAttempts (0);

		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 3f).setOnComplete (SetupLevel);
		//LeanTween.delayedCall (3f, SetupLevel);

		m_eState = GAME_STATE.RESULTS;
		if (m_currentScore > m_scriptGamesceneInstance.GetBestScore()) 
		{
			m_scriptGamesceneInstance.UpdateScore (m_currentScore);
			m_currentLevel = m_currentScore;
			// = true;

			// Only submit if new high score
			#if UNITY_ANDROID
			//ZPlatformCenterMgr.Instance.PostScore(m_scriptGamesceneInstance.GetBestScore(), m_scriptGamesceneInstance.AndroidID);
			ZPlatformCenterMgr.Instance.PostScore (m_scriptGamesceneInstance.GetBestScore(), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
			#else
			//ZPlatformCenterMgr.Instance.PostScore(m_scriptGamesceneInstance.GetBestScore(), m_scriptGamesceneInstance.iOSID);
			ZPlatformCenterMgr.Instance.PostScore (m_scriptGamesceneInstance.GetBestScore(), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
			#endif
		}

		Debug.Log ("Success");

	}

	private void SetWinText()
	{
		int Attempts = m_scriptGamesceneInstance.GetTotalAttempts ();
		if (Attempts <= 1) {
			m_textMiss.text = "LEGENDARY!";
		} else if (Attempts <= 2) {
			m_textMiss.text = "THE GOAT!";
		} else if (Attempts <= 3) {
			m_textMiss.text = "MVP!";
		} else if (Attempts <= 4)
			m_textMiss.text = "FANTASTIC!";
		else if (Attempts <= 5)
			m_textMiss.text = "SWOOSH";
		else if (Attempts <= 15)
			m_textMiss.text = "NICE";
		else if (Attempts >= 30) {
			m_textMiss.text = "FINALLY";
		} else if (Attempts >= 50) {
			m_textMiss.text = "ABOUT TIME";
		} else {
			m_textMiss.text = "COOL SHOT";
		}
//		int chatvalue = Random.Range (0, 15);
//		switch (chatvalue) 
//		{
//			case 0 : 
//				m_textMiss.text = "YEY!";
//			break;
//			case 1:
//				m_textMiss.text = "LIT!";
//			break;
//			case 2:
//				m_textMiss.text = "ALL NET!";
//			break;
//			case 3:
//				m_textMiss.text = "SWOOSH!";
//			break;
//			case 4:
//				m_textMiss.text = "SUPERSTAR!";
//			break;
//			case 5:
//				m_textMiss.text = "ON FIRE!";
//			break;
//			default : 
//				m_textMiss.text = "MVP!";
//			break;
//		}

		m_textMiss.gameObject.SetActive (true);
		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.5f, 0.5f, 1f), 0.5f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutExpo);

	//	LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (6).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		m_textMiss.color = Color.yellow;
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;

		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
		} else { 
			//m_currentAds = 2;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		Advertisement.Show ("rewardedVideo", options);

		m_objectWatchVideo.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
				ContinueLevel ();
			break;
		case ShowResult.Skipped:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}

	void SuccessRoutine()
	{
		m_textLevel.text = "" + GetGameTag();
		//m_textScore.text = "" + m_currentLevel.ToString("00");
		ShowScore(m_currentLevel);
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1.5f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_currentLevel > 1 && ZRewardsMgr.instance.WillShowRewardSuccess ()) 
		{
			ZRewardsMgr.instance.ShowRewardSuccess ();
		}
	}

	void SuccessRoutine3()
	{
		//m_objectSuccess.SetActive (false);
		m_currentLevel++;
		ResetScore();

		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);

		SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public int GetLevel()
	{
		if (IsLevelBased()) 
		{
			return m_scriptGamesceneInstance.GetBestScore();
		} 
		else 
		{
			return m_currentLevel;
		}
	}

	public void Play()
	{	
		//if (m_currentMode == 1 || m_currentMode == 15)
		//{
		//	m_currentHighscore = m_currentLevel;
		//}


		//m_eState = GAME_STATE.IDLE;
		m_textScore.color = m_colorTextCorrect;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		
		m_objectTopBar.SetActive(false);
		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);

		m_textScore.color = m_colorTextCorrect;
		//m_eState = GAME_STATE.IDLE;

	}

	bool isChangeModeFromButton = false;

	public void ChangeMode(bool p_changeByReverse)
	{
        ShowWhiteFlash();
		LeanTween.cancel(this.gameObject);
		isChangeModeFromButton = true;


		if (!p_changeByReverse) {
			m_currentMode++;// = p_modeSelected;
			if (m_currentMode >= m_listGamesceneInstance.Count)
				m_currentMode = 0;
		} else {
			m_currentMode--;// = p_modeSelected;
			if (m_currentMode <= -1)
				m_currentMode = m_listGamesceneInstance.Count - 1;
		}

		int gamesceneCount = 0;
		foreach (FDGamesceneInstance instance in m_listGamesceneInstance) {
			if (gamesceneCount != m_currentMode) {
				instance.gameObject.SetActive (false);
			} else {
				instance.gameObject.SetActive (true);
			}
			gamesceneCount++;
		}
		m_listGamesceneInstance [m_currentMode].gameObject.SetActive (true);

		//CourtManager.Instance.SetupCourt (m_currentMode);
		RefreshMode ();

		ZAnalytics.Instance.SendModesButton ();

		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		m_scriptGamesceneInstance.Gamescene_Unloadlevel ();
		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];
		m_scriptGamesceneInstance.Gamescene_Setuplevel (false);
		m_textModes.text = m_scriptGamesceneInstance.GameName;


		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		isChangeModeFromButton = false;
		SetupLevel ();
	}

	public int GetTotalAttempts()
	{
		return m_scriptGamesceneInstance.GetTotalAttempts ();
	}

	public void NextLevel()
	{
		int curScore = m_scriptGamesceneInstance.GetBestScore () + 1;
		m_scriptGamesceneInstance.UpdateScore (curScore);
		SetupLevel ();
	}

	public void PrevLevel()
	{
		int curScore = m_scriptGamesceneInstance.GetBestScore () - 1;

		if (curScore > 0) {
			m_scriptGamesceneInstance.UpdateScore (curScore, true);	
			SetupLevel ();
		}
	}

//	public void ChangeMode(bool p_reverseChange)
//	{
//		isChangeModeFromButton = true;
//		m_currentMode++;
//		if (m_currentMode >= m_listGamesceneInstance.Count) 
//		{
//			m_currentMode = 0;
//		}
//
//		int gamesceneCount = 0;
//		foreach (FDGamesceneInstance instance in m_listGamesceneInstance) {
//			if (gamesceneCount != m_currentMode) {
//				instance.gameObject.SetActive (false);
//			} else {
//				instance.gameObject.SetActive (true);
//			}
//			gamesceneCount++;
//		}
//		m_listGamesceneInstance [m_currentMode].gameObject.SetActive (true);
//
//
//		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];
//
//		SetupLevel ();
//		RefreshMode ();
//
//		ZAnalytics.Instance.SendModesButton ();
//
//		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";
//		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";
//
//		
//
//		PlayerPrefs.SetInt ("Mode", m_currentMode);
//
//		ResetModeText ();
//
//		//Debug.LogError ("Change Mode : " + m_currentMode);
//
//		m_scriptGamesceneInstance.Gamescene_Unloadlevel ();
//
//
//		ZAudioMgr.Instance.PlaySFX (m_audioButton);
//		isChangeModeFromButton = false;
//	}

	void ResetModeText()
	{
		m_textLevel.text = "" + GetGameTag();
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendNoAdsButton ();
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		ScreenCapture.CaptureScreenshot(ScreenshotName);

		//LeanTween.delayedCall(1f,ShareScreenshotRoutine);
		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		//ShareScreenshotWithText ("");
		//ZFollowUsMgr.Instance.ShareScreenshotWithText ();
		//ShareScreenshotWithText("");

		#if UNITY_ANDROID
		if (UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)) {
			Debug.Log("SharePhoto");
			ZFollowUsMgr.Instance.Share ();
			ZAnalytics.Instance.SendShareButton ();
		} else {
			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			}, () => {
			// add not permit action
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			});
		}
		#else
		ZFollowUsMgr.Instance.Share ();
		ZAnalytics.Instance.SendShareButton ();
		#endif
	}

	public void OpenLeaderboard()
	{
//		if (m_eState == GAME_STATE.SHOP || m_eState == GAME_STATE.UNLOCK) {
//			return;
//		}

		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
	//	ZPlatformCenterMgr.Instance.ShowLeaderboardUI(m_scriptGamesceneInstance.AndroidID);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;
	public GameObject m_objectUnlock;
	public GameObject m_objectModeSelect;

	public void ShowModeSelect()
	{
		if (m_objectShop.gameObject.activeSelf) 
		{
			m_objectShop.gameObject.SetActive (false);
		}

		m_objectModeSelect.gameObject.SetActive (true);
		m_eState = GAME_STATE.START;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		//mouseDown = false;
	}

	public void CloseModeSelect()
	{
		//m_eState = GAME_STATE.IDLE;
		//m_objectModeSelect.gameObject.SetActive (false);
		//GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		//GameScene.instance.m_rootUIBestScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);
		GameScene.instance.SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowShop(bool p_isUnlock = false)
	{
        Camera.main.fieldOfView = 37.1f;

		LeanTween.cancel (this.gameObject);


		if (p_isUnlock) {
			m_scriptGamesceneInstance.Gamescene_Unloadlevel ();
		}

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 10;
		LeanTween.cancel (ZCameraMgr.instance.gameObject);
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (0, 13, -80);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3(21f, 0, 0);
        BBCar.Instance.StartShowcase();
        BBColorManager.Instance.ResetGameColor();
        m_objectEarned.gameObject.SetActive(false);

        m_scriptGamesceneInstance.Gamescene_OpenShop();

		m_groupScore.gameObject.SetActive (false);
		//m_rootUIScore.gameObject.SetActive (false);
		//m_rootUIBestScore.gameObject.SetActive (false);
		m_rootMain.gameObject.SetActive (false);
		m_objShootoutTimer.gameObject.SetActive (false);

        m_objectWatchVideo.gameObject.SetActive(false);
		m_objectShop.gameObject.SetActive (true);
		ShopScene.instance.SetupScene(p_isUnlock);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZObjectMgr.Instance.ResetAll ();
		ZAnalytics.Instance.SendShopButton ();

		m_eState = GAME_STATE.SHOP;
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif

		ZAnalytics.Instance.SendRateUsButton ();
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	public void Resume(){
		ZGameMgr.instance.Resume ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void AddCoins(int index, bool playSound=false)
	{

		m_currentHits += index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

        ZAudioMgr.Instance.PlaySFXVolume (m_audioCoin, 0.25f);

//		if (index <= 0) {
//			m_textMeshCoin.text = "";
//			m_textMeshCoinShadow.text = "";
//		} else {
//			m_textMeshCoin.text = "+" + index;
//			m_textMeshCoinShadow.text = "+" + index;
//		}

		if( index > 5 ) 
		{
			LeanTween.scale (m_objectFullBar.gameObject, new Vector3 (Mathf.Min (1, m_currentHits * 1.0f / GetCurrentShopPrice ()),5.38f, 1), 0.6f).setEase (LeanTweenType.easeInCubic).setOnComplete(AddCoinPlayAudioFull); //9.22f
		
           // float fillAmount = 

          //  LeanTween.value(m_objectFullBar.gameObject,m_objectFullBar.GetComponent<Image>().fillAmount, Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice()), 0.6f).setEase(LeanTweenType.easeInCubic).setOnComplete(AddCoinPlayAudioFull); //9.22f

            //m_objectFullBar.GetComponent<Image>().fillAmount = Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice());
            ZAudioMgr.Instance.PlaySFX (m_audioAddCoinGain);
			m_objectFullBar.GetComponent<Image>().color = m_colorFullBarGain;
		} 
		else 
		{
            // float fillAmount = Mathf.Min(1, m_curDistance * 1.0f / m_targetDistance);

            //m_objectFullBar.GetComponent<Image>().fillAmount = Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice());

			m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 5.38f, 1);
			m_objectFullBar.GetComponent<Image>().color = m_colorFullBarGain;
			LeanTween.delayedCall (0.2f, AddCoinRoutine3);
		}

	}



		void AddCoinPlayAudioFull()
		{
    		ZAudioMgr.Instance.PlaySFX (m_audioAddCoinFull);
    		if (GetCurrentCoins() >= GetCurrentShopPrice ()) 
            {
    	        m_objectFullBar.GetComponent<Image>().color = m_colorFullBarFull;
                UnlockNewCar();
            } 
            else 
            {
    		    m_objectFullBar.GetComponent<Image>().color = m_colorFullBarYellow;
    		}
		}
	void AddCoinRoutine3()
		{
		if (GetCurrentCoins() >= GetCurrentShopPrice ()) {
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarFull;
		} else {
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarYellow;
		}
		//m_directionalLight.intensity = 0.7f;
		//	m_spotLight.SetActive (false);
	}


	public void SpendCoins(int index)
	{
		if (m_currentHits >= index) 
		{
			m_currentHits -= index;
			PlayerPrefs.SetInt ("Hit", m_currentHits);

			m_currentPurchaseCount++;

			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			//m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

			//if (playSound) {
			ZAudioMgr.Instance.PlaySFX (m_audioCoin);
			//}

			//m_textMeshCoin.text = "+" + index;
			//m_textMeshCoinShadow.text = "+" + index;

			m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min (1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 5.38f, 1);

			m_objectFullBar.GetComponent<Image> ().color = m_colorCOinYellow;
		}
	}

		public void ResetCoins()
		{
		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 5.38f, 1);
		}

	void AddCoinRoutine()
	{
		ZRewardsMgr.instance.ShowShopButton (true);
		LeanTween.delayedCall (1f, ShowShopAvailable);
	}

	public void RestoreAds()
	{
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		//return;

		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendRestoreIAPButton ();
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendMoreGamesButton ();
	}

	void SpawnViaMode()
	{
		switch(m_currentMode)
		{
		case 0:
			break;
		case 1: 
			break;
		case 2:
			break;
		}

		//m_objectHeadshot.SetActive (false);
	}

	public int GetCurrentSelectedBird()
	{
		return m_currentItem;
	}

	public List<GameObject> m_listBirds;

		public Text m_textName;

		public ShopBird m_currentItemObject;


	public void UseBird(int count)
	{
		if (PlayerPrefs.GetInt ("birdbought" + count) > 0)
		{
			m_currentItem = count;
			PlayerPrefs.SetInt ("currentBird", m_currentItem);
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (count);
            BBCar.Instance.SetCar(m_currentItem);
			ShopScene.instance.SelectBird ();
		} 
		else 
		{

		}
	}

	void SetupItem()
	{
		int x = 0;
		foreach (ShopBird objBird in m_listShopBirds) 
		{
			if (x == m_currentItem) 
			{
				objBird.objectCharacter.SetActive (true);
				m_textName.text = objBird.m_textCharacterName;

			} else {
				objBird.objectCharacter.SetActive (false);
			}
			x++;
		}
	}

	public int GetCurrentShopPrice()
	{
		return m_listPrices [m_currentPurchaseCount];
	}



	public void ShowUnlock()
	{
        m_objUnlockBanner.gameObject.SetActive(false);
        m_objUnlockText.gameObject.SetActive(false);
		m_scriptGamesceneInstance.Gamescene_Setuplevel (true);
	//	m_objHomeBtn.gameObject.SetActive (false);
	//	m_objCreditsBtn.gameObject.SetActive (false);
		m_objectReadyUnlock.gameObject.SetActive (false);
		ShowShop ();

		m_objectBox.SetActive (true);
		Vector3 currentPosition;
		currentPosition = m_objectBox.gameObject.transform.localPosition;
		m_objectBox.transform.localPosition += new Vector3 (0, 30, 0);
        m_objectBox.transform.eulerAngles = Vector3.zero;
        LeanTween.moveLocal (m_objectBox.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic).setOnComplete(RotateGift);

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 80;
		LeanTween.cancel (ZCameraMgr.instance.gameObject);//42, -11, -254
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (-188f, 39f, -254);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3(15f, 0, 0);

		ZAudioMgr.Instance.PlaySFX (m_audioThrow);

		m_eState = GAME_STATE.UNLOCK;
	}

    private void RotateGift()
    {
        LeanTween.cancel(m_objectBox.gameObject);
        LeanTween.rotateAround(m_objectBox.gameObject, new Vector3(0, 1, 0), 360, 5f).setLoopClamp();
    }

    //public void Idle()
    //{
    //    LeanTween.cancel(this.gameObject);
    //    LeanTween.rotateX(this.gameObject, 0, 0.3f).setEase(LeanTweenType.easeInOutExpo);
    //    LeanTween.scale(this.gameObject, this.transform.localScale, 0.5f).setOnComplete(Shake);
    //}

    //public void Shake()
    //{
    //    LeanTween.cancel(this.gameObject);
    //    LeanTween.rotateZ(this.gameObject, 15.0f, 0.3f).setLoopPingPong().setLoopCount(2).setEase(LeanTweenType.easeShake).setOnComplete(Idle);
    //}

		public int GetCurrentCoins()
		{
		return m_currentHits;
		}

	public void BuyBird(int count)
	{
		//if (m_currentHits >= m_listPrices[m_currentPurchaseCount] && PlayerPrefs.GetInt("birdbought" + count) == 0) 
		{
			m_eState = GAME_STATE.SHOP;
			PlayerPrefs.SetInt ("birdbought" + count, 1);
			
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();
			UseBird (count);

			m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 5.38f, 1);

		}
	}

	public void UnlockNewItem()
	{
		int count = 0;
		int unlokedControlledRandom = Random.Range (0, 100);

		m_listItemsToUnlock.Clear ();
		m_newBottlesToUnlock = 6;

		m_objectChangeModeBtn.gameObject.SetActive (false);
		m_eState = GAME_STATE.SHOP;

		for (int x = 0; x < m_listShopBirds.Count; x++) 
		{
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {

				if (x > 30) 
				{
					m_newBottlesToUnlock -= 1;
				}
			}
			else 
			{
				m_listItemsToUnlock.Add(x);
			}
		}

		// + EDIT JOSH 20170706
		// Controlled random unlock
		if (unlokedControlledRandom > 30) 
		{
		// + DAM - the algo is not so controlled, just making it full random for now
		//int randomval = Random.Range (m_listItemsToUnlock.Count - m_newBottlesToUnlock - 1, m_listItemsToUnlock.Count - 1);
		int randomval = Random.Range (0, m_listItemsToUnlock.Count - 1);
		int itemId = m_listItemsToUnlock [randomval];
		ZUnlockManager.Instance.ShowUnlock(itemId, m_listPrices[m_currentPurchaseCount]);
		} 
		else 
		{
			int randomval = Random.Range (0, m_listItemsToUnlock.Count - 1);
			int itemId = m_listItemsToUnlock [randomval];

		    ZUnlockManager.Instance.ShowUnlock(itemId, m_listPrices[m_currentPurchaseCount]);
		}
	}

	public void PurchaseItem()
	{
		m_currentHits -= m_listPrices [m_currentPurchaseCount];
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	}

	public int GetBestScore()
	{
		return m_scriptGamesceneInstance.GetBestScore ();
		//return PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);
	}

	public int GetScore()
	{
		return m_currentScore;
	}

	public void ConfirmBirdPurchase(int count)
	{
		m_currentHits -= m_listPrices[m_currentPurchaseCount];
		PlayerPrefs.SetInt ("Hit", m_currentHits);
		PlayerPrefs.SetInt ("birdbought" + count, 1);

		ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);
		ZAnalytics.Instance.SendCharComplete ("char", m_currentPurchaseCount);

		m_currentPurchaseCount++;
		PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
		PlayerPrefs.Save ();

		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;


		int x = 0;
		int currentBirdCount = 0;
		foreach (ShopBird objBird in m_listShopBirds) {
		if (PlayerPrefs.GetInt ("birdbought" + x) == 1) 
		{
			currentBirdCount++;
		}
			x++;
		}

		if (currentBirdCount >= 15) 
		{
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEg");
		}
	}

	public void BuyBall(int count)
	{
		if (m_currentHits >= 250  && PlayerPrefs.GetInt("ballbought" + count) == 0) {
			m_currentHits -= 250;
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("ballbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("ballbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("ball", m_currentPurchaseCount);

			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

			ShopScene.instance.SetupScene ();
		}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void RefreshMode()
	{	
		m_currentLevel = m_scriptGamesceneInstance.GetBestScore ();
        m_textModes.text = m_scriptGamesceneInstance.GameName;
		ResetScore ();
	}

	void ShowTutorialRoutine()
	{
		//m_objectTextTutorial.gameObject.SetActive (false);
		//m_objectTextTutorial.gameObject.SetActive (true);
//		if (m_eState != GAME_STATE.IDLE)
//			return;
//
//		Vector3 currentPosition;
//		currentPosition = m_objectTextTutorial.gameObject.transform.localPosition;
//		m_objectTextTutorial.gameObject.transform.localPosition -= new Vector3 (0, 30, 0);
//		LeanTween.moveLocal (m_objectTextTutorial.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);
	}

	private void ResetPlayerPrefs(int p_gameMode = -1)
	{
		if (p_gameMode > -1) 
		{
			m_listGamesceneInstance [p_gameMode].ResetScore ();
		}
		else 
		{
			foreach (FDGamesceneInstance instance in m_listGamesceneInstance) 
			{
				instance.ResetScore ();
			}
		}
	}

	public void ShowCredits()
	{
		m_objCreditsScreen.gameObject.SetActive (true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowTitleScreen()
	{
		LeanTween.cancel(this.gameObject);
		Camera.main.orthographicSize = 100;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		m_objTitleGroup.gameObject.SetActive (true);
		m_objTitleButtons.gameObject.SetActive (true);
		ZObjectMgr.Instance.ResetAll ();
		m_objectTutorialBanner.SetActive (false);
	//	m_objHomeBtn.gameObject.SetActive (false);
		m_objectTopBar.gameObject.SetActive (true);
		//m_objCreditsBtn.gameObject.SetActive (true);
	//	m_objectTextTutorial.gameObject.SetActive (false);
		//m_eState = GAME_STATE.TITLE_SCREEN;
		m_scriptGamesceneInstance.Gamescene_Unloadlevel ();
		m_textMiss.gameObject.SetActive (false);
	}

	public void StartGame()
	{
		m_objTitleGroup.gameObject.SetActive (false);
		m_objTitleButtons.gameObject.SetActive (false);
		m_objTitleButtons.gameObject.SetActive (false);
        m_eState = GAME_STATE.IN_GAME;
	}

	public void SetupLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

        ShowWhiteFlash();

        m_scriptGamesceneInstance.gameObject.SetActive (true);
        m_objTitleGroup.gameObject.SetActive(true);

        LeanTween.cancel(m_textBest.gameObject);
        m_textBest.gameObject.SetActive(true);
        m_textBest.text = "BEST " + m_currentScore;
        m_textBest.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);

		RefreshMode ();
		ResetModeText ();

		m_objectMiss.SetActive (false);
        m_objectNoInternet.gameObject.SetActive(false);
		m_textHit.gameObject.SetActive (true);
		m_objectReadyUnlock.SetActive (false);

		m_groupScore.gameObject.SetActive (true);
		m_objectBox.SetActive (false);

		m_objectTopBar.SetActive (true);
        m_isNewBest = false;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);

		PlayerPrefs.Save ();

        m_eState = GAME_STATE.TITLE_SCREEN;
        ShowScore(m_currentLevel);

		//ParticleShootManager.Instance.AnimCrowdIdle1 ();
		m_scriptGamesceneInstance.Gamescene_Setuplevel (true);

        if (m_objectWatchVideo.activeSelf)
        {
            m_objTutorial.gameObject.SetActive(false);
        }
        else{
            m_objTutorial.gameObject.SetActive(true);
        }


		m_textScoreShadow.gameObject.SetActive (true);
	}


		
	void ContinueLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

		m_listButtons [0].SetActive (true);
		m_textLevel.text = "Continue";
		//m_textScore.text = "" + m_currentScore.ToString("00");;
		ShowScore (m_currentLevel);

		//m_textLevel.color = m_colorTextSuccess;
		//m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		//m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		//ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendSkipComplete ();

		m_eState = GAME_STATE.START;
	}

	void ResetScore()
	{
		m_textLevel.text = GetGameTag();

		//if (IsLevelBased ())
		//	m_currentScore = m_currentLevel;
		//else
			//m_currentScore = 0;

		Debug.LogWarning ("Reset Score : " + m_currentScore);
	}

	private string GetInGameTag()
	{
		return (IsLevelBased()) ? ("LEVEL") : ("SCORE");
	}

	private string GetGameTag()
	{
		return (IsLevelBased()) ? ("LEVEL") : ("BEST");
	}


}
