﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopScene : ZGameScene 
{
	public static ShopScene instance;
	public Color m_colorNotbought;

	public AudioClip m_audioButton;
	public AudioClip m_audioReward;
	public AudioClip m_audioCelebrate;
	public AudioClip m_audioPop;
	public AudioClip m_audioCharge;
	public AudioClip m_audioFall;

	public GameObject m_objectShopContainer;

	public GameObject m_objectButton;
	public GameObject m_objCloseButton;
	public GameObject m_objectButtonUnlock;
    public GameObject m_objFloor;

	public GameObject m_obj;
	public ParticleSystem m_particleCelebrate;

	public List<string> m_listOfShopNames = new List<string>();
	public List<ZShopButton> m_listButtons;

	public Text m_textPrice;
	public Text m_textButtonPrice;
	public Text m_textRarity;

	public Sprite m_spriteLocked;
	public Sprite m_spriteUnlocked;

	public Sprite m_selected;
	public Sprite m_unlocked;

	public Text m_textName;

	public GameObject m_objectFlash;

	public GameObject m_objectScroll;

	public GameObject m_objectIAPButton;
	public GameObject m_objItem;

	public GameObject m_objectNewCharAvail;

	public SBBall m_ballData;

	private bool m_isUnlocking;

	int currentUnlockedBird;
	void Awake()
	{
		instance = this;

		currentUnlockedBird = PlayerPrefs.GetInt ("currentUnlockedBird");
	}

	public void Back()
	{
		if (!m_isUnlocking) {
			return;
		}

		this.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
        ZCameraMgr.instance.transform.localEulerAngles = new Vector3(9.8f, 0, 0);
		//GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		//GameScene.instance.m_rootUIBestScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);
        //GameScene.instance.m_objLapsCounter.gameObject.SetActive(true);

		//m_objCloseButton.gameObject.SetActive (false);
        BBCar.Instance.StopShowCase();
        m_objFloor.gameObject.SetActive(false);
		//GameScene.instance.SetupLevel ();

		// ResetItemsToUnlock
		//GameScene.instance.m_listItemsToUnlock.Clear ();
		//GameScene.instance.m_newBottlesToUnlock = 6;
		//GameScene.instance.m_objCreditsBtn.gameObject.SetActive(false);
        GameScene.instance.m_eState = GAME_STATE.TITLE_SCREEN;
		GameScene.instance.SetupLevel ();
        ZCameraMgr.instance.transform.position = new Vector3(0, 24.9f, -100f);
		//GameScene.instance.ResetCamera ();
	}

//	public void EnableItem(int p_item)
//	{
//		for (int i = 0; i < m_objItem.transform.childCount; i++) 
//		{
//			m_objItem.transform.GetChild(i).gameObject.SetActive(false);
//		}
//		m_objItem.transform.GetChild (p_item).gameObject.SetActive (true);
//		m_textName.text = "" + m_listOfShopNames [p_item];
//
//	}


	GameObject boxObject;
	int unlockCount = 0;
	float currentTimer;
	float currentTimerUp;
	int currentShopRarity;

	public void UnlockNow()
	{
		ZObjectMgr.Instance.ResetAll ();
		boxObject = GameScene.instance.m_objectBox;
        LeanTween.cancel(boxObject.gameObject);
        LeanTween.rotate(boxObject.gameObject, new Vector3(0, 0, 0), 0.2f).setEase(LeanTweenType.easeInOutExpo).setOnComplete(UnlockNow2);
	}

    public void UnlockNow2()
    {
        m_objectNewCharAvail.gameObject.SetActive(false);
        GameScene.instance.SpendCoins(GameScene.instance.GetCurrentShopPrice());

        currentUnlockedBird = PlayerPrefs.GetInt("currentUnlockedBird");
        currentUnlockedBird++;
        GameScene.instance.BuyBird(GameScene.instance.m_listToRandomUnlock[currentUnlockedBird]);
        PlayerPrefs.SetInt("currentUnlockedBird", currentUnlockedBird);
        PlayerPrefs.Save();

        m_objectButtonUnlock.SetActive(false);

        m_objectNewCharAvail.SetActive(false);

        int unlockType = GameScene.instance.m_listToRandomUnlock[currentUnlockedBird];

        currentTimer = 0.001f;
        currentTimerUp = 0;
        m_textRarity.text = "";

        UnlockRoutineShake();
    }

	//void UnlockRoutine()
	//{
	//	if( unlockCount < 5 )
	//		LeanTween.rotateLocal (boxObject, boxObject.transform.localEulerAngles + new Vector3 (0, 90, 0), 0.2f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop);
	//	else
	//		LeanTween.rotateLocal (boxObject, boxObject.transform.localEulerAngles + new Vector3 (0, 90, 0), 0.12f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop);
		
	//	unlockCount--;
	//	ZAudioMgr.Instance.PlaySFX (m_audioButton);

	//	currentTimerUp += 0.005f;
	//	currentTimer += currentTimerUp;
	//}

	void UnlockRoutineStop()
	{
		//if (unlockCount <= 0) {
			LeanTween.scale (boxObject, boxObject.transform.localScale + new Vector3 (20f, 20f, 20f), 0.2f).setEase (LeanTweenType.easeInOutCubic).setOnComplete(UnlockRoutineStop2);//.setLoopPingPong ().setLoopCount (2);
			//UnlockRoutineShake();
			ZAudioMgr.Instance.PlaySFX (m_audioReward);
		//} else {
		//	LeanTween.delayedCall (currentTimer, UnlockRoutine);
		//}
	}

	void UnlockRoutineStop2()
	{
		LeanTween.delayedCall(0.15f, UnlockRoutineShake);
	}

	void UnlockRoutineShake()
	{
		ZAudioMgr.Instance.PlaySFX (m_audioCharge);

        LeanTween.scale (boxObject,  new Vector3 (boxObject.transform.localScale.x, 10f, boxObject.transform.localScale.z), 2f).setEase (LeanTweenType.easeInOutCubic).setOnComplete(Explode);//.setOnComplete(UnlockRoutineShake);//.setLoopPingPong ().setLoopCount (2);
		boxObject.transform.localEulerAngles += new Vector3(0,10,0);
		//boxObject.transform.localPosition += new Vector3 (0, 0, 10);
		LeanTween.rotate (boxObject, boxObject.transform.localEulerAngles + new Vector3 (0, -10, 0), 0.04f).setLoopPingPong ();//.setEase(LeanTweenType.easeInOutQuad;
	}

	void Explode()
	{
		LeanTween.scale (boxObject, boxObject.transform.localScale + new Vector3 (300f, 300f, 300f), 0.2f);
		m_particleCelebrate.GetComponent<ParticleSystem> ().Clear ();
		m_particleCelebrate.gameObject.SetActive (true);
		m_particleCelebrate.GetComponent<ParticleSystem> ().Play ();
		if (currentShopRarity == 2) {
			m_particleCelebrate.GetComponent<ParticleSystem> ().Emit (200);
		} else if (currentShopRarity == 1) {
			m_particleCelebrate.GetComponent<ParticleSystem> ().Emit (100);
		}

		LeanTween.delayedCall (0.1f, Explode2);

		ZAudioMgr.Instance.PlaySFX (m_audioPop);
		ZAudioMgr.Instance.StopSFX (m_audioCharge);
	}

	void Explode2()
	{
		m_objectFlash.gameObject.SetActive (true);
		//m_objItem.gameObject.SetActive (true);
		LeanTween.delayedCall (0.1f, DisableFlash);
	}

	void DisableFlash()
	{
		UnlockNextBird ();
		m_objectFlash.gameObject.SetActive (false);


	}

	void UnlockNextBird()
	{
		boxObject.gameObject.SetActive (false);
//		currentUnlockedBird = PlayerPrefs.GetInt ("currentUnlockedBird");
//		currentUnlockedBird++;
//		GameScene.instance.BuyBird (GameScene.instance.m_listToRandomUnlock[currentUnlockedBird]);
//		PlayerPrefs.SetInt ("currentUnlockedBird", currentUnlockedBird);
//		PlayerPrefs.Save ();

		ShopScene.instance.SelectBird ();
		//GameScene.instance.UseBird(GameScene.instance.m_listToRandomUnlock[currentUnlockedBird]);
		Debug.Log ("Unlock Next Bird");

		m_listButtons [GameScene.instance.m_listToRandomUnlock[currentUnlockedBird-1]].m_imageButtonImage.GetComponent<Button> ().Select ();

		GameScene.instance.ShowShop (true);

		ZAudioMgr.Instance.PlaySFX (m_audioCelebrate);

		//LeanTween.delayedCall (1f, UnlockNextBirdRoutine);
		//ShopScene.instance.S ();
	}

	void UnlockNextBirdRoutine()
	{
		GameScene.instance.ResetCoins ();
	}

	public void SelectBird()
	{
		int x = 0;
        foreach (ZShopButton shopItems in m_listButtons) 
		{
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) 
			{
				if (x == GameScene.instance.GetCurrentSelectedBird ()) 
				{
					m_listButtons [x].m_imageButtonImage.sprite = m_selected;
					//if (ballType.BallRarity == Rarity.RARE) {
					//	m_textRarity.color = Color.yellow;
					//	m_textRarity.text = "RARE";
					//} else if (ballType.BallRarity == Rarity.EPIC) {
					//	m_textRarity.color = Color.magenta;
					//	m_textRarity.text = "EPIC";
					//} else {
					//	m_textRarity.color = Color.white;
					//	m_textRarity.text = "";
					//}
				} 
				else 
				{
					//m_listButtons [x].m_imageButtonImage.color = m_colorNotbought;
					m_listButtons [x].m_imageButtonImage.sprite = m_unlocked;
				}
			}
			x++;
		}

		ShowItem ();
		//EnableItem (ballItem);
	}

	public void SetupScene(bool p_isUnlock = false)
	{
		if (GameScene.instance.GetCurrentSelectedBird () < 12f) {
			m_objectScroll.transform.localPosition = new Vector3 (-1f, 0, 7f);
		} else if (GameScene.instance.GetCurrentSelectedBird () < 24f) {
			m_objectScroll.transform.localPosition = new Vector3 (-1f, 240.0f, 7f);
		} else {
			m_objectScroll.transform.localPosition = new Vector3 (-1f, 360.0f, 7f);
		}

		m_isUnlocking = p_isUnlock;

		//if (!p_isUnlock) {
		//	GameScene.instance.m_textModes.text = "UNLOCK";
		//	GameScene.instance.m_textModes.color = Color.white;
		//} else {
		//	GameScene.instance.m_textModes.text = "SHOP";
		//	GameScene.instance.m_textModes.color = Color.white;
		//}

		//m_textName.transform.localScale = new Vector3 (1, 1, 1);
		//LeanTween.cancel (m_textName.gameObject);

		m_textButtonPrice.text = "" + GameScene.instance.GetCurrentShopPrice ();
        m_objFloor.gameObject.SetActive(true);

		m_objectFlash.gameObject.SetActive (false);
		//m_textName.gameObject.SetActive (true);

		int ballType = GameScene.instance.GetCurrentSelectedBird ();
        m_textName.text = "" + m_listOfShopNames[ballType];

        //GameScene.instance.m_objLapsCounter.gameObject.SetActive(false);

		int x = 0;
        foreach (ZShopButton desc in m_listButtons) 
		{
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) 
			{
                //m_listButtons [x].m_imageButtonOverlay.gameObject.SetActive (false);
                //m_listButtons [x].m_textLabel.gameObject.SetActive (false);

				m_listButtons [x].m_imageButtonImage.sprite = m_unlocked;
				m_listButtons [x].m_imageCharacterPortrait.gameObject.SetActive (true);

                if (x == GameScene.instance.GetCurrentSelectedBird())
                {
                    m_listButtons[x].m_imageButtonImage.sprite = m_selected;
                    //if (shopItem.BallRarity == Rarity.RARE) {
                    //	m_textRarity.color = Color.yellow;
                    //	m_textRarity.text = "RARE";
                    //} else if (shopItem.BallRarity == Rarity.EPIC) {
                    //	m_textRarity.color = Color.magenta;
                    //	m_textRarity.text = "EPIC";
                    //} else {
                    //	m_textRarity.color = Color.white;
                    //	m_textRarity.text = "";
                    //}
                }

			} 
			else 
			{
                //m_listButtons [x].m_imageButtonOverlay.gameObject.SetActive (false);
                //m_listButtons [x].m_textLabel.gameObject.SetActive (true);
                {
                    m_listButtons[x].m_imageButtonImage.sprite = m_spriteLocked; ;
                    m_listButtons[x].m_imageCharacterPortrait.gameObject.SetActive(false);
                }
			}
			x++;
		}


		if (p_isUnlock) 
		{
			m_objectButtonUnlock.SetActive (false);
            m_objCloseButton.SetActive(true);
			m_objectButton.gameObject.SetActive (true);

			m_objectShopContainer.SetActive (true);
			Vector3 currentPosition = new Vector3 (0, -180, 20);
			//currentPosition = m_objectShopContainer.gameObject.transform.localPosition;
			m_objectShopContainer.gameObject.transform.localPosition -= new Vector3 (0, 60, 0);
			LeanTween.moveLocal (m_objectShopContainer.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic).setOnComplete(ShowItem);

			m_objectIAPButton.SetActive (true);
			//ShowItem ();
			//EnableItem (item);
			//m_objCloseButton.gameObject.SetActive (true);
			m_objectNewCharAvail.SetActive (false);
		} 
		else 
		{
			ZObjectMgr.Instance.ResetAll ();
			//m_objItem.gameObject.SetActive (false);
			m_objectButton.SetActive (false);
            m_objCloseButton.SetActive(false);
			m_objectButtonUnlock.SetActive (true);
			m_objectShopContainer.SetActive (false);

			m_textRarity.text = "";

			//m_textName.text = "UNLOCK\nNEW GIFT!";
			//m_textName.transform.localScale = new Vector3 (1, 1, 1);
			//LeanTween.scale (m_textName.gameObject, new Vector3 (1.1f, 1.1f, 1.1f), 1f).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

			m_textName.gameObject.SetActive (true);

			m_objectIAPButton.SetActive (false);

			boxObject = GameScene.instance.m_objectBox;

			LeanTween.cancel (boxObject);
			boxObject.transform.localEulerAngles = new Vector3 (0, 0, 0);
			boxObject.transform.localScale = new Vector3 (40f, 40f, 40f);

			ZAudioMgr.Instance.PlaySFX (m_audioFall);

			m_objectNewCharAvail.SetActive (true);
		}

		//m_textPrice.text = "" + GameScene.instance.GetCurrentShopPrice ();

		//GameScene.instance.m_rootUIScore.gameObject.SetActive (false);
		//GameScene.instance.m_rootUIBestScore.gameObject.SetActive (false);
		GameScene.instance.m_rootMain.gameObject.SetActive (false);


	}

	public void ShowItem()
	{
		int item = GameScene.instance.GetCurrentSelectedBird ();
        BBCar.Instance.SetCar(item);
		//m_objItem.GetComponent<SBBall> ().EnableBall (item);
        m_textName.text = "" + m_listOfShopNames[item];
		//EnableItem (item);
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
	}
}
