﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FCCharacterChecker : MonoBehaviour {

	public FDCharacter m_scriptCharacter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Floor") {
			if (m_scriptCharacter.gameObject.transform.localEulerAngles.z > -10 && m_scriptCharacter.gameObject.transform.localEulerAngles.z < 10) {
				m_scriptCharacter.Land ();
			}
		}
	}
}
