﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZGameButton : MonoBehaviour {
		
	[SerializeField]
	private float m_buttonOffset = 5.4f;
	private Vector3 m_origPos;

	public void OnPressDown()
	{
		m_origPos = this.transform.localPosition;
		this.transform.localPosition = new Vector3(this.transform.localPosition.x, m_origPos.y - m_buttonOffset, this.transform.localPosition.z);
		//Debug.Log ("OnPressDown");
	}

	public void OnRelease()
	{
		this.transform.localPosition = m_origPos;
		//Debug.Log ("OnRelease");
	}
}
