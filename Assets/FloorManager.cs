﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour 
{
    public List<MaterialOffseter> m_listOfMaterials;

    public void StartAnimation(float p_parallaxSpeed)
    {
        foreach(MaterialOffseter offset in m_listOfMaterials)
        {
            offset.StartMoving(p_parallaxSpeed); 
        }
    }

    public void StopAnimation()
    {
        foreach(MaterialOffseter offset in m_listOfMaterials)
        {
            offset.Reset();
        }
    }
}
