﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGScoreChecker : MonoBehaviour {

	public FGGoal m_scriptGoal;
	public GameObject m_objectBall;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void HideGoal()
	{
		m_scriptGoal.gameObject.SetActive (false);
	}

	void EnableBall()
	{
		m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = false;
		//FGKickMode.instance.Gamescene_Score ();
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Player") {
			GameScene.instance.Score (1);

			coll.GetComponent<Rigidbody2D> ().isKinematic = true;
			coll.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;

			m_objectBall = coll.gameObject;
			LeanTween.delayedCall (1f, EnableBall);

			LeanTween.delayedCall (0.5f, HideGoal);
		}
	}
}
