﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarShakeEffect : MonoBehaviour {

    public void StartAnimation()
    {
        StopAnimation();
        IdleCar();
    }

    public void IdleCar()
    {
        float speed = Random.Range(0.1f, 0.5f);
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalY(this.gameObject, this.transform.localPosition.y, speed).setOnComplete(ShakeCar);
    }

    public void ShakeOnce()
    {
        this.transform.localPosition = Vector3.zero;
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalY(this.gameObject, this.transform.localPosition.y - 0.4f, 0.2f).setLoopPingPong().setLoopCount(1).setEase(LeanTweenType.easeShake);
    }

    public void ShakeCar()
    {
        int randVal = Random.Range(0, 4);
        float speed = Random.Range(0.5f, 0.7f);
        LeanTween.cancel(this.gameObject);
        LeanTween.moveLocalY(this.gameObject, this.transform.localPosition.y - 0.25f, speed).setLoopPingPong().setLoopCount(randVal).setEase(LeanTweenType.easeShake).setOnComplete(IdleCar);
    }

    public void StopAnimation()
    {
        LeanTween.cancel(this.gameObject);
        this.transform.localPosition = Vector3.zero;
    }
}
