﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FDShootyEnum
{
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE
}

[System.Serializable]
public class LevelData
{
	public Vector3 SpawnPoint;
	public int CameraSetting;
	public float BoardYPos;
	public float Sensitivity;
}

public class FDShootyBasket : FDGamesceneInstance {

	public List<int> m_listOfRandomLevels = new List<int>();
	public List<Dictionary<string,object>> m_data 	= new List<Dictionary<string,object>>();

	public static FDShootyBasket instance;
	public FDShootyEnum m_eState;

	public GameObject m_spawnPoint;

	// TODO Create a seperate arrow object handler
	public GameObject m_objArrowChild;
	public GameObject m_objArrowParent;

	public bool m_hasChanged;

	public List<CameraStats> m_listOfCameraSettings;
	private List<Vector2> m_listOfShotAngle = new List<Vector2>()
	{
		new Vector2(1,1),			 // NORMAL
		new Vector2(0.7f,1.2f),		// HIGH ANGLE
		new Vector2(2f, 0.9f) 		// LOW ANGLE
	};

	public Text m_textTries;

	private SBBall m_curBall;
	private string m_prefabBallName;
	private float m_currentDeltaTime;
	private float m_animMovementSpeed;
	private float m_currentSensitivity;
	private Vector2 m_currentShotStrength;

	void Awake () 
	{
		instance = this;
		m_hasChanged = true;
	}

	public override void Gamescene_Update ()
	{
		Controls();
	}

	void Controls()
	{
		if (IsButtonPressed()) 
		{
			return;
		}

		if (Input.GetMouseButtonUp (0) && m_eState == FDShootyEnum.CHARGE) 
		{
			float timeDiff = Time.time - m_currentDeltaTime;
			Vector2 shotStrength = new Vector2 (m_currentSensitivity * m_currentShotStrength.x * 45000 * timeDiff, 
												m_currentSensitivity * m_currentShotStrength.y * 160000 * timeDiff);
			m_curBall.GetComponent<SBBall>().ShootBall (shotStrength);
			m_eState = FDShootyEnum.SHOOT;
			m_objArrowParent.gameObject.SetActive (false);
		} 
		else if (Input.GetMouseButtonDown (0) && m_eState == FDShootyEnum.IDLE) 
		{
			m_curBall.GetComponent<SBBall>().AnimateBall (m_currentSensitivity);
			m_currentDeltaTime = Time.time;
			m_eState = FDShootyEnum.CHARGE;
		}
	}

	public override void Gamescene_Score ()
	{
		BoardManager.Instance.AnimateNet ();
		m_hasChanged = true;
	}

	public override void Gamescene_Die ()
	{
		m_hasChanged = false;
	}


	public override void Gamescene_Setuplevel(bool hardreset) 
	{
		m_objArrowParent.gameObject.SetActive (true);

		BoardManager.Instance.EnableBoard();
		BoardManager.Instance.transform.localScale = new Vector3 (90, 90, 90);
		BoardManager.Instance.transform.eulerAngles = new Vector3 (0, 270, 0);

		LoadLevel ();
		GetNewBall ();
		m_objArrowParent.transform.position = m_curBall.transform.position;
		m_textTries.gameObject.SetActive (true);

		m_eState = FDShootyEnum.IDLE;
	}

	public override void Gamescene_Unloadlevel() 
	{
		this.gameObject.SetActive (false);
		m_textTries.gameObject.SetActive (false);
		if (m_curBall != null) {
			m_curBall.ResetBall ();
		}
	}

	private void GetNewBall()
	{
		m_prefabBallName = GameScene.instance.m_prefabBall.name;
		GameObject objBall = ZObjectMgr.Instance.Spawn2D (m_prefabBallName, m_spawnPoint.transform.position);
		m_curBall = objBall.GetComponent<SBBall>();
		int currentBall = GameScene.instance.GetCurrentSelectedBird ();
		LeanTween.cancel (m_curBall.gameObject);
		m_curBall.ResetBall ();
		m_curBall.EnableBall (currentBall);
	}

	private void LoadLevel()
	{
		if (m_data.Count <= 0) 
		{
			m_data = CSVReaderPlus.Read ("level_data");
		}

		int levelToLoad;
		int bestScore = GetBestScore ();
		if (bestScore > 200) 
		{
			int dataCount = m_listOfRandomLevels.Count;
			levelToLoad = m_listOfRandomLevels [bestScore % m_listOfRandomLevels.Count];
		} 
		else 
		{
			levelToLoad = bestScore - 1;
		}
			
		// BoardPosition
		float BoardYPos = float.Parse("" + m_data [levelToLoad] ["BoardY"]);
	//	if (p_newLevel) 
		{
			BoardManager.Instance.transform.localPosition = new Vector3 (123, BoardYPos, BoardManager.Instance.transform.localPosition.z);
		}

		// SpawnPoint
		float XPos = float.Parse("" + m_data [levelToLoad] ["BallX"]);
		float YPos = float.Parse("" + m_data [levelToLoad] ["BallY"]);
		m_spawnPoint.transform.localPosition = new Vector3(XPos, YPos, m_spawnPoint.transform.localPosition.z);

		// Camera Settings
		int cameraView = int.Parse("" + m_data [levelToLoad] ["Camera"]);
		CameraStats CameraSetting = m_listOfCameraSettings [cameraView];
		UpdateCamera (CameraSetting);
		UpdateBoardScale (cameraView);

		// Gravity 
		float Gravity = float.Parse ("" + m_data [levelToLoad] ["Gravity"]);
		Physics2D.gravity = new Vector3 (0, -Gravity);

		// Sensitivity;
		float Sensitivity = float.Parse("" + m_data[levelToLoad]["Sensitivity"]);
		SetArrowSensitivityIndicator (Sensitivity);

		// ANGLE
		string Angle = "" + m_data [levelToLoad] ["Angle"];
		SetArrowAngle (Angle);

		float RotateXStr = float.Parse("" + m_data [levelToLoad] ["RotateX"]);
		BoardManager.Instance.transform.eulerAngles = new Vector3 (RotateXStr, BoardManager.Instance.transform.eulerAngles.y, BoardManager.Instance.transform.eulerAngles.z);

		// MOVEMENT SPEED 
		string movementSpeedStr = "" + m_data [levelToLoad] ["MovementSpeed"];
		m_animMovementSpeed = float.Parse (movementSpeedStr);

		// IS MOVING
		string isMovingDir = "" + m_data [levelToLoad] ["IsMoving"];
		BoardManager.Instance.AnimateBoard (isMovingDir, m_animMovementSpeed);
		//MakeBoardMove (isMovingDir, m_hasChanged);

		// IS BOARDLESS
		string isBoardlessStr = "" + m_data[levelToLoad]["Boardless"];
		bool isBoardless = (isBoardlessStr == "TRUE") ? true : false;
		BoardManager.Instance.IsBoardless (isBoardless);
	}


	private void SetArrowAngle(string p_angle)
	{
		switch (p_angle) {
			case "Normal":
			{
				m_objArrowParent.transform.localEulerAngles = new Vector3 (0, 0, -20);
				m_currentShotStrength = m_listOfShotAngle[0];
			}
			break;

			case "High Angle":
			{
				m_objArrowParent.transform.localEulerAngles = new Vector3 (0, 0, -10);
				m_currentShotStrength = m_listOfShotAngle [1];
			}
			break;

			case "Low Angle":
			{
				m_objArrowParent.transform.localEulerAngles = new Vector3 (0, 0, -55);
				m_currentShotStrength = m_listOfShotAngle[2];
			}
			break;
		}
	}

	private void SetArrowSensitivityIndicator(float p_sensitivity)
	{
		m_currentSensitivity = p_sensitivity;

		if (m_currentSensitivity < 1) 
		{
			m_objArrowChild.GetComponent<SpriteRenderer> ().color = Color.red;		
		} else if (m_currentSensitivity == 1) {
			m_objArrowChild.GetComponent<SpriteRenderer> ().color = Color.yellow;
		} else if (m_currentSensitivity > 1) {
			m_objArrowChild.GetComponent<SpriteRenderer> ().color = Color.green;
		}
	}

	private void UpdateBoardScale(int p_cameraSetting)
	{
		int bestScore = GameScene.instance.GetBestScore ();
		if (GameScene.instance.GetBestScore() > 300) {
			BoardManager.Instance.transform.localScale = new Vector3 (80, 80, 80);
		}
		else if (GameScene.instance.GetBestScore() > 200) {
			BoardManager.Instance.transform.localScale = new Vector3 (86, 86, 86);
		} else {
			BoardManager.Instance.transform.localScale = new Vector3 (90, 90, 90);
		}
	}
}