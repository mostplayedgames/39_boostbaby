﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FDRocketLander : FDGamesceneInstance {

	public GameObject m_rootRocket;
	public Rigidbody2D m_rigidbodyRocket;
	public GameObject m_rocketBoost;

	float m_currentMousePos;
	float m_currentRocketRotation;

	// Use this for initialization
	void Awake () {
	}

	// Update is called once per frame
	//public override void Gamescene_Update () {
	public void FixedUpdate(){
		if (Input.GetMouseButtonDown (0)) {
			m_currentMousePos = m_currentRocketRotation - Input.mousePosition.x;// + m_rigidbodyRocket.gameObject.transform.localEulerAngles.z;
		}
		if(Input.GetMouseButton(0)){
			//Debug.Log ("Boost : " + Input.mousePosition.x);
			m_rigidbodyRocket.AddForce (m_rigidbodyRocket.transform.TransformDirection(Vector3.up * 2000 * Time.deltaTime));
			m_rigidbodyRocket.gameObject.transform.localEulerAngles = new Vector3 (0, 0, Input.mousePosition.x + m_currentMousePos );
			m_rocketBoost.SetActive (true);
		}
		if (Input.GetMouseButtonUp (0)) {
			m_rocketBoost.SetActive (false);
			m_currentRocketRotation = Input.mousePosition.x + m_currentMousePos;
		}
		Debug.Log ("Speed : " + m_rigidbodyRocket.gameObject.GetComponent<Rigidbody2D> ().velocity.y);

	}

	public override void Gamescene_ButtonDown () {
	}

	public override void Gamescene_ButtonUp () {

	}

	public override void Gamescene_Die () {
		m_rigidbodyRocket.gravityScale = 10;
	}

	public override void Gamescene_Score () {

	}

	public override void Gamescene_Setuplevel(bool hardreset) {
		this.m_rigidbodyRocket.transform.position = m_rootRocket.transform.position;
		this.m_rigidbodyRocket.transform.eulerAngles = Vector3.zero;
		m_rocketBoost.SetActive (false);
		m_rigidbodyRocket.gravityScale = 2;
	}

	public override void Gamescene_Unloadlevel() {
	}
}