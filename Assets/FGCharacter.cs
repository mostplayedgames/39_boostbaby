﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGCharacter : MonoBehaviour {

	public GameObject m_objectHead;
	public GameObject m_objectFeet;

	public int m_characterGround = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Kick()
	{
		LeanTween.cancel (this.m_objectFeet);
		m_objectFeet.transform.localEulerAngles = new Vector3 (0, 0, -15f);
		//m_objectFeet.transform.localEulerAngles = new Vector3 (0, 0, 0f);

		//LeanTween.rotateZ (m_objectFeet, 45f, 0.2f).setLoopPingPong().setLoopCount(2);
		LeanTween.rotateLocal (m_objectFeet, new Vector3(0,0,85f), 0.2f).setLoopPingPong().setLoopCount(2);

		if (m_characterGround > 0) {
			
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (80f, 1000f, 0));
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (300f, 1000f, 0));
		}//this.GetComponent<Rigidbody2D> ().AddTorque (-25f);
		//this.GetComponent<Rigidbody2D> ().AddTorque (-45f);
		this.GetComponent<Rigidbody2D> ().AddTorque (-65f);

		m_characterGround = 0;
		Debug.Log ("Kick : " + m_characterGround);
	}

	public void KickReset()
	{
		LeanTween.cancel (this.m_objectFeet);
		//m_objectFeet.transform.localEulerAngles = new Vector3 (0, 0, 15);

		//LeanTween.rotateZ (m_objectFeet, 0f, 0.2f).setLoopPingPong().setLoopCount(2);
		LeanTween.rotateLocal (m_objectFeet, new Vector3(0,0,15f), 0.2f);

		///if (m_characterGround > 0) {
			//this.GetComponent<Rigidbody2D> ().AddTorque (-20f);
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (40f, 400f, 0));
		//}



		m_characterGround = 0;
		Debug.Log ("Kick : " + m_characterGround);
	}

	public void KickBack()
	{
		LeanTween.cancel (this.m_objectFeet);
		m_objectFeet.transform.localEulerAngles = new Vector3 (0, 0, 15f);
		//m_objectFeet.transform.localEulerAngles = new Vector3 (0, 0, 0f);

		//LeanTween.rotateZ (m_objectFeet, 45f, 0.2f).setLoopPingPong().setLoopCount(2);
		LeanTween.rotateLocal (m_objectFeet, new Vector3(0,0,85f), 0.2f).setLoopPingPong().setLoopCount(2);;//.setLoopPingPong().setLoopCount(2);

		if (m_characterGround > 0) {

			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (80f, 1000f, 0));
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (-300f, 1000f, 0));
		}//this.GetComponent<Rigidbody2D> ().AddTorque (-25f);
		//this.GetComponent<Rigidbody2D> ().AddTorque (-45f);
		this.GetComponent<Rigidbody2D> ().AddTorque (65f);

		m_characterGround = 0;
		Debug.Log ("Kick : " + m_characterGround);
	}

	public void Reset()
	{
		this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		//LeanTween.cancel (this.gameObject);
		m_objectFeet.transform.localEulerAngles = new Vector3 (0, 0, 15f);
	}
}
