﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BBLevelMode : FDGamesceneInstance
{
    public static BBLevelMode instance;
    public BBStateEnum m_eState;
    public GameObject m_objectCar;
    public GameObject m_objectFloor;
    public GameObject m_objFinishLine;
    public AudioClip m_audioCelebrate;
    public AudioClip m_audioButton;

    public FloorManager m_floorAnimationMgr;


    public float m_gemTimerIdx = 25;

    private bool m_reachedFinishLine;

    void Awake()
    {
        instance = this;
        if(!PlayerPrefs.HasKey("BoostLevel"))
        {
            PlayerPrefs.SetInt("BoostLevel", 1);
        }
        if(!PlayerPrefs.HasKey("NumberOfCarsToSpawn"))
        {
            PlayerPrefs.SetInt("NumberOfCarsToSpawn", 20);
        }

        m_floorAnimationMgr = this.GetComponent<FloorManager>();
    }

    private Vector3 m_vecInit;
    private float m_initTouchX;
    private Vector3 m_latePos;
    public override void Gamescene_Update()
    {
        if (!m_objectCar.GetComponent<BBCar>().m_isBoost)
            m_gemTimerIdx -= 1 * Time.deltaTime;

        m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, 0, m_objectCar.transform.localPosition.z);

        if (m_reachedFinishLine)
            return;

 //       if (Input.touchCount < 2)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_initTouchX = Input.mousePosition.x;
                m_vecInit = m_objectCar.transform.localPosition;
                m_latePos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                float deltaPosX = (Input.mousePosition.x - m_initTouchX);
                m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, 0, (m_vecInit.z + (deltaPosX * 0.07f)));

                if (m_objectCar.transform.localPosition.z >= 12.6f)
                    m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, 12.6f);
                if (m_objectCar.transform.localPosition.z <= -12.6f)
                    m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, -12.6f);

                m_latePos = Input.mousePosition;
            }
        }
    }

    public override void Gamescene_ButtonDown()
    {
        BBCar.Instance.StartCar();
        m_floorAnimationMgr.StartAnimation(8f);
    }

    public void ReachedFinishLine()
    {
        m_floorAnimationMgr.StopAnimation();
        m_reachedFinishLine = true;

        int curBoostLevel = PlayerPrefs.GetInt("BoostLevel");
        PlayerPrefs.SetInt("BoostLevel", curBoostLevel + 1);

        int carsToSpawn = PlayerPrefs.GetInt("NumberOfCarsToSpawn") + 10;
        PlayerPrefs.SetInt("NumberOfCarsToSpawn", carsToSpawn);

        ZAudioMgr.Instance.PlaySFX(m_audioButton);

        LeanTween.delayedCall(2.5f, ScaleScoreBanner);
    }

    private void ScaleScoreBanner()
    {
        ZAudioMgr.Instance.PlaySFX(m_audioButton);
        LeanTween.scale(GameScene.instance.m_textBest.gameObject, GameScene.instance.m_textBest.transform.localScale + new Vector3(0.4f, 0.4f, 0.4f), 0.2f);//.setOnComplete(IncrementLevel);
        LeanTween.delayedCall(1f, IncrementLevel);
    }

    private void IncrementLevel()
    {
        GameScene.instance.m_textBest.text = "LEVEL " + PlayerPrefs.GetInt("BoostLevel");
        ZAudioMgr.Instance.PlaySFX(m_audioButton);
        LeanTween.delayedCall(1f, ReturnScoreBannerScale);
    }

    private void ReturnScoreBannerScale()
    {
        LeanTween.scale(GameScene.instance.m_textBest.gameObject, GameScene.instance.m_textBest.transform.localScale - new Vector3(0.4f, 0.4f, 0.4f), 0.2f);
        ZAudioMgr.Instance.PlaySFX(m_audioButton);
        LeanTween.delayedCall(0.5f, ReloadLevel);

    }

    private void ReloadLevel()
    {
        GameScene.instance.SetupLevel();
    }

    public override void Gamescene_ShowWatchAdsToContinue()
    {
        m_floorAnimationMgr.StopAnimation();
    }

    public override void GameScene_WatchAdsContinue()
    {
        m_floorAnimationMgr.StartAnimation(8f);
    }

    public override void GameScene_WatchAdsCancel()
    {
    }


    public override void Gamescene_Score()
    {
        if ((m_currentLevel -1) >= m_totalCarsSpawned)
        {
            SpawnLevel();
            if (m_currentLevel <= m_totalCarsSpawned)
            {
                m_currentX -= CAR_DISTANCE;
                m_objFinishLine.gameObject.SetActive(true);
                m_objFinishLine.transform.localPosition = new Vector3(m_currentX, 0, 0);
            }
        }

        if (GameScene.instance.m_currentScore % 30 == 0 && GameScene.instance.m_currentScore > 0)
        {
            BBColorManager.Instance.ChangeColor();
        }
    }

    public override void Gamescene_Die()
    {
        m_floorAnimationMgr.StopAnimation();
    }

    public bool WillSpawGem()
    {
        if (m_gemTimerIdx <= 0 && PlayerPrefs.GetInt("BoostLevel") > 8)
        {
            m_gemTimerIdx = 25;
            return true;
        }
        return false;
    }

    float m_currentX;
    int m_targetIndex;
    int m_curIndex;
    float CAR_DISTANCE = 80f;

    public void SpawnInitialLevel()
    {
        m_currentX = -1200;

        for (int x = 0; x < m_currentLevel; x++)
        {
            SpawnLevel();
        }


    }

    bool isBlock = false;

    public List<int> m_listOfXPos;


    public int m_totalCarsSpawned;


    public void SpawnLevel()
    {
        GameObject obj;
        // int xOffset = GetXOffset();

        int randVal = Random.Range(0, 100);

        if (WillSpawGem())
        {
            SpawnerManager.Instance.Spawn(OBJ_TYPE.BOOST_GEM, new Vector3(m_currentX, 0, GetXOffset()));
        }
        else if (randVal < 12)
        {
            SpawnerManager.Instance.Spawn(OBJ_TYPE.COIN, new Vector3(m_currentX, 0, GetXOffset()));
        }
        else
        {
            m_totalCarsSpawned++; 
            int randomEnemyType = Random.Range(0, 100);
            if (WillSpawnMovingCar())
            {
                obj = SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_MOVING, new Vector3(m_currentX, 0, GetXOffset()));
                obj.GetComponent<MovingCar>().Initialize(m_listOfXPos);
            }
            else if (randomEnemyType > 50)
            {
                SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_PURPLE, new Vector3(m_currentX, 0, GetXOffset()));
            }
            else
            {
                SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_GREEN, new Vector3(m_currentX, 0, GetXOffset()));
            }
        }



        int randomDistance = Random.Range(0, 100);
        if (isBlock || randomDistance > 85)
        {
            m_currentX -= CAR_DISTANCE;
            m_currentX -= CAR_DISTANCE;
            ResetAvailablePosition();
            isBlock = false;
        }
        else if (randomDistance > 70)
        {
            m_currentX -= CAR_DISTANCE;
        }
        else
        {
            isBlock = true;
        }

        m_currentX++;
    }

    public override void Gamescene_OpenShop()
    {
        m_floorAnimationMgr.StopAnimation();
    }

    private bool WillSpawnMovingCar()
    {
        if (m_curIndex >= m_targetIndex && m_listOfXPos.Contains(0) && GameScene.instance.GetBestScore() > 10)
        {
            int min = 7;
            int max = 14;

            if (GameScene.instance.GetScore() > 150)
            {
                min = 5;
                max = 10;
            }

            else if (GameScene.instance.GetScore() > 250)
            {
                min = 3;
                max = 7;
            }
            else
            {
                min = 7;
                max = 14;
            }

            m_targetIndex = Random.Range(min, max);
            isBlock = true;
            m_curIndex = 0;
            return true;
        }
        else
        {
            m_curIndex++;
            return false;
        }
    }



    private int GetXOffset()
    {
        if (m_listOfXPos.Count < 1)
        {
            return 10000;
        }

        int randPos = Random.Range(0, m_listOfXPos.Count);
        int retVal = m_listOfXPos[randPos];
        m_listOfXPos.Remove(retVal);

        return retVal;
    }

    private void ResetAvailablePosition()
    {
        m_listOfXPos.Clear();
        m_listOfXPos.Add(10);
        m_listOfXPos.Add(0);
        m_listOfXPos.Add(-10);
    }

    public int m_currentLevel;
    public override void Gamescene_Setuplevel(bool hardreset)
    {
        ZCameraMgr.instance.GetComponent<Camera>().fieldOfView = 37.1f;
        ResetAvailablePosition();
        GameScene.instance.m_textBest.text = "LEVEL " + PlayerPrefs.GetInt("BoostLevel");
        m_currentLevel = PlayerPrefs.GetInt("NumberOfCarsToSpawn");
        GameScene.instance.m_currentScore = m_currentLevel;

        GameScene.instance.ShowScore(m_currentLevel);
        m_totalCarsSpawned = 0;
        SpawnInitialLevel();
        BBColorManager.Instance.ResetGameColor();
        SpawnerManager.Instance.DisableHighlight();
        m_objectFloor.gameObject.SetActive(true);

        m_objectCar.transform.localPosition = Vector3.zero;
        m_objectCar.transform.localEulerAngles = Vector3.zero;
        m_objectCar.GetComponent<Rigidbody>().isKinematic = false;

        Camera.main.transform.localPosition = new Vector3(Camera.main.transform.localPosition.x, Camera.main.transform.localPosition.y, m_objectCar.transform.localPosition.z - 100f);

        m_objectCar.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation; ;
        m_eState = BBStateEnum.TITLE_SCREEN;

        m_objectCar.GetComponent<BBCar>().Reset();
        m_floorAnimationMgr.StopAnimation();

        m_reachedFinishLine = false;
    }

    public override void Gamescene_Unloadlevel()
    {
        m_objectFloor.gameObject.SetActive(false);
        m_objFinishLine.gameObject.SetActive(false);
    }

}
