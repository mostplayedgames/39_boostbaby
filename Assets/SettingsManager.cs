﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

	[SerializeField]
	private Image m_imgSfx;
	[SerializeField]
	private Image m_imgBgm;
	[SerializeField]
	private Sprite m_sprEnabled;
	[SerializeField]
	private Sprite m_sprDisabled;
	[SerializeField]
	private AudioClip m_audioBtn;

	private bool showSettings;
	private bool isSfxEnabled;
	private bool isBgmEnabled;

	void Awake()
	{
		showSettings = false;
		isSfxEnabled = (PlayerPrefs.GetInt ("isSfxEnabled") == 1) ? false : true;
		isBgmEnabled = (PlayerPrefs.GetInt ("isBgmEnabled") == 1) ? false : true;

		EnableSfx ();
		EnableBgm ();
	}

	public void BtnCall_ShowSettings()
	{
		showSettings = (showSettings);
		m_imgSfx.gameObject.SetActive (true);
		m_imgBgm.gameObject.SetActive (true);
	}

	public void EnableSfx()
	{
		isSfxEnabled = (isSfxEnabled) ? !isSfxEnabled : isSfxEnabled;
		int saveFile = (isBgmEnabled) ? 1 : 0;
		Sprite tempSprite = (isSfxEnabled) ? m_sprEnabled : m_sprDisabled; 

		m_imgSfx.sprite = tempSprite;

		PlayerPrefs.SetInt ("isSfxEnabled", saveFile);
	}

	public void EnableBgm()
	{
		isBgmEnabled = (isBgmEnabled) ? !isBgmEnabled : isBgmEnabled;
		int saveFile = (isBgmEnabled) ? 1 : 0;
		Sprite tempSprite = (isBgmEnabled) ? m_sprEnabled : m_sprDisabled; 

		m_imgBgm.sprite = tempSprite;

		PlayerPrefs.SetInt ("isBgmEnabled", saveFile);
	}
}
