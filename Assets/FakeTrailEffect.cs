﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeTrailEffect : MonoBehaviour {

	public void Awake()
	{
		float speed = Random.Range (0.1f, 0.7f); 
		LeanTween.cancel (this.gameObject);
		LeanTween.moveLocalY (this.gameObject, this.transform.localPosition.y - 0.15f, speed).setLoopPingPong ().setEase(LeanTweenType.easeShake);
	}
}
