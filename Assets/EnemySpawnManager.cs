﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySpawnManager : MonoBehaviour 
{
    public const int CAR_SPACES = 100;

    // 1 As Normal Car
    // 0.5 As Highway Mid
    // 2 As Moving Car

    public Vector3[,] m_tutorialTuning = new Vector3[,]
    {
        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },
    };


    public Vector3[,] m_easyTuningVec = new Vector3[,]
    {
        { 
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },
    };

    public Vector3[,] m_tempoTuningVec = new Vector3[,]
    {
        {
            new Vector3(1,0,1),
            new Vector3(0,1,1),
            new Vector3(1,0,1)
        },

        {
            new Vector3(1,1,0),
            new Vector3(1,0,1),
            new Vector3(1,1,0)
        },

        {
            new Vector3(1,1,0),
            new Vector3(1,1,0),
            new Vector3(0,1,1)
        },

        {
            new Vector3(0,1,1),
            new Vector3(1,0,0),
            new Vector3(0,1,1)
        },

        {
            new Vector3(0,1,0),
            new Vector3(1,0,1),
            new Vector3(0,1,0)
        },

        {
            new Vector3(1,0,1),
            new Vector3(0,1,0),
            new Vector3(1,0,1)
        },
    };


    public Vector3[,] m_medTuningVec = new Vector3[,]
    {
        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },
    };

    public Vector3[,] m_hardTuningVec = new Vector3[,]
    {
        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },

        {
            new Vector3(0,0,0),
            new Vector3(0,0,0),
            new Vector3(0,0,0)
        },
    };

    private GameObject m_prefabMpvingCar;
    private GameObject m_prefabNormalCar;

    private const int LIMIT = 3;
    private int m_curTuningIdx;
    private int m_curSubTuningIdx;
    private Vector3[,] m_currentTuning;

    public void SpawnCarLane()
    {
        Vector3 carLane = m_currentTuning[m_curTuningIdx, m_curSubTuningIdx];

        float leftLane = carLane.x;
        float centerLane = carLane.y;
        float rightLane = carLane.z;

        if(leftLane > 0)
        {

        }

        else if(centerLane > 0)
        {

        }

        else if(rightLane > 0)
        {

        }
    }

    private void CheckObjectToSpawn(int p_test)
    {
        if(p_test > 1)
        {
            // Spawn Normal Car
        }
        else
        {
            // Spawn Moving Car
        }
    }

    private void SpawnCar()
    {

    }
}
