﻿                                                                                    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum BBStateEnum
{
    TITLE_SCREEN,
    IN_GAME,
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE,
    WATCH_TO_CONTINUE
}

public class BBEndlessMode : FDGamesceneInstance
{
    public static BBEndlessMode instance;
    public BBStateEnum m_eState;
    public GameObject m_objectCar;
    public GameObject m_objectFloor;

    private FloorManager m_floorAnimationMgr;
    public MaterialOffseter m_road;
    public MaterialOffseter m_sideRoadLeft;
    public MaterialOffseter m_sideRoadRight;

    private float m_gemTimerIdx = 25;

    void Awake()
    {
        instance = this;
        m_floorAnimationMgr = this.GetComponent<FloorManager>();
    }

    private Vector3 m_vecInit;
    private float m_initTouchX;
    private Vector3 m_latePos;
    public override void Gamescene_Update ()
    {
        if (!m_objectCar.GetComponent<BBCar>().m_isBoost)
            m_gemTimerIdx -= 1 * Time.deltaTime;
        
        m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, 0, m_objectCar.transform.localPosition.z);

         
        if (Input.touchCount < 2)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_initTouchX = Input.mousePosition.x;
                m_vecInit = m_objectCar.transform.localPosition;
                m_latePos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                float deltaPosX = (Input.mousePosition.x - m_initTouchX);
                m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, 0, (m_vecInit.z + (deltaPosX * 0.07f)));

                if (m_objectCar.transform.localPosition.z >= 12.6f)
                    m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, 12.6f);
                if (m_objectCar.transform.localPosition.z <= -12.6f)
                    m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, -12.6f);

                m_latePos = Input.mousePosition;
            }
        }
    }

    public override void Gamescene_ButtonDown()
    {
        BBCar.Instance.StartCar();
        GameScene.instance.m_textBest.text = "SCORE";
        GameScene.instance.m_textScore.text = "0";
        GameScene.instance.m_currentScore = 0;

    }

    public override void Gamescene_Score ()
	{
		if (GameScene.instance.GetScore () > 15)
			SpawnLevel ();


        if (GameScene.instance.m_currentScore % 30 == 0 && GameScene.instance.m_currentScore > 0)
        {
            BBColorManager.Instance.ChangeColor();
        }

        if(GameScene.instance.m_currentScore % 50 == 0)
        {
            if (CAR_DISTANCE > 70)
            {
                CAR_DISTANCE -= 10;
            }
        }

     //   LapsCounter.Instance.UpdateScaleBar();
	}

    public override void Gamescene_OpenShop()
    {
        m_floorAnimationMgr.StopAnimation();
    }

    public override void Gamescene_Die ()
	{
        m_floorAnimationMgr.StopAnimation();
	}

    public bool WillSpawGem()
    {
        if (m_gemTimerIdx <= 0)
        {
            m_gemTimerIdx = 15;
            return true;
        }
        return false;
    }

	float m_currentX;
	int m_targetIndex;
    int m_curIndex;
	float CAR_DISTANCE = 100f;
	//float CAR_DISTANCE = 200f;

	public void SpawnInitialLevel()
	{
		m_currentX = -1200;

		for (int x = 0; x < 50; x++) {
			SpawnLevel ();
		}
	}

	bool isBlock = false;

    public List<int> m_listOfXPos;
    private bool m_spawnedMovingCar;

	public void SpawnLevel()
	{
		GameObject obj;
       // int xOffset = GetXOffset();

        int randVal = Random.Range(0, 100);

        if (WillSpawGem()) 
        {
            SpawnerManager.Instance.Spawn(OBJ_TYPE.BOOST_GEM, new Vector3(m_currentX, 0, GetXOffset()));

            //obj = ZObjectMgr.Instance.Spawn3D (m_prefabGem.name, new Vector3 (m_currentX, 0, GetXOffset()));
		}
        else if(randVal < 10)
        {
            SpawnerManager.Instance.Spawn(OBJ_TYPE.COIN, new Vector3(m_currentX, 0, GetXOffset()));

           // obj = ZObjectMgr.Instance.Spawn3D(m_prefabCoin.name, new Vector3(m_currentX, 0, GetXOffset()));
        }
        else 
        {
			int randomEnemyType = Random.Range (0, 100);
            if (WillSpawnMovingCar()) 
            {
                int xOffset = GetXOffset();
                obj = SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_MOVING, new Vector3(m_currentX, 0, xOffset));
                obj.GetComponent<MovingCar>().Initialize(m_listOfXPos);
                m_listOfXPos.Clear();
                isBlock = true;
            }
            else if (randomEnemyType > 50) 
            {
                SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_PURPLE, new Vector3(m_currentX, 0, GetXOffset()));
			} 
            else//if (randomEnemyType >15)
            {
                SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_GREEN, new Vector3(m_currentX, 0, GetXOffset()));
			}
            //else if(m_listOfXPos.Count > 2 && randomEnemyType > 0)
            //{
            //    float val = Random.Range(0, 100);
            //    float randPos = 0;
            //    if(val >50)
            //    {
            //        randPos = 5;
            //    }
            //    else
            //    {
            //        randPos = -5;
            //    }
            //    SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_GREEN, new Vector3(m_currentX, 0, randPos));
            //    isBlock = true;
            //}
        }



		int randomDistance = Random.Range (0, 100);
		if (isBlock || randomDistance > 95) //85
        {
			m_currentX -= CAR_DISTANCE;
			m_currentX -= CAR_DISTANCE;
            ResetAvailablePosition();
			isBlock = false;
		} 
        else if (randomDistance > 80)//70 
        {
			m_currentX -= CAR_DISTANCE;
		}
        else 
        {
			isBlock = true;
		}

        //if(m_spawnedMovingCar)
        //{
        //    m_spawnedMovingCar = false;
        //    isBlock = false;
        //}

        m_currentX++;
	}

    public override void Gamescene_ShowWatchAdsToContinue()
    {
        m_floorAnimationMgr.StopAnimation();
    }

    public override void GameScene_WatchAdsContinue()
    {
        m_floorAnimationMgr.StartAnimation(8f);
        BBCar.Instance.ReviveCar();
        BBCar.Instance.ActivateBoost();
        GameScene.instance.m_eState = GAME_STATE.IN_GAME;
    }

    public override void GameScene_WatchAdsCancel()
    {
    }

    private bool WillSpawnMovingCar()
    {
        if (m_curIndex >= m_targetIndex && m_listOfXPos.Contains(0) && GameScene.instance.GetScore() > 0)//m_listOfXPos.Contains(0
        {
            int min = 7;
            int max = 14;

            if(GameScene.instance.GetScore() > 150)
            {
                min = 5;
                max = 10;
            }
            else if(GameScene.instance.GetScore() > 250)
            {
                min = 3;
                max = 7;
            }
            else
            {
                min = 7;
                max = 14;
            }

            m_targetIndex = Random.Range(min, max);
            m_curIndex = 0;
            return true;
        }
        else
        {
            m_curIndex++;
            return false;
        }
    }



    private int GetXOffset()
    {
        if(m_listOfXPos.Count< 1)
        {
            return 10000;
        }

        int randPos = Random.Range(0, m_listOfXPos.Count);
        int retVal = m_listOfXPos[randPos];
        m_listOfXPos.Remove(retVal);

        return retVal;
    }

    private void ResetAvailablePosition()
    {
        m_listOfXPos.Clear();
        m_listOfXPos.Add(10);
        m_listOfXPos.Add(0);
        m_listOfXPos.Add(-10);
    }

	public override void Gamescene_Setuplevel(bool hardreset) 
	{
        ZCameraMgr.instance.GetComponent<Camera>().fieldOfView = 37.1f;

        ResetAvailablePosition();

        CAR_DISTANCE = 100;

        SpawnInitialLevel ();
       // LapsCounter.Instance.Initialize(); 
        BBColorManager.Instance.ResetGameColor();
        SpawnerManager.Instance.DisableHighlight();
        //DisableHighlight();
        m_objectFloor.gameObject.SetActive(true);

		m_objectCar.transform.localPosition = Vector3.zero;
		m_objectCar.transform.localEulerAngles = Vector3.zero;
		m_objectCar.GetComponent<Rigidbody> ().isKinematic = false;

        Camera.main.transform.localPosition = new Vector3(Camera.main.transform.localPosition.x, Camera.main.transform.localPosition.y, m_objectCar.transform.localPosition.z - 100f);

		m_objectCar.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;;
		m_objectCar.GetComponent<BBCar> ().Reset ();

        if (m_objectFloor.activeSelf)
        {
            m_floorAnimationMgr.StartAnimation(8);
        }
        m_eState = BBStateEnum.TITLE_SCREEN;
	}

	public override void Gamescene_Unloadlevel() 
	{
        m_objectFloor.gameObject.SetActive(false);
	}

}
