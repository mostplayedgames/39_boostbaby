﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapsCounter : ZSingleton<LapsCounter> {

	public Text m_textCurLevel;
	public Text m_textNextLevel;
	public Text m_textCurScore;
	public Image m_imgFillBar;
	public GameObject m_objLevelUpBanner;
	public AudioClip m_audioLevelUp;

	private int m_curLevel = 1;
	private float m_targetDistance;
	private float m_curDistance;

	public void Awake()
	{
		if (!PlayerPrefs.HasKey ("CurLap")) 
		{
			m_curLevel = 1;
			PlayerPrefs.SetInt ("CurLap", 1);
		}

		if (!PlayerPrefs.HasKey ("CurDist")) {
			PlayerPrefs.SetInt ("CurDist", 1);
		}
	}

	public void Initialize()
	{
		m_curLevel = PlayerPrefs.GetInt ("CurLap");

		m_curDistance = PlayerPrefs.GetInt("CurDist");
		m_targetDistance = 50;
		m_imgFillBar.fillAmount = 0;

		m_textCurLevel.text = "" + m_curLevel;
		m_textNextLevel.text = "" + (m_curLevel + 1);

        m_objLevelUpBanner.gameObject.SetActive(false);
	}

	public void Update()
	{
		//if (BBLevelMode.instance.m_eState == BBStateEnum.TITLE_SCREEN ||
			//BBLevelMode.instance.m_eState == BBStateEnum.WATCH_TO_CONTINUE ||
			//BBLevelMode.instance.m_eState == BBStateEnum.RESULTS)
			//return;

		//UpdateScaleBar_TimeBased ();
	}

	public void UpdateScaleBar()
	{
		m_curDistance++;

		if (m_curDistance >= m_targetDistance) 
		{
			UpdateLevel ();
			//ScaleBar ();
			ZAudioMgr.Instance.PlaySFX (m_audioLevelUp);
		}

		m_textCurScore.text = "" + m_curDistance + "/" + m_targetDistance;
		float fillAmount = Mathf.Min (1, m_curDistance * 1.0f / m_targetDistance);
		m_imgFillBar.fillAmount = fillAmount;
	}

	public void UpdateLevel()
	{
		m_curLevel++;
		m_textNextLevel.text = "" + (m_curLevel + 1);
		m_textCurLevel.text = "" + m_curLevel;
		m_curDistance = 0;
		m_targetDistance = 50;
		PlayerPrefs.SetInt ("CurrentLap", m_curLevel);
		AnimateLevelUpSign ();
        //BBColorManager.Instance.ChangeColor();
	}

	private void AnimateLevelUpSign()
	{
		m_objLevelUpBanner.gameObject.SetActive (true);
		m_objLevelUpBanner.transform.localScale = Vector3.zero;
		LeanTween.cancel (m_objLevelUpBanner.gameObject);
        LeanTween.scale (m_objLevelUpBanner.gameObject, new Vector3(1.75f,1.75f,1.75f), 0.25f).setEase(LeanTweenType.easeOutQuad).setOnComplete (Delay);
	}

	private void Delay()
	{
        LeanTween.scale (m_objLevelUpBanner.gameObject, m_objLevelUpBanner.transform.localScale, 1f).setEase(LeanTweenType.easeOutQuad).setOnComplete (ShrinkLevelupSign);

	}

	private void ShrinkLevelupSign()
	{
        //m_objLevelUpBanner.gameObject.SetActive(false);
		LeanTween.scale (m_objLevelUpBanner.gameObject, Vector3.zero, 0.25f).setEase(LeanTweenType.easeOutQuad).setOnComplete (DeactivateLevelUpSign);
	}

	private void DeactivateLevelUpSign()
	{
		m_objLevelUpBanner.gameObject.SetActive (false);
	}
		

	public void ScaleBar()
	{
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale + new Vector3 (0.1f, 0.1f, 0.1f), 0.5f).setLoopPingPong ().setLoopOnce ().setEase(LeanTweenType.easeInExpo);
	}

}
