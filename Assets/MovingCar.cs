﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCar : MonoBehaviour 
{
    //public GameObject m_objArrow;
    public AudioClip m_audioMoveSfx;
    public GameObject m_objLeftSignalLight;
    public GameObject m_objRightSignalLight;
    public List<AudioClip> m_audioMoving;
    public List<int> debugInt;

    public float m_targetPosX;
    public float m_curretPosZ;

    public int min;
    public int max;
    public int movementSpeed;
    public float speed;

    // 0 GO LEFT
    // 1 GO RIGHT
    // 2 BOTH

    public void Initialize(List<int> p_listOfXpos)
	{
        m_curretPosZ = this.transform.localPosition.z;
        debugInt = p_listOfXpos;

        if (GameScene.instance.GetCurrentMode() == 2)
        {
            min = -5;
            max = 5;
            speed = 0.5f;
        }
        else
        {
            min = -10;
            max = 10;
            speed = Random.Range(0.5f, 1);
        }

        if(GameScene.instance.GetBestScore() < 20)
        {
            speed = 2f;
        }


        if (m_curretPosZ == max) {
			SetToMoveLeft ();
        } 
        else if (m_curretPosZ == min) {
			SetToMoveRight ();
        } 
        else if (m_curretPosZ == 0) 
        {

            if (p_listOfXpos.Contains(min) && !p_listOfXpos.Contains(max))
            {
                SetToMoveLeft();
            }
            else if(p_listOfXpos.Contains(min) && !p_listOfXpos.Contains(max))
            {
                SetToMoveRight();
            }
            else
            {
                SetToMoveAwayFromCenter();
            }
		}
	}

	public void MoveTruck()
	{
		LeanTween.cancel (this.gameObject);
        LeanTween.moveLocalZ(this.gameObject, m_targetPosX, speed).setEase (LeanTweenType.easeInOutQuart);
	}

	private void SetToMoveAwayFromCenter()
	{
		int randVal = Random.Range (0, 100);
		if (randVal > 50) {
			SetToMoveRight ();
		} else {
			SetToMoveLeft ();
		}
	}

    private void SetToMoveRight()
    {
        float randVal = Random.Range(5, 11);
        m_targetPosX = m_curretPosZ + 10;//10
        m_objRightSignalLight.gameObject.SetActive(true);
        m_objLeftSignalLight.gameObject.SetActive(false);
        //m_objArrow.gameObject.transform.localEulerAngles = new Vector3 (0, m_objArrow.transform.localPosition.y, m_objArrow.transform.localPosition.z);
    }


	private void SetToMoveLeft()
	{
        float randVal = Random.Range(5, 11);
        m_targetPosX = m_curretPosZ - 10;//10
        m_objRightSignalLight.gameObject.SetActive(false);
        m_objLeftSignalLight.gameObject.SetActive(true);
		//m_objArrow.gameObject.transform.localEulerAngles = new Vector3 (180, m_objArrow.transform.localPosition.y, m_objArrow.transform.localPosition.z);
	}
}
