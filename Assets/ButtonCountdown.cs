﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCountdown : MonoBehaviour {

	public Text m_textCountdown;
	public float m_maxCountdownValue;
	private float m_curCountdownValue;
	public GameObject m_btnNoThanks;
	
	void Update () 
	{
		if (!this.gameObject.activeSelf)
			return;
		
		if (m_curCountdownValue > 0.8f) {

			if (m_curCountdownValue < 7 && !m_btnNoThanks.activeSelf) {
				m_btnNoThanks.gameObject.SetActive (true);
			}

			m_curCountdownValue -= Time.deltaTime;
			m_textCountdown.text = "Continue ? " + (int)m_curCountdownValue;
		}
		else {
			ResetButton ();
			this.gameObject.SetActive (false);
            GameScene.instance.m_eState = GAME_STATE.TITLE_SCREEN;
			GameScene.instance.Die ();
		}
	}
     
	void OnEnable()
	{
		ResetButton ();
	}

    private void OnDisable()
    {
        m_btnNoThanks.gameObject.SetActive(false);
    }

    private void ResetButton()
	{
		m_curCountdownValue = m_maxCountdownValue;
	}
}
