﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextEffect : MonoBehaviour {

	public GameObject m_objText;
	public ParticleSystem m_particle;

	public void Initialize(Vector3 p_otherPos)
	{
        //Vector3 test = new Vector3(10.1f, 0.1f, 2989.3f);

        Vector3 WorldPos = Camera.main.WorldToViewportPoint (p_otherPos);
        float x = 0;

       // Debug.Log("WorldPos: " + );


        if(p_otherPos.z <= -5)
        {
            x = Random.Range(0.25f, 0.4f);
        }
        else if(p_otherPos.z > -5 && p_otherPos.z < 5)
        {
            x = Random.Range(0.4f, 0.6f);
        }
        else if(p_otherPos.z >= 5)
        {
            x = Random.Range(0.6f, 0.75f);
        }

        //0.45f
        Vector3 test2 = new Vector3(x, 0.45f, 0);

        this.gameObject.SetActive(true);
        this.transform.localScale = new Vector3(1, 1, 1);
        // this.transform.position = Vector3.zero;
        //Debug.Log("START: " + this.gameObject.name + "_" + x+ "_"+ p_otherPos);
        //Debug.Log("B4: " + this.GetComponent<RectTransform>().localPosition);
        this.GetComponent<RectTransform>().localPosition = Vector3.zero;
        this.GetComponent<RectTransform> ().anchorMin = test2;
        this.GetComponent<RectTransform> ().anchorMax = test2;
      //  this.GetComponent<RectTransform>().localPosition = Vector3.zero;
        //Debug.Log("ATR: " + this.GetComponent<RectTransform>().localPosition);



        //Debug.Log("Cale2" + this.transform.localScale);

        //Vector3 camPos = Camera.main.WorldToScreenPoint(p_otherPos);
        //this.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(camPos.x - 500, 0, 0);

		m_particle.Stop ();
		m_particle.Play ();

		StartAnimateText ();
        //Debug.Log("NAME: " + this.gameObject.name + "_" + this.transform.GetComponent<RectTransform>().localPosition);
       // Debug.Log("END~~~~~");
	}

    public void Update()
    {
      //  Initialize(BBCar.Instance.transform.position);
    }

    private void StartAnimateText()
	{
		m_objText.transform.localPosition = Vector3.zero;
		m_objText.transform.localScale = Vector3.zero;

		LeanTween.cancel (m_objText.gameObject);
		LeanTween.moveLocalY (m_objText.gameObject, m_objText.transform.localPosition.y + 50, 0.3f).setEase (LeanTweenType.easeOutExpo).setOnComplete (Delay);
		LeanTween.scale (m_objText.gameObject, new Vector3 (1, 1, 1), 0.1f).setEase (LeanTweenType.easeOutExpo);
	}

	private void Delay()
	{
        LeanTween.moveLocalY(m_objText.gameObject, m_objText.transform.localPosition.y - 50, 0.3f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (m_objText.gameObject, m_objText.transform.localScale, 0.2f).setOnComplete(Shrink);
	}

	private void Shrink()
	{
        LeanTween.scale(m_objText.gameObject, Vector3.zero, 0.1f);//.setOnComplete(DelayReset);

	}

    //private void DelayReset()
    //{
    //    LeanTween.scale(m_objText.gameObject, Vector3.zero, 0.5f).setOnComplete(DelayReset);
    //}

    //private void ResetEffect()
    //{
    //    this.transform.localPosition = Vector3.zero;
    //    this.GetComponent<RectTransform>().localPosition = Vector3.zero;
    //    this.gameObject.SetActive(false);
    //}
}
