﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum FDShootOut_State
{
	START,
	COUNTDOWN,
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE,
	RESTART
}

[System.Serializable]
public struct CameraStats
{
	public Vector3 Position;
	public Vector3 Rotation;
	public float CameraSize;
	public float Near;
	public float Far;
}

public enum GAME_TYPE
{
	ENDLESS,
	LEVEL
}

public class GameData
{
	public FDCharacter Character;
	public string GameName;
	public GAME_TYPE GameType;
	public string AndroidID;
	public string iOSID;
	public int Score;
	public CameraStats CameraStats;
}

public class FDGamesceneInstance : MonoBehaviour 
{
	//private GameData m_gameData;
	public FDCharacter Character;
	public string GameName;
	public GAME_TYPE GameType;
	private string AndroidID;
	private string iOSID;
	private int Score;
	private int Attempts;
	public CameraStats CameraStats;

	// Use this for initialization
	void Start () 
	{ 
	}

	void Update () {}
	public virtual void Gamescene_Update () {}
	public virtual void Gamescene_ButtonDown () {}
	public virtual void Gamescene_ButtonUp () {}
	public virtual void Gamescene_Die () {} 
	public virtual void Gamescene_Score () {}
	public virtual void Gamescene_Setuplevel(bool hardreset=false) {}
	public virtual void Gamescene_Unloadlevel() {}
    public virtual void Gamescene_OpenShop() {}

    public virtual void Gamescene_ShowWatchAdsToContinue() {}
    public virtual void GameScene_WatchAdsContinue() {}
    public virtual void GameScene_WatchAdsCancel() {}

	public int GetBestScore()
	{
		string gameName = GameName;
		if (GameType == GAME_TYPE.LEVEL && !PlayerPrefs.HasKey ("Score_" + gameName)) 
		{
			PlayerPrefs.SetInt ("Score_" + gameName, 1);
		}
		
		return PlayerPrefs.GetInt ("Score_" + gameName);
	}

	public void UpdateAttempts(int p_resetAttempt = 1)
	{
		Attempts = PlayerPrefs.GetInt ("Attempts_" + GameName);

		if (p_resetAttempt < 1) {
			Attempts = 0;
		} else {
			Attempts++;
		}
		PlayerPrefs.SetInt ("Attempts_" + GameName, Attempts);
	}

	public int GetTotalAttempts()
	{
		return PlayerPrefs.GetInt ("Attempts_" + GameName);
	}

	public void UpdateScore(int p_score, bool p_overide = false)
	{
		int bestScore = GetBestScore ();
		if (bestScore < p_score || p_overide) 
		{
			PlayerPrefs.SetInt ("Score_" + GameName, p_score);
		}
	}

	public void ResetScore()
	{
		PlayerPrefs.DeleteKey ("Score_" + GameName);
	}

	public void UpdateCamera(CameraStats p_cameraStats)
	{
		Camera.main.transform.position = p_cameraStats.Position;
		Camera.main.transform.eulerAngles = p_cameraStats.Rotation;
		Camera.main.nearClipPlane = p_cameraStats.Near;
		Camera.main.farClipPlane = p_cameraStats.Far;
		Camera.main.orthographicSize = p_cameraStats.CameraSize;
	}


	protected bool IsButtonPressed()
	{
		//return false;
		string objPressed = "TouchInputManager";
		if (EventSystem.current.IsPointerOverGameObject (0) || EventSystem.current.IsPointerOverGameObject(-1)) {
			if (EventSystem.current.currentSelectedGameObject != null) {
				objPressed = EventSystem.current.currentSelectedGameObject.name;
			}
		}

		if (objPressed == "TouchInputManager") 
		{
			return false;
		} 
		else 
		{
			return true;
		}
	}

	// Checks if game is a level based mode and new data to prevent game from starting to 0
//	private bool IsNewGameData()
//	{
//		return (GameType == GAME_TYPE.LEVEL && !PlayerPrefs.HasKey ("Score_" + GameName)) ? true: false;
//	}
}
