﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagAnimation : MonoBehaviour {

    public bool m_reverse;

    public void Awake()
    {
        if(m_reverse)
        {
            RotateRight();    
        }
        else
        {
            RotateLeft();
        }
    }

    public void RotateRight()
    {
        Vector3 scale = this.transform.localScale;
       // this.transform.localScale = new Vector3(scale.x * -1f, scale.y, scale.z);

        //Vector3 scale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
        //LeanTween.cancel(this.gameObject);

        //LeanTween.scale(this.gameObject, new Vector3(scale.x * -1f, scale.y, scale.z), 0.25f).setEase(LeanTweenType.easeOutSine);

        LeanTween.rotateAroundLocal(this.gameObject, new Vector3(0, 0, 1), 100f, 0.25f).setOnComplete(RotateFlagRight).setEase(LeanTweenType.easeInOutSine);
    }

    public void RotateFlagRight()
    {
        Vector3 scale = new Vector3(this.transform.localScale.x * -1f, this.transform.localScale.y, this.transform.localScale.z);
        LeanTween.scale(this.gameObject, scale , 0.2f).setOnComplete(RotateLeft).setEase(LeanTweenType.easeInOutSine);
    }

    public void RotateLeft()
    {
        Vector3 scale = this.transform.localScale;
        //this.transform.localScale = new Vector3(scale.x * -1f, scale.y, scale.z);

        //Vector3 scale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
        LeanTween.cancel(this.gameObject);

       // LeanTween.scale(this.gameObject, new Vector3(scale.x * -1f, scale.y, scale.z), 0.25f).setEase(LeanTweenType.easeOutSine);

        LeanTween.rotateAroundLocal(this.gameObject, new Vector3(0, 0, 1), -100f, 0.25f).setOnComplete(RotateFlagLeft).setEase(LeanTweenType.easeInOutSine);
    }

    public void RotateFlagLeft()
    {
        Vector3 scale = new Vector3(this.transform.localScale.x * -1f, this.transform.localScale.y, this.transform.localScale.z);
        this.transform.localScale = new Vector3(scale.x * -1f, scale.y, scale.z);
        LeanTween.scale(this.gameObject, scale, 0.2f).setOnComplete(RotateRight).setEase(LeanTweenType.easeInOutSine);;
    }
	
	// Update is called once per frame
	void Update () 
    {
		
	}
}
