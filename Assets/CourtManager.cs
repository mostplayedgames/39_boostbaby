﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CourtData
{
	public Texture CourtTexture;
	public Color CourtColor;
	public Color ModeTextColor;
	public Texture CrowdTex1;
	public Texture CrowdTex2;
	public Sprite TitleScreen;
}

public class CourtManager : ZSingleton<CourtManager> 
{
	public List<CourtData> m_listOfCourtData = new List<CourtData>();
	public GameObject m_objCourt;
	public Text m_textModeIndicator;
	public List<GameObject> m_objCrowd = new List<GameObject> ();
	public List<Image> m_titleScreen = new List<Image>();

	private Texture m_crowdTex1;
	private Texture m_crowdTex2;
	private float m_crowdAnimSpeed;

	public void Awake()
	{
		AnimateCrowd (1);
	}

	public void SetupCourt(int p_courtIdx)
	{
		Camera.main.backgroundColor = m_listOfCourtData [p_courtIdx].CourtColor;
		m_crowdTex1 = m_listOfCourtData [p_courtIdx].CrowdTex1;
		m_crowdTex2 = m_listOfCourtData [p_courtIdx].CrowdTex2;
		m_objCourt.GetComponent<Renderer> ().material.mainTexture = m_listOfCourtData [p_courtIdx].CourtTexture;
		m_textModeIndicator.color = m_listOfCourtData [p_courtIdx].ModeTextColor;

		foreach (Image spr in m_titleScreen) 
		{
			if(spr.gameObject.activeSelf)
				spr.sprite = m_listOfCourtData [p_courtIdx].TitleScreen;
		}
		AnimateCrowd (1);

	}

	public void AnimateCrowd(float p_animSpeed)
	{
		m_crowdAnimSpeed = p_animSpeed;
		AnimCrowdIdle1 ();
	}

	private void AnimCrowdIdle2()
	{
		m_objCrowd [0].GetComponent<Renderer> ().material.mainTexture = m_crowdTex1;
		m_objCrowd [1].GetComponent<Renderer> ().material.mainTexture = m_crowdTex1;
		LeanTween.cancel (m_objCrowd [0].gameObject);
		LeanTween.scale (m_objCrowd [0].gameObject, m_objCrowd[0].transform.localScale, m_crowdAnimSpeed).setOnComplete(AnimCrowdIdle1);
	}

	public void AnimCrowdIdle1()
	{
		m_objCrowd [0].GetComponent<Renderer> ().material.mainTexture = m_crowdTex2;
		m_objCrowd [1].GetComponent<Renderer> ().material.mainTexture = m_crowdTex2;
		LeanTween.cancel (m_objCrowd [0].gameObject);
		LeanTween.scale (m_objCrowd [0].gameObject, m_objCrowd[0].transform.localScale, m_crowdAnimSpeed).setOnComplete(AnimCrowdIdle2);
	}
}
