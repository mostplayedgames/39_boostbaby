﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BBExtremeMode : FDGamesceneInstance
{
    public GameObject m_objectCar;
    public GameObject m_objectFloor;
    public MaterialOffseter m_objFloorPattern;
    public GameObject m_objParticles;

    private Vector3 m_vecInit;
    private FloorManager m_floorAnimationMgr;

    private int m_targetIndex;
    private int m_curIndex;
    private float CAR_DISTANCE = 90f;
    private float m_currentX;
    private float m_initTouchX;
    private bool isBlock = false;
    private List<int> m_listOfXPos = new List<int>();
    private float m_gemTimerIdx = 25;
    private int m_carInSameLine;
    private int m_prevX;


    public void Awake()
    {
        m_floorAnimationMgr = this.GetComponent<FloorManager>();
    }

    public override void Gamescene_Update()
    {


        if (!m_objectCar.GetComponent<BBCar>().m_isBoost)
            m_gemTimerIdx -= 1 * Time.deltaTime;

        m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, 0, m_objectCar.transform.localPosition.z);

        if (Input.touchCount < 2)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_initTouchX = Input.mousePosition.x;
                m_vecInit = m_objectCar.transform.localPosition;
            }
            else if (Input.GetMouseButton(0))
            {
                float deltaPosX = (Input.mousePosition.x - m_initTouchX);

                m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, 0, (m_vecInit.z + (deltaPosX * 0.07f)));

                if (m_objectCar.transform.localPosition.z >= 6f)
                    m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, 6f);
                if (m_objectCar.transform.localPosition.z <= -6f)
                    m_objectCar.transform.localPosition = new Vector3(m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, -6f);

            }
        }
    }

    public override void Gamescene_ShowWatchAdsToContinue()
    {
        m_floorAnimationMgr.StopAnimation();
        m_objParticles.gameObject.SetActive(false);
    }

    public override void GameScene_WatchAdsContinue()
    {
        m_floorAnimationMgr.StartAnimation(8f);
        m_objParticles.gameObject.SetActive(true);
    }

    public override void GameScene_WatchAdsCancel()
    {
    }

    public override void Gamescene_ButtonDown()
    {
        BBCar.Instance.StartCar();
        GameScene.instance.m_textBest.text = "SCORE";
        GameScene.instance.m_textScore.text = "0";
        GameScene.instance.m_currentScore = 0;
    }

    public override void Gamescene_Score()
    {
        //if (GameScene.instance.GetScore() > 15)
            SpawnLevel();

        if (GameScene.instance.m_currentScore % 30 == 0 && GameScene.instance.m_currentScore > 0)
        {
            BBColorManager.Instance.ChangeColor();
        }    }

    public override void Gamescene_Die()
    {
        m_floorAnimationMgr.StopAnimation();
       // m_objFloorPattern.Reset();
        m_objParticles.gameObject.SetActive(false);
    }



    public void SpawnInitialLevel()
    {
        m_currentX = -1200;

        for (int x = 0; x < 50; x++)
        {
            SpawnLevel();
        }
    }

    public bool WillSpawGem()
    {
        if (m_gemTimerIdx <= 0)
        {
            m_gemTimerIdx = 25;
            return true;
        }
        return false;
    }


    public void SpawnLevel()
    {
        GameObject obj;

        int randVal = Random.Range(0, 100);
        int xOffset = 0;


        if (WillSpawGem())
        {
            xOffset = GetXOffset();
            SpawnerManager.Instance.Spawn(OBJ_TYPE.BOOST_GEM, new Vector3(m_currentX, 0, xOffset));
        }
        else if (randVal < 12)
        {
            xOffset = GetXOffset();
            SpawnerManager.Instance.Spawn(OBJ_TYPE.COIN, new Vector3(m_currentX, 0, xOffset));
        }
        else
        {
            int randomEnemyType = Random.Range(0, 100);
            if (WillSpawnMovingCar())
            {
                xOffset = GetXOffset();
                obj = SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_MOVING, new Vector3(m_currentX, 0, xOffset));
                obj.GetComponent<MovingCar>().Initialize(m_listOfXPos);
            }
            else if (randomEnemyType > 50)
            {
                xOffset = GetXOffset();
                SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_PURPLE, new Vector3(m_currentX, 0, xOffset));
            }
            else
            {
                xOffset = GetXOffset();
                SpawnerManager.Instance.Spawn(OBJ_TYPE.CAR_GREEN, new Vector3(m_currentX, 0, xOffset));
            }
        }

        int score = GameScene.instance.GetScore();
        if (score < 50)
        {
            CAR_DISTANCE = Random.Range(80, 120);
        }
        else if(score > 100)
        {
            CAR_DISTANCE = Random.Range(90, 100);
        }
        else if(score > 150)
        {
            CAR_DISTANCE = Random.Range(80, 90);
        }

        int randomDistance = Random.Range(0, 100);

        if (randomDistance > 95)
        {
            m_currentX -= CAR_DISTANCE;
            m_currentX -= CAR_DISTANCE;
        }
        else
        {
            m_currentX -= CAR_DISTANCE;
        }

        m_prevX = xOffset;
        ResetAvailablePosition();
    }

    private bool WillSpawnMovingCar()
    {
        if (m_curIndex >= m_targetIndex && GameScene.instance.GetScore() > 15)
        {
            int min = 7;
            int max = 14;

            if (GameScene.instance.GetScore() > 150)
            {
                min = 5;
                max = 10;
            }
            else if (GameScene.instance.GetScore() > 250)
            {
                min = 3;
                max = 7;
            }
            else
            {
                min = 7;
                max = 14;
            }

            m_targetIndex = Random.Range(min, max);
            isBlock = true;
            m_curIndex = 0;
            return true;
        }
        else
        {
            m_curIndex++;
            return false;
        }
    }

    private int prevPosX;
    private int GetXOffset()
    {
        if (m_listOfXPos.Count < 1)
        {
            return 10000;
        }

        int randPos = Random.Range(0, m_listOfXPos.Count);
        int retVal = m_listOfXPos[randPos];
        prevPosX = retVal;

        m_listOfXPos.Remove(retVal);

        if (retVal == m_prevX)
        {
            m_carInSameLine++;
            if (m_carInSameLine > 2)
            {
                retVal *= -1;
                m_carInSameLine = 0;
            }
        }
        else
        {
            m_carInSameLine = 0;
        }

        return retVal;
    }

    private void ResetAvailablePosition()
    {
        m_listOfXPos.Clear();
        m_listOfXPos.Add(5);
        m_listOfXPos.Add(-5);
    }

    public override void Gamescene_Setuplevel(bool hardreset)
    {
        ResetAvailablePosition();

        SpawnInitialLevel();
        BBColorManager.Instance.ResetGameColor();
        SpawnerManager.Instance.DisableHighlight();
        m_objectFloor.gameObject.SetActive(true);

        m_objFloorPattern.StartMoving(8);
        //m_objFloorPattern.StartMoving(8);

        m_objectCar.transform.localPosition = Vector3.zero;
        m_objectCar.transform.localEulerAngles = Vector3.zero;
        m_objectCar.GetComponent<Rigidbody>().isKinematic = false;
        m_objectCar.GetComponent<BBCar>().Reset();
        m_objParticles.gameObject.SetActive(true);

        Camera.main.transform.localPosition = new Vector3(Camera.main.transform.localPosition.x, Camera.main.transform.localPosition.y, m_objectCar.transform.localPosition.z - 100f);

        m_gemTimerIdx = 25;
        m_prevX = -1;
        m_carInSameLine = 0;
        GameScene.instance.m_eState = GAME_STATE.TITLE_SCREEN;
    }

    public override void Gamescene_OpenShop()
    {
        m_floorAnimationMgr.StopAnimation();
        //m_objFloorPattern.Reset();
    }


    public override void Gamescene_Unloadlevel()
    {
        m_objectFloor.gameObject.SetActive(false);

    }

}
