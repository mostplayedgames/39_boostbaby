﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallContainer : MonoBehaviour 
{
	public List<GameObject> m_listOfBalls = new List<GameObject>();

	public void Initialize(int p_ballType)
	{
		for (int i = 0; i < m_listOfBalls.Count; i++) 
		{
			m_listOfBalls [i].GetComponent<SBBall> ().EnableBall (p_ballType);
		}
	}

	public void UpdateBall(int p_score)
	{
		for (int i = 0; i < m_listOfBalls.Count; i++) 
		{
			if (i < p_score) 
			{
				m_listOfBalls [i].gameObject.SetActive (false);
			} else {
				m_listOfBalls[i].gameObject.SetActive(true);
			}
		}
	}
}
