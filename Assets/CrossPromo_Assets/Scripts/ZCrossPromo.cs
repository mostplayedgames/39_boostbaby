﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine.Networking;

[Serializable]
public class AppData
{
	public string app_id;
	public string app_name;
	public string trailer_url;
	public string android_link;
	public string iOS_link;
	public bool isAppInstalled;

	public AppData(string p_appId, string p_appName, string p_trailer_url, string p_android_link, string p_iOS_link)
	{
		app_id = p_appId;
		app_name = p_appName;
		trailer_url = p_trailer_url;
		android_link = p_android_link;
		iOS_link = p_iOS_link;

		isAppInstalled = CheckIfAppIsInstalled (p_appId);
	}

	public bool CheckIfAppIsInstalled(string p_bundleId)
	{
		#if UNITY_EDITOR
		return false;
		#elif UNITY_ANDROID
		Debug.Log("android: CheckIfAppIsInstalled()");

		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");

		AndroidJavaObject launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", p_bundleId);

		if(launchIntent == null)
		{
		Debug.Log("android: False");

		return false;
		}
		else
		{
		Debug.Log("android: True");

		return true;
		}

		#elif UNITY_IPHONE

		int isGameDownloaded = PlayerPrefs.GetInt ("ID_" + p_bundleId); 
		if (isGameDownloaded == 0) 
		{
		return false;
		} 
		else 
		{
		return true;
		}

		#endif

		return false;
		}
}


public enum REQUEST_TYPE
{
	GET_ALL_GAMES,
	GET_CROSS_PROMO
}

public class ZCrossPromo : Singleton<ZCrossPromo> 
{
	#if UNITY_IPHONE
	[DllImport ("__Internal")] private static extern void _OpenAppInStore(int appID);
	#endif

	public delegate void OnRequestRecieved (REQUEST_TYPE p_type);
	public event OnRequestRecieved onRequestReceived;

	public AppData m_curData;

	[SerializeField]
	private GameObject m_crossPromoBanner;

	private bool m_isVideoAvailable;

	public VideoPlayer m_vpCrossPromo;
	public RawImage m_imgCrossPromo;

	public List<AppData> m_listOfCrossPromoGames = new List<AppData>();
	public List<AppData> m_listOfAllGames = new List<AppData>();

	public void Awake()
	{
		m_crossPromoBanner.gameObject.SetActive (false);

//		StartCoroutine ("InstantiateObject");
		Debug.Log ("Awake_CrossPRomo");
	}

	public void OnEnable()
	{
		if (m_vpCrossPromo.isPrepared) 
		{
			m_vpCrossPromo.Play ();
			m_imgCrossPromo.texture = m_vpCrossPromo.texture;	
		}

//		if (m_listOfCrossPromoGames.Count > 0) {
//			StartCoroutine ("InstantiateObject");
//		} else {
//			RequestCrossPromoVideo ();
//		}
	}

	public void OnApplicationFocus()
	{
//		Debug.Log ("OnApplicationFocus");
//
//		foreach(AppData data in m_listOfCrossPromoGames)
//		{
//			if (data.CheckIfAppIsInstalled ()) 
//			{
//				Debug.Log ("Removed: " + data.app_name);
//				m_listOfCrossPromoGames.Remove (data);
//			}
//		}
//
//		if (m_listOfCrossPromoGames.Count > 0) {
//			m_curData = m_listOfCrossPromoGames [0];
//			Debug.Log ("CurData: " + m_curData.app_name);
//		}
//
//		StartCoroutine("InstantiateObject");
	}

	public void Update()
	{
//		if (!m_vpCrossPromo.isPrepared) 
//		{
//		m_crossPromoBanner
//		}
		//Debug.Log ("IsPlayerPrepared" + m_vpCrossPromo.isPrepared);
	}


	public void RequestGameList()
	{
		m_listOfAllGames.Clear ();
		StartCoroutine ("GetAllGames");
	}

	public void RequestCrossPromoVideo(bool p_randFromList = false)
	{
		//m_vpCrossPromo = m_crossPromoBanner.GetComponent<VideoPlayer> ();
		//m_imgCrossPromo = m_crossPromoBanner.GetComponent<RawImage> ();
		//m_vpCrossPromo.Stop ();

		Debug.Log ("RequestCrossPromo Video");
		//StartCoroutine ("downloadAsset");
		StartCoroutine (GetCrossPromoGames(p_randFromList));
	}

		public string tempPath = "";

		IEnumerator downloadAsset()
		{
		Debug.Log ("Time Sta5: " + Time.time);


		string url = "http://ec2-13-58-197-76.us-east-2.compute.amazonaws.com/videos/flippytrailer.ex";

		UnityWebRequest www = UnityWebRequest.Get(url);
		DownloadHandler handle = www.downloadHandler;

		//Send Request and wait
		yield return www.Send();

		if (www.isNetworkError)
		{

		UnityEngine.Debug.Log("Error while Downloading Data: " + www.error);
		}
		else
		{
		UnityEngine.Debug.Log("Success");

		//handle.data

		//Construct path to save it
		string dataFileName = "FlippyBottle";
		tempPath = Path.Combine(Application.persistentDataPath, "AssetData");
		tempPath = Path.Combine(tempPath, dataFileName + ".unity3d");
		Debug.Log ("Time Sta52: " + Time.time);

		//Save
		save(handle.data, tempPath);
		StartCoroutine ("LoadObject");
		Debug.Log ("aaaaEH");
		}


		}

		void save(byte[] data, string path)
		{
		//Create the Directory if it does not exist
		if (!Directory.Exists(Path.GetDirectoryName(path)))
		{
		Directory.CreateDirectory(Path.GetDirectoryName(path));
		}

		try
		{
		File.WriteAllBytes(path, data);
		Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
		}
		catch (Exception e)
		{
		Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
		Debug.LogWarning("Error: " + e.Message);
		}
		}

		IEnumerator LoadObject()
		{
		Debug.Log ("StartLoad");
		AssetBundleCreateRequest bundle = AssetBundle.LoadFromFileAsync(tempPath);
		yield return bundle;

		AssetBundle myLoadedAssetBundle = bundle.assetBundle;
		if (myLoadedAssetBundle == null)
		{
		Debug.Log("Failed to load AssetBundle!");
		yield break;
		}

		AssetBundleRequest request = myLoadedAssetBundle.LoadAssetAsync<VideoClip>("Flippy Bottle Xtreme! - iOS Trailer");
		yield return request;

		//		request.asset as VideoClip

		//		AssetBundle bundleX = DownloadHandlerAssetBundle.GetContent(request);
		//
		//		clip = bundleX.LoadAsset<VideoClip> ("Flippy Bottle Xtreme! - iOS Trailer");
		//
		//		if (clip == null) {
		//			Debug.Log ("meh");
		//		} else {
		//			Debug.Log ("Hooray");
		//		}

		clip = request.asset as VideoClip;

		if (request == null) {
		Debug.Log ("meh");
		} else {
		Debug.Log ("Hooray");
		}

		m_vpCrossPromo.clip = clip;
		m_vpCrossPromo.Prepare ();
		WaitForSeconds waitForSeconds = new WaitForSeconds (2);

		while (!m_vpCrossPromo.isPrepared) 
		{
		yield return waitForSeconds;
		//break;
		}
		m_vpCrossPromo.Play ();	
		m_imgCrossPromo.texture = m_vpCrossPromo.texture;	
		m_crossPromoBanner.gameObject.SetActive (true);

		Debug.Log ("Time End: " + Time.time);

		//		GameObject obj = request.asset as GameObject;
		//		obj.transform.position = new Vector3(0.08f, -2.345f, 297.54f);
		//		obj.transform.Rotate(350.41f, 400f, 20f);
		//		obj.transform.localScale = new Vector3(1.0518f, 0.998f, 1.1793f);
		//
		//		Instantiate(obj);
		Debug.Log ("EndLoad");

		myLoadedAssetBundle.Unload(false);
		}


	public void EnableCrossPromoVideo(bool p_isActive)
	{
		m_crossPromoBanner.gameObject.SetActive (p_isActive);
	}



	// Services Offered
	// 0 - Get List Of All games
	// 1 - Get List of all cross promo games

	IEnumerator GetAllGames()
	{
		string _GetLink = "http://13.58.197.76/mp?s=0";

		Debug.Log ("Link: " + _GetLink);

		WWW _wwwLink = new WWW(_GetLink);

		yield return (_wwwLink);

		try
		{
			if (string.IsNullOrEmpty(_wwwLink.error))
			{
				if(m_listOfAllGames!=null)
				{
					m_listOfCrossPromoGames.Clear();
				}

				string jsonData = _wwwLink.text;
				Debug.Log("jsonresponse: " + jsonData);


				JsonData _JsonData = JsonMapper.ToObject(jsonData)["Node"];

				foreach (JsonData data in _JsonData)
				{
					AppData tempData = ExtractData(data);

//					string appName = data["app_name"].ToString();
//					string appId = data["app_id"].ToString();
//					string trailerUrl = data["trailer_dir"].ToString();
//					string androidLink = data["android_link"].ToString();
//					string iOSLink = data["iOS_link"].ToString();

					Debug.Log("tempdData: " + tempData.app_id);

					m_listOfAllGames.Add(tempData);
				}
				onRequestReceived(REQUEST_TYPE.GET_ALL_GAMES);
			}
			else
			{
				Debug.LogWarning("Failed Loading Highscores!");
			}
		}
		catch /*(System.Exception _Error)*/
		{
			Debug.LogWarning("PM Josh, an error occured!");
		}
	}

	IEnumerator GetCrossPromoGames(bool p_randFromList)
	{
		string flagNum = "1";

		#if UNITY_ANDROID
		flagNum = "1";
		#elif UNITY_IOS
		flagNum = "0";
		#endif

		string _GetLink = "http://13.58.197.76/mp?s=1&f=" + flagNum;
		WWW _wwwLink = new WWW(_GetLink);

		yield return (_wwwLink);

		try
		{
			if (string.IsNullOrEmpty(_wwwLink.error))
			{
				if(m_listOfCrossPromoGames!=null)
				{
					m_listOfCrossPromoGames.Clear();
				}

				string jsonData = _wwwLink.text;

				JsonData _JsonData = JsonMapper.ToObject(_wwwLink.text)["Node"];

				foreach (JsonData data in _JsonData)
				{
					AppData tempData = ExtractData(data);

					if(!tempData.isAppInstalled)
					{
						m_listOfCrossPromoGames.Add(tempData);
					}
					Debug.Log ("m_listOfCrossPromoGames Video1: " + tempData.app_name);


				}
				Debug.Log ("m_listOfCrossPromoGames Video2: " + m_listOfCrossPromoGames.Count);

				if(!p_randFromList)
				{
					if(m_listOfCrossPromoGames.Count > 0)
					{	
		Debug.Log ("FIFO: ");

						m_curData = m_listOfCrossPromoGames[0];
					}
				}
				else
				{
					if(m_listOfCrossPromoGames.Count > 0)
					{	
						Debug.Log ("Rand: ");

						int randRange = UnityEngine.Random.Range(0, m_listOfCrossPromoGames.Count);
						m_curData = m_listOfCrossPromoGames[randRange];
					}
				}
				//StartCoroutine("InstantiateObject");
		//m_curData = null;
				if(m_curData != null && m_listOfCrossPromoGames.Count > 0)
				{
					Debug.Log("Data: " + m_curData);
					StartCoroutine ("PlayVideo");
				}
			}
			else
			{
				Debug.LogWarning("Failed Loading Highscores!");
			}
		}
		catch /*(System.Exception _Error)*/
		{
			Debug.LogWarning("PM Josh, an error occured!");
		}
	}

		VideoClip clip;

		IEnumerator InstantiateObject()
		{
		Debug.Log ("Time Start: " + Time.time);
		string localUri = "file:///" + Application.dataPath + "/AssetBundles/" + "flippytrailer.ex"; 

		if (clip != null) {
			Debug.Log ("Time End: " + Time.time);
		} else {
			Debug.Log ("File Does Not Exist: Now Downloading from the server");
			localUri = m_curData.trailer_url;//"http://ec2-13-58-197-76.us-east-2.compute.amazonaws.com/videos/flippytrailer.ex";
			UnityWebRequest request = UnityWebRequest.GetAssetBundle(localUri, 0);
			yield return request.Send();

			AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
			clip = bundle.LoadAsset<VideoClip> ("Flippy Bottle Xtreme! - iOS Trailer");


		}

			Debug.Log ("Time End: " + Time.time);

			m_vpCrossPromo.clip = clip;
			m_vpCrossPromo.Prepare ();
			WaitForSeconds waitForSeconds = new WaitForSeconds (1);

			while (!m_vpCrossPromo.isPrepared) 
			{
			yield return waitForSeconds;
			//break;
			}
			m_vpCrossPromo.Play ();	
			m_imgCrossPromo.texture = m_vpCrossPromo.texture;	

			m_crossPromoBanner.gameObject.SetActive (true);

			Debug.Log ("PlayVideo2: " + m_vpCrossPromo.isPrepared);
			Debug.Log ("Time End: " + Time.time);


		}

	private AppData ExtractData(JsonData p_data)
	{
		string appName = p_data["app_name"].ToString();
		string appId = p_data["app_id"].ToString();
		string trailerUrl = p_data["trailer_dir"].ToString();
		string androidLink = p_data["android_link"].ToString();
		string iOSLink = p_data["iOS_link"].ToString();

		return new AppData (appId, appName, trailerUrl, androidLink, iOSLink);
	}

//		public void Update()
//		{
//		if (Input.GetKeyDown (KeyCode.A)) {
//				StartCoroutine("LoadClip");
//
//		}
//		}

	public void LaunchGameAtPlaystore()
	{
		if (m_curData == null) 
		{
			Debug.LogWarning ("There is no data available please use RequestCrossPromoVideo() first");
		}

		#if UNITY_ANDROID
		Application.OpenURL (m_curData.android_link);
		#elif UNITY_IPHONE
		int appIDIOS;

		if(int.TryParse(m_curData.iOS_link, out appIDIOS))
		{	
			_OpenAppInStore(appIDIOS);
			PlayerPrefs.SetInt(("ID_" + m_curData.app_id), 1);
		}		
		#endif
	}

//		if (string.IsNullOrEmpty(_LoadPicWWW.error))
//		{
//			Texture2D _ProfilePic = new Texture2D(50, 50, TextureFormat.DXT1, false);
//			_LoadPicWWW.LoadImageIntoTexture(_ProfilePic);
//			m_RawImageComponent.texture = _ProfilePic;
//		}

//	IEnumerator PlayMovieTexture()
//	{
//			string _GetLink = "http://13.58.197.76/videos/ShootyTrailer.mp4";
//			WWW www = new WWW(_GetLink);
//
////		if (string.IsNullOrEmpty(_LoadPicWWW.error))
////		{
//////				while(!
////			//Texture2D _ProfilePic = new Texture2D(50, 50, TextureFormat.DXT1, false);
////		}
//
//				//		MovieTexture movieTexture = _LoadPicWWW.GetMovieTexture ();
//				//		while (!movieTexture.isReadyToPlay) {
//				//				Debug.Log ("Time: " + _LoadPicWWW.progress);
//				//
//				//				yield return new WaitForSeconds (1);
//				//		}
//				//
//				//		movieTexture.Play ();
//				//		m_imgCrossPromo.texture = movieTexture;
//				//			Debug.Log ("Time: " + movieTexture.isReadyToPlay);
//
//
//			Debug.Log ("Downloading!");
//			yield return www;
//			File.WriteAllBytes(Application.persistentDataPath+"/videoFile.mp4",www.bytes);
//			Debug.Log ("File Saved!" + Application.persistentDataPath+"/videoFile.mp4");
//			//PlayVideo ("videoFile.mp4");
//		}
//
//		IEnumerator LoadClip()
//		{
//			string path = "file:" + Application.persistentDataPath + "/videoFile.mp4";
//
//			WWW www = new WWW(path);
//			yield return www;
//			MovieTexture clip = www.GetMovieTexture ();//(false, false, AudioType.WAV);
//			m_imgCrossPromo.texture = clip;
//			clip.Play ();
//			//m_vpCrossPromo.Play ();
//			//auSource.clip = clip;
//			//auSource.Play();
//		}

	IEnumerator PlayVideo()
	{
		//Debug.Log ("PlayVideo: " + Time.time + " _ " + m_curData.trailer_url);

		if (m_curData == null) {
		yield return null;
		}

		m_vpCrossPromo.source = VideoSource.Url;
		m_vpCrossPromo.url = m_curData.trailer_url;
		m_vpCrossPromo.Prepare ();
		WaitForSeconds waitForSeconds = new WaitForSeconds (2);
		while (!m_vpCrossPromo.isPrepared) 
		{
			yield return waitForSeconds;
			//break;
		}
		m_vpCrossPromo.Play ();
		m_imgCrossPromo.texture = m_vpCrossPromo.texture;	
		m_crossPromoBanner.gameObject.SetActive (true);
		//Debug.Log ("PlayVideo2: " + Time.time);

	}
}
