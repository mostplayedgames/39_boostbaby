﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OBJ_TYPE
{
    CAR_MOVING,
    CAR_PURPLE,
    CAR_GREEN,
    COIN,
    BOOST_GEM,
    EXPLOSION,
    SCORE_EFFECT,
    FINISH_LINE,
    COIN_HIT_EFFECT,
}

public class SpawnerManager : ZSingleton<SpawnerManager> {

    public GameObject m_prefabMovingCar;
    public GameObject m_prefabPurpleCar;
    public GameObject m_prefabGreenCar;
    public GameObject m_prefabGem;
    public GameObject m_prefabCoin;
    public GameObject m_prefabExplosion;
    public GameObject m_prefabScoreEffect;
    public GameObject m_prefabCoinHitEffect;
    public GameObject m_prefabFinishLine;

    public GameObject m_groupSpawnedObjects;
    public GameObject m_groupSpawnedUIObjects;


    public void InitializeManager()
    {
        ZObjectMgr.Instance.AddNewObject(m_prefabMovingCar.name, 50, m_groupSpawnedObjects);
        ZObjectMgr.Instance.AddNewObject(m_prefabPurpleCar.name, 50, m_groupSpawnedObjects);
        ZObjectMgr.Instance.AddNewObject(m_prefabGreenCar.name, 50, m_groupSpawnedObjects);

        ZObjectMgr.Instance.AddNewObject(m_prefabGem.name, 50, m_groupSpawnedObjects);
        ZObjectMgr.Instance.AddNewObject(m_prefabCoin.name, 50, m_groupSpawnedObjects);
        ZObjectMgr.Instance.AddNewObject(m_prefabExplosion.name, 10, m_groupSpawnedObjects);
        ZObjectMgr.Instance.AddNewObject(m_prefabCoinHitEffect.name, 10, m_groupSpawnedObjects);


        ZObjectMgr.Instance.AddNewObject(m_prefabScoreEffect.name, 10, m_groupSpawnedUIObjects);
        ZObjectMgr.Instance.AddNewObject(m_prefabFinishLine.name, 10, m_groupSpawnedObjects);

    }

    public GameObject Spawn(OBJ_TYPE p_objType, Vector3 p_pos)
    {
        GameObject retVal = null;
        switch(p_objType)
        {
            case OBJ_TYPE.CAR_GREEN:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabGreenCar.name, p_pos);
                    retVal.GetComponent<Rigidbody>().isKinematic = true;
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);

                }
                break;
            case OBJ_TYPE.CAR_PURPLE:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabPurpleCar.name, p_pos);
                    retVal.GetComponent<Rigidbody>().isKinematic = true;
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);

                }
                break;
            case OBJ_TYPE.CAR_MOVING:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabMovingCar.name, p_pos);
                    retVal.GetComponent<Rigidbody>().isKinematic = true;
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);

                }
                break;
            case OBJ_TYPE.COIN_HIT_EFFECT:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabCoinHitEffect.name, p_pos + new Vector3(-30,10,0));
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);
                    break;
                }
            case OBJ_TYPE.COIN:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabCoin.name, p_pos);
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);

                }
                break;
            case OBJ_TYPE.EXPLOSION:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabExplosion.name, p_pos);
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);

                }
                break;
            case OBJ_TYPE.BOOST_GEM:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabGem.name, p_pos);
                    retVal.transform.localEulerAngles = new Vector3(0, 0f, 0);

                }
                break;
            case OBJ_TYPE.FINISH_LINE:
                {
                    retVal = ZObjectMgr.Instance.Spawn3D(m_prefabFinishLine.name, p_pos);
                }
                break;
        }
        return retVal;
    }

    public void SpawnCoinParticle(Vector3 p_position)
    {
        ScoreTextEffect scoreTextEffect = ZObjectMgr.Instance.Spawn(m_prefabScoreEffect.name).GetComponent<ScoreTextEffect>();
        scoreTextEffect.Initialize(p_position);

        //Spawn(OBJ_TYPE.COIN_HIT_EFFECT, p_position);
    }

    public void EnableHighlight()
    {
        m_prefabGreenCar.GetComponent<CarHiglight>().Highlight();
        m_prefabPurpleCar.GetComponent<CarHiglight>().Highlight();
        m_prefabMovingCar.GetComponent<CarHiglight>().Highlight();

    }

    public void DisableHighlight()
    {
        m_prefabGreenCar.GetComponent<CarHiglight>().Reset();
        m_prefabPurpleCar.GetComponent<CarHiglight>().Reset();
        m_prefabMovingCar.GetComponent<CarHiglight>().Reset();
    }
}
