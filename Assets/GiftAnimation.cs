﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftAnimation : MonoBehaviour {

    public void Awake()
    {
        //Shake();
    }

    public void Idle()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.rotateX(this.gameObject, 0, 0.3f).setEase(LeanTweenType.easeInOutExpo); 
        LeanTween.scale(this.gameObject, this.transform.localScale, 0.5f).setOnComplete(Shake);
    }

    public void Shake()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.rotateZ(this.gameObject, 15.0f, 0.3f).setLoopPingPong().setLoopCount(2).setEase(LeanTweenType.easeShake).setOnComplete(Idle); 
    }
}
