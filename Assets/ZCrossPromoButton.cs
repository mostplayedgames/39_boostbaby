﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZCrossPromoButton : MonoBehaviour
{
    public REWARD_TYPES m_crossPromoType;

    public void BtnCall_UseBird(int p_count)
    {
        if(PlayerPrefs.GetInt("birdbought" + p_count) > 0)
        {
            GameScene.instance.UseBird(p_count);
        }
        else
        {
            ZRewardsMgr.instance.RedirectCrossPromo(m_crossPromoType);
            //RedirectLink();   
            //PlayerPrefs.SetInt("birdbought" + p_count, 1);
            ShopScene.instance.SetupScene(true);
        }
    }

    private void RedirectLink()
    {

    }
}
