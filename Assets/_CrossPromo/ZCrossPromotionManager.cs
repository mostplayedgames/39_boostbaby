﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tapdaq;

[RequireComponent(typeof(Canvas), typeof(GraphicRaycaster))]
public class ZCrossPromotionManager : ZSingleton<ZCrossPromotionManager> 
{
    [Space]
    [Header("UI Components")]
    [SerializeField]
    CanvasGroup m_CanvasGroup;

    [SerializeField]
    Canvas m_CanvasComponent;

    [SerializeField]
    Image m_ImageComponent;

    [SerializeField]
    GraphicRaycaster m_GraphicRaycaster;

    [Header("Game Objects")]
    [SerializeField]
    GameObject m_CrossPromoPanel;

    [Header("Various")]
    [SerializeField]
    bool m_isUnscaledDeltatime;

    [SerializeField]
    bool m_ZeroTimeScaleIfShow;

    bool m_canInteract;
    string m_CrossPromoTag = "crosspromo";
    TDNativeAd m_TDNativeAd;

    #region Properties
    public bool isCrossPromoVisible
    {
        get { return m_CrossPromoPanel.activeInHierarchy; }
    }
    #endregion

    void Reset()
    {
        m_GraphicRaycaster = GetComponent<GraphicRaycaster>();
        m_CrossPromoPanel = transform.GetChild(0).gameObject;

        m_ImageComponent = m_CrossPromoPanel.transform.GetChild(0).GetComponent<Image>();
        m_CanvasGroup = m_CrossPromoPanel.GetComponent<CanvasGroup>();

        m_CanvasComponent = gameObject.GetComponent<Canvas>();
        m_CanvasComponent.worldCamera = Camera.main;
    }

    void Start()
    {
        m_GraphicRaycaster.enabled = false;
        AdManager.Init();
    }

    #region For Tapdaq Delegates
    private void OnEnable()
    {
        TDCallbacks.TapdaqConfigLoaded += OnTapdaqConfigLoaded;
        TDCallbacks.AdAvailable += OnAdAvailable;
    }

    private void OnDisable()
    {
        TDCallbacks.TapdaqConfigLoaded -= OnTapdaqConfigLoaded;
        TDCallbacks.AdAvailable -= OnAdAvailable;
    }

    private void OnTapdaqConfigLoaded()
    {
        AdManager.LoadNativeAdvertForTag(m_CrossPromoTag, TDNativeAdType.TDNativeAdType2x3Large);
    }

    private void OnAdAvailable(TDAdEvent e)
    {
        if (e.adType == "NATIVE_AD" && e.tag == m_CrossPromoTag)
        {
            m_TDNativeAd = AdManager.GetNativeAd(TDNativeAdType.TDNativeAdType2x3Large, m_CrossPromoTag);
            forceShowCrossPromo();
        }
    }
    #endregion

    void Update()
    {
        #if UNITY_EDITOR
        //For editor purposes only
        if (Input.GetKeyDown(KeyCode.Space) && !m_CrossPromoPanel.activeInHierarchy)
            forceShowCrossPromo();
        #endif

        #if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape) && m_CrossPromoPanel.activeInHierarchy)
            CrossPromoButtonCallback(false);
        #endif
    }

    public void forceShowCrossPromo()
	{
        #if UNITY_EDITOR
        StartCoroutine(CrossPromotionSequence(true));
        return;
        #endif

        if (m_TDNativeAd == null)
            return;

        m_TDNativeAd.LoadTexture((TDNativeAd obj) =>
        {
            Texture2D _NewTexture = obj.texture;

            if (_NewTexture == null)
                return;

            Sprite _GetSprite = Sprite.Create(_NewTexture, new Rect(0, 0, _NewTexture.width, _NewTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
            m_ImageComponent.sprite = _GetSprite;
            AdManager.SendNativeImpression(m_TDNativeAd);
            StartCoroutine(CrossPromotionSequence(true));
        });
	}

    public void CrossPromoButtonCallback(bool _ToOpen)
    {
        if (!m_canInteract)
            return;

        if (_ToOpen)
        {
            AdManager.SendNativeClick(m_TDNativeAd);
            Unpause();
        }
        else
        {
            StartCoroutine(CrossPromotionSequence(false));
        }
    }

    IEnumerator CrossPromotionSequence(bool _ToShow)
    {
        #region Stays here if it's not in the menu
        if (_ToShow)
        {
            m_CrossPromoPanel.SetActive(false);
			yield return new WaitUntil(() => GameScene.instance.m_eState == GAME_STATE.IDLE);
        }
        #endregion

        if (m_ZeroTimeScaleIfShow)
            Time.timeScale = 0;

        m_GraphicRaycaster.enabled = true;
        m_canInteract = false;

        if (_ToShow)
        {
            m_CrossPromoPanel.SetActive(true);
            m_CanvasGroup.alpha = 0;
        }

        float _ToAlpha = _ToShow ? 1 : 0;

        while (m_CanvasGroup.alpha != _ToAlpha)
        {
            yield return new WaitForEndOfFrame();
            m_CanvasGroup.alpha = Mathf.MoveTowards(m_CanvasGroup.alpha, _ToAlpha, (m_isUnscaledDeltatime ? Time.unscaledDeltaTime : Time.deltaTime) * 2);
        }

        if (!_ToShow)
            Unpause();

        m_canInteract = true;
        yield return null;
    }

    void Unpause()
    {
        m_CrossPromoPanel.SetActive(false);
        m_GraphicRaycaster.enabled = false;

        if (m_ZeroTimeScaleIfShow)
            Time.timeScale = 1;
    }
}
