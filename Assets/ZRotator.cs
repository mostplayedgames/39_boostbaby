﻿using UnityEngine;
using System.Collections;

public class ZRotator : MonoBehaviour {

	public bool isWorld = false;
	public Vector3 speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isWorld) {
			this.transform.eulerAngles += speed * Time.deltaTime;
		} else {
			this.transform.localEulerAngles += speed * Time.deltaTime;
		}
	}
}
