﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CHAR_STATE
{
	IDLE,
	CHARGE,
	JUMP
}

public class FDCharacter : MonoBehaviour {

	public GameObject m_objectCharacter;
	public GameObject m_objectFoot;
	public GameObject m_objectTrail;

	public AudioClip m_audioFlap;

	public ParticleSystem m_particleSystem;

	float m_currentChargeTime;

	CHAR_STATE m_eState;

	// Use this for initialization
	void Start () {
		LeanTween.scale (this.gameObject, new Vector3 (1f, 0.9f, 1f), 0.5f).setLoopPingPong ();
		Reset ();
	}
	
	// Update is called once per frame
	void Update () {
		if ( isWarpTime ){
			if (this.GetComponent<Rigidbody2D> ().drag >= 0) {
				this.GetComponent<Rigidbody2D> ().drag -= 50 * Time.deltaTime;
			} else {
				this.GetComponent<Rigidbody2D> ().drag = 0;
				isWarpTime = false;
			}
			//if (Time.timeScale <= 1) {
				//Time.timeScale += Time.deltaTime;
			//} else {
			//	Time.timeScale = 1;
			//	isWarpTime = false;
			//}
		}
	}

	void FixedUpdate()
	{
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.y < 0) {
			//this.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, -400f));
			//this.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, -200f));
			this.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, -300f));
			//Debug.Log ("Add Force");
		}else{
			//this.gameObject.GetComponent<Rigidbody2D> ().drag = 5;
		}
	}

	public void Reset()
	{
		this.transform.localEulerAngles = Vector3.zero;
		this.transform.localScale = new Vector3 (1, 1, 1);
		m_objectTrail.SetActive (true);
		//m_eState = CHAR_STATE.IDLE;
	}

	public void Flap()
	{
		//if (m_eState == CHAR_STATE.JUMP)
		//	return;
		
		/*Debug.Log ("Time : " + (Time.time - m_currentChargeTime));
		LeanTween.cancel (m_objectCharacter.gameObject);
		float jumpCoefficient = 0.2f + Mathf.Abs((Time.time - m_currentChargeTime) * 1.0f / 1.5f);
		Debug.Log ("Jump Coefficient : " + jumpCoefficient);
		LeanTween.moveLocalY (m_objectCharacter.gameObject, 13.8f, 0.02f);
		this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (300f + (700f * jumpCoefficient), 1500f + (4500f * jumpCoefficient)));
		this.GetComponent<Rigidbody2D> ().AddTorque (-2050f);*/


		//m_particleSystem.GetComponent<ParticleSystem> ().Clear ();
		//m_particleSystem.transform.position = m_rootParticle.transform.position;
		m_particleSystem.gameObject.SetActive (true);
		//m_particleSystem.gameObject.GetComponent<ParticleSystem> ().spe
		m_particleSystem.gameObject.GetComponent<ParticleSystem> ().Emit(Random.Range(1,3));
		//m_particleSystem.gameObject.GetComponent<ParticleSystem> ().Emit(Random.Range(3,7));

		//Debug.Log ("Jump");
		//Normal Flappy - this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
		//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (100f, 250f);
		LeanTween.cancel (this.gameObject);
		//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
		//LeanTween.cancel(this.gameObject);
		//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
		//Normal Flappy - LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);

		switch(GameScene.instance.GetCurrentShopBird().flipAnimType){
		case ShopBirdFlapAnimType.FLIP:
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 120f, 0.3f).setEase (LeanTweenType.easeOutCubic);
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z - 150f, 0.35f).setEase (LeanTweenType.easeOutCubic);
			break;
		case ShopBirdFlapAnimType.FLAPPY:
			LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			break;
		case ShopBirdFlapAnimType.REVERSEFLIP:
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 120f, 0.3f).setEase (LeanTweenType.easeOutCubic);
			break;
		case ShopBirdFlapAnimType.FLIPPY:
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z - 170f, 0.35f).setEase (LeanTweenType.easeOutCubic).setOnComplete(DipDown);
			//LeanTween.rotateLocal (this.gameObject, this.gameObject.transform.localEulerAngles + new Vector3(70f,0,0), 0.35f);//.setEase (LeanTweenType.easeInOutCubic);//.setOnComplete (FlipDown);
			break;
		}
		//this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 4700f));

		switch (GameScene.instance.m_birdType) {
		case 0: 
			//Debug.Log ("Jump");
			this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
			LeanTween.cancel (this.gameObject);
			//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			LeanTween.cancel(this.gameObject);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			break;
		case 1 :
			//Debug.Log ("Jump");
			//Normal Flappy - this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (100f, 250f);
			LeanTween.cancel (this.gameObject);
			//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			LeanTween.cancel(this.gameObject);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//Normal Flappy - LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 120f, 0.3f).setEase (LeanTweenType.easeOutCubic);
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 4700f));
			break;
		case 2 :
			//Debug.Log ("Jump");
			//Normal Flappy - this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 170f);
			LeanTween.cancel (this.gameObject);
			//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			LeanTween.cancel(this.gameObject);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//Normal Flappy - LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 150f, 0.3f).setEase (LeanTweenType.easeOutCubic);
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 4700f));
			break;
		case 3 :
			//Debug.Log ("Jump");
			//Normal Flappy - this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 120f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 140f);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (this.GetComponent<Rigidbody2D> ().velocity.x, 200f);
			//LeanTween.cancel (this.gameObject);
			//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			//LeanTween.cancel(this.gameObject);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//Normal Flappy - LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z - 150f, 0.35f).setEase (LeanTweenType.easeOutCubic);
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 4700f));
			break;
		case 4 :
			//Debug.Log ("Jump");
			//Normal Flappy - this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 120f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 140f);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (50f, 150f);
			LeanTween.cancel (this.gameObject);
			//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			LeanTween.cancel(this.gameObject);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//Normal Flappy - LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 150f, 0.3f).setEase (LeanTweenType.easeOutCubic);
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 4700f));
			break;
		case 10:
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (0, 12000f));
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (this.GetComponent<Rigidbody2D> ().velocity.x, 225f);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (this.GetComponent<Rigidbody2D> ().velocity.x, 227f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 230f);
			//this.GetComponent<Rigidbody2D> ().

			//Debug.Log ("Jump");
			//Normal Flappy - this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 220f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 120f);
			//this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 140f);

			//this.GetComponent<Rigidbody2D> ().
			//LeanTween.cancel (this.gameObject);
			//LeanTween.rotateZ (this.gameObject, 39f, 0.15f).setEase (LeanTweenType.easeInOutCubic).setOnComplete (DipDown);
			//LeanTween.cancel(this.gameObject);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 39f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//Normal Flappy - LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 60f, 0.2f).setEase (LeanTweenType.easeOutQuad);
			//LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z - 150f, 0.35f).setEase (LeanTweenType.easeOutCubic);
			//this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 4700f));
			break;
		}

		ZAudioMgr.Instance.PlaySFX (m_audioFlap, Random.Range(0.9f, 1.1f));

		//m_eState = CHAR_STATE.JUMP;
	}

	public void Rotate()
	{
		//LeanTween.rotateX (this.gameObject, this.gameObject.transform.localEulerAngles.z + 180f, 0.1f).setEase (LeanTweenType.easeOutCubic);
		LeanTween.rotateZ (this.gameObject, this.gameObject.transform.localEulerAngles.z + 150f, 0.1f).setEase (LeanTweenType.easeOutCubic);
	}

	void DipDown(){
		LeanTween.cancel (this.gameObject);
		LeanTween.rotateZ (this.gameObject, -90f, 0.8f).setEase (LeanTweenType.easeInOutCubic);
	}

	void FlipDown(){
		LeanTween.cancel (this.gameObject);
		LeanTween.rotateLocal (this.gameObject, new Vector3(0,0,-90f), 0.8f).setEase (LeanTweenType.easeInOutCubic);
	}

	public void Land()
	{
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0f;
		this.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		GameScene.instance.Score (1);
		//LeanTween.moveY (m_objectCharacter.gameObject, m_objectCharacter.transform.position.y - 0.2f, 0.1f).setLoopPingPong ().setLoopCount (1);
	}

	bool isWarpTime = false;
	bool isFloor = false;
	public void Die()
	{
		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;
		LeanTween.cancel (this.gameObject);
		//this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Static;
		//this.GetComponent<Rigidbody2D>().isKinematic = true;
		//this.GetComponent<BoxCollider2D>().enabled = false;
		int randomDeathAnims = Random.Range(0,120);

		//this.GetComponent<Rigidbody2D> ().drag = 50;
		//LeanTween.rotateX (this.gameObject, Random.Range (-180, 180), 0.5f);
		//isWarpTime = true;
		//this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Static;
		this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-950f, 900));
		//LeanTween.delayedCall (0.5f, Move);
		if (false && !isFloor) {
			if (randomDeathAnims > 90) {
				this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-950f, 900));
				//LeanTween.rotateX (this.gameObject, Random.Range (-180, 180), 0.5f);
			} else if (randomDeathAnims > 60) {
				this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Static;
				LeanTween.delayedCall (0.5f, Move);
			} else { //if (randomDeathAnims > 30) {
				//Time.timeScale = 0.2f;
				//this.GetComponent<Rigidbody2D> ().drag = 100;
				//LeanTween.rotateX (this.gameObject, Random.Range (-180, 180), 0.5f);
				//isWarpTime = true;
				this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-150f, 100));
				this.GetComponent<Rigidbody2D> ().drag = 50;
				isWarpTime = true;
			} /*else {
				this.GetComponent<Rigidbody2D> ().drag = 100;
				isWarpTime = true;
				//this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range(-950f, 900));
				//LeanTween.rotateX (this.gameObject, Random.Range (-180, 180), 0.5f);
			}*/
		}
		//this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range(-950f, 900));
		//LeanTween.rotateX (this.gameObject, Random.Range (-180, 180), 0.5f);

		//LeanTween.rotateZ (this.gameObject, Random.Range (-180, 180), 0.5f);

		//m_objectTrail.SetActive (false);
		/*if (Random.Range (0, 100) > 50) {
		}
			//LeanTween.scaleX (this.gameObject, Random.Range(0.7f, 0.5f), 0.01f);
		else {
			LeanTween.scaleY (this.gameObject, Random.Range (0.3f, 0.2f), 0.01f);
		}*/

		//Time.timeScale = 0.2f;

		//LeanTween.delayedCall (0.2f, BackInTime);
		//isWarpTime = true;
		m_particleSystem.gameObject.GetComponent<ParticleSystem> ().Emit(Random.Range(10,30));

		GameScene.instance.Die ();
	}

	void Move()
	{
		this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
	}

	void BackInTime()
	{
		//this.GetComponent<Rigidbody2D> ().isKinematic = false;
		this.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		
		//Debug.Log ("COllide enemy 1");
		if (coll.gameObject.tag == "Enemy") {
			//Debug.Log ("COllide enemy");
			Die ();
			if (coll.gameObject.name == "piperedbottom" && GameScene.instance.m_eState == GAME_STATE.RESULTS) {
				this.transform.localScale = new Vector3 (1, Random.Range (0.3f, 0.5f), 1);
			}

			coll.gameObject.GetComponentInParent<PoleObject> ().CancelAnims ();
			//LeanTween.cancel(coll.gameObject);

			isFloor = false;
		}
		if (coll.gameObject.tag == "Floor") {
			//Debug.Log ("COllide enemy");
			//LeanTween.scaleY (this.gameObject, Random.Range (0.3f, 0.2f), 0.01f);
			if( GameScene.instance.GetCurrentShopBird().willFlat)
				this.transform.localScale = new Vector3 (1, Random.Range (0.3f, 0.5f), 1);

			//this.gameObject.GetComponent<Rigidbody2D> ().gravityScale = 400;
			
			LeanTween.cancel (this.gameObject);
			//this.GetComponent<Rigidbody2D> ().gravityScale = 2000;
			Die();

			isFloor = true;
		}
	}

	public GameObject m_objectCoin;

	void Coin_Fadeout()
	{
		LeanTween.alpha (m_objectCoin.gameObject, 0, 0.5f);
	}

	void Coin_AddCoinRoutine()
	{
		GameScene.instance.AddCoins (1);
		currentCoinObject.SetActive (false);
		currentCoinObject.gameObject.GetComponent<BoxCollider2D>().enabled = true;
	}

	GameObject currentCoinObject;

	public AudioClip m_audioBumpCoin;

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Coin" && !coll.gameObject.GetComponent<PoleObject> ().isHit) {
			GameScene.instance.Score (1);
			coll.gameObject.GetComponent<PoleObject> ().Score ();
		}
		else if (coll.gameObject.tag == "Cat") {
			
			//coll.gameObject.SetActive (false);
			Vector3 targetPosition = ZCameraMgr.instance.gameObject.transform.position;
			Vector3 newPosition = new Vector3 (targetPosition.x + 120, targetPosition.y + 100, coll.gameObject.transform.position.z);
			LeanTween.move (coll.gameObject, newPosition, 0.4f).setOnComplete (Coin_AddCoinRoutine).setEase(LeanTweenType.easeInQuad);
			//LeanTween.scale (coll.gameObject, new Vector3(0.2f,0.2f,0.2f), 0.6f);
			currentCoinObject = coll.gameObject;
			coll.gameObject.GetComponent<BoxCollider2D>().enabled = false;

			ZAudioMgr.Instance.PlaySFX (m_audioBumpCoin);

			//m_objectCoin.transform.position = this.gameObject.transform.position + new Vector3(25f, 0f, 0);
			//LeanTween.cancel (m_objectCoin);
			//Color textColor = m_objectCoin.GetComponent<Renderer> ().material.color;
			//m_objectCoin.GetComponent<Renderer> ().material.color = new Color(textColor.r, textColor.g, textColor.b, 1);
			//LeanTween.move (m_objectCoin.gameObject, m_objectCoin.transform.position + new Vector3 (0, 7f, 0), 0.5f).setEase (LeanTweenType.easeOutQuad).setOnComplete(Coin_Fadeout);
			//m_objectCoin.GetComponent<TextMesh> ().text = "+1";
		}
	}
}
