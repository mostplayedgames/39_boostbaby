﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public delegate void OnContinueGamePressed(); 
public delegate void OnNoThanksPressed();

public class ContinueManager : ZSingleton<ContinueManager> {

	public event OnNoThanksPressed continueGamePressed;
	public event OnNoThanksPressed noThanksPressed;

	public Button m_btnContinueGame;
	public Button m_btnNoThanks;

	private bool m_canContinueGame = true;

	public void Reset()
	{
		m_canContinueGame = true;
		ShowButtons (false);
	}

	public void ShowButtons(bool p_willShowBtns)
	{
		if (m_canContinueGame && p_willShowBtns) 
		{
			m_canContinueGame = false;
		}
			
		m_btnContinueGame.gameObject.SetActive (p_willShowBtns);
		m_btnNoThanks.gameObject.SetActive (p_willShowBtns);
	}

	public void CallBack_ContinueGame()
	{
		Debug.Log ("CallBack ConitnueGame");
		continueGamePressed();
	}

	public void BtnCall_ContinueGame()
	{
		ZRewardsMgr.instance.ShowContinueVideo ();
		ShowButtons (false);
    }

	public void BtnCall_NoThanks()
	{
		noThanksPressed();
		//GameScene.instance.Die ();
	}

	public bool IsInternetAvailable()
	{
		switch (Application.internetReachability) {
		case NetworkReachability.NotReachable:
			return false;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			return true;
			break;
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			return true;
			break;
		default :
			return false;
			break;
		}
	}
		
	public bool CanContinueGame()
	{
        return m_canContinueGame && IsInternetAvailable() && 
                ZRewardsMgr.instance.IsVideoReady() && 
               (GameScene.instance.GetScore() > 50 && 
                GameScene.instance.GetCurrentMode() != 1);
	}
}
