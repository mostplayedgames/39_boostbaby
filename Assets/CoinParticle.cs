﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinParticle : MonoBehaviour {

	public ParticleSystem m_particle;
	public Text m_textScore;

	public void Play()
	{
		//m_textScore.transform.localScale = Vector3.zero;
		m_particle.Play (); 
		//LeanTween.cancel (m_textScore.gameObject);
		//LeanTween.scale (m_textScore.gameObject, new Vector3 (2.5f, 2.5f, 2.5f), 0.5f).setEase (LeanTweenType.easeOutExpo);

	}
}
