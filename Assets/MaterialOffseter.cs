﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialOffseter : MonoBehaviour {

    private Material m_curMaterial;
    private bool m_startParallax;
    private float yOffset;
    private float m_parallaxSpeed;
	// Use this for initialization
	void OnEnable () {
        m_curMaterial = this.GetComponent<Renderer>().material;

        //StartMoving(8);
	}

    public void StartMoving(float p_parallaxSpeed)
    {
        yOffset = 0;
        m_curMaterial.mainTextureOffset = new Vector2(0, yOffset);

        m_startParallax = true;
        m_parallaxSpeed = p_parallaxSpeed;
    }

    public void Reset()
    {
        m_startParallax = false;
    }

    // Update is called once per frame
    void Update () 
    {
        if (!m_startParallax)
            return;
            
        yOffset += m_parallaxSpeed * Time.deltaTime;
        m_curMaterial.mainTextureOffset = new Vector2(0, yOffset);
	}
}
