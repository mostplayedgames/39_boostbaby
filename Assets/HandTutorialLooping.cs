﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandTutorialLooping : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		LeanTween.cancel (this.gameObject);
		LeanTween.moveLocalX (this.gameObject, -60f, 1f).setLoopPingPong ().setEase (LeanTweenType.easeInOutSine);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
