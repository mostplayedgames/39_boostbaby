﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZAudioMgr : ZSingleton<ZAudioMgr> {

	List<AudioSource> m_listAudioSource;

	void Awake()
	{
		InstantiateAudioSources ();
	}

	void InstantiateAudioSources()
	{
		m_listAudioSource = new List<AudioSource> ();

		for(int x = 0; x < 50; x++) {
			m_listAudioSource.Add(this.gameObject.AddComponent<AudioSource>());
		}
	}

	AudioSource GetAvailableAudioSource()
	{
		foreach (AudioSource source in m_listAudioSource) {
			if (!source.isPlaying) {
				return source;
			}
		}
		return null;
	}

	public void StopSFX(AudioClip clip)
	{
		for(int x = 0; x < 16; x++) {
			if( m_listAudioSource[x].clip == clip )
			{	m_listAudioSource[x].Stop();
			}
		}
	}

    public void SetVolume(int p_volume)
    {
        AudioSource[] audioList = this.GetComponents<AudioSource>();
        foreach (AudioSource audioS in audioList)
        {
            audioS.volume = p_volume;
        }
    }

	public void PlaySFX(AudioClip clip, bool looping, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = looping;
		source.clip = clip;
        source.volume = PlayerPrefs.GetInt("isSfxEnabled");
        //if(PlayerPrefs.GetInt("isSfxEnabled") == 0)
        //{
        //    source.volume = 0;
        //}
        //else
        //{
        //    source.volume = 1;
        //}
		source.Play ();
	}

	public void PlaySFX(AudioClip clip, bool looping)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = 1;
		source.loop = looping;
		source.clip = clip;
		source.Play ();
	}

	public void PlaySFX(AudioClip clip)
	{
		AudioSource source = GetAvailableAudioSource ();

		if (source) {
			source.pitch = 1;
			source.loop = false;
			source.clip = clip;
            source.volume = PlayerPrefs.GetInt("isSfxEnabled");
			source.Play ();
		}
	}

	public void PlaySFX(AudioClip clip, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = false;
		source.clip = clip;
		source.Play ();
	}

	public void PlaySFXVolume(AudioClip clip, float volume)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = 1;
		source.loop = false;

        if(PlayerPrefs.GetInt("isSfxEnabled") == 0)
        {
            source.volume = 0;
        }
        else
        {
            source.volume = volume;
        }

		source.clip = clip;
		source.Play ();
	}

	public void PlaySFXVolume(AudioClip clip, float volume, bool looping)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = 1;
		source.loop = looping;
        if (PlayerPrefs.GetInt("isSfxEnabled") == 0)
        {
            source.volume = 0;
        }
        else
        {
            source.volume = volume;
        }		
        source.clip = clip;
		source.Play ();
	}
}
