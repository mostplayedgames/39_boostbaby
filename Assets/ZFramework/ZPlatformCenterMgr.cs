﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

using GoogleMobileAds.Api;

public class ZPlatformCenterMgr : ZSingleton<ZPlatformCenterMgr> {
	public void Login () {
		//return;

		#if UNITY_ANDROID
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
			// enables saving game progress.
			//.EnableSavedGames()
				// requests a server auth code be generated so it can be passed to an
			//  associated back end server application and exchanged for an OAuth token.
			.RequestServerAuthCode(false)
			// requests an ID token be generated.  This OAuth token can be used to
			//  identify the player to other services such as Firebase.
			.RequestIdToken()
			.Build();

		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		//PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();

		//Social.localUser.Authenticate((bool success) => {
			// handle success or failure
			//Social.LoadAchievements (ProcessLoadedAchievements);
		//	ProcessAuthentication(success);
		//});
		#endif // UNITY_ANDROID

		// Authenticate and register a ProcessAuthentication callback
		// This call needs to be made before we can proceed to other calls in the Social API s
		Social.localUser.Authenticate (ProcessAuthentication);
	}

	// This function gets called when Authenticate completes
	// Note that if the operation is successful, Social.localUser will contain data from the server.
	void ProcessAuthentication (bool success) {
		if (success) {
			Debug.Log ("Authenticated, checking achievements");

			// Request loaded achievements, and register a callback for processing them
			Social.LoadAchievements (ProcessLoadedAchievements);
		}
		else
			Debug.Log ("Failed to authenticate");
	}

	// This function gets called when the LoadAchievement call completes
	void ProcessLoadedAchievements (IAchievement[] achievements) {
		/*if (achievements.Length == 0)
			Debug.Log ("Error: no achievements found");
		else
			Debug.Log ("Got " + achievements.Length + " achievements");

		// You can also call into the functions like this
		/*Social.ReportProgress ("Achievement01", 100.0, function(result) {
			if (result)
				Debug.Log ("Successfully reported achievement progress");
			else
				Debug.Log ("Failed to report achievement");
		});*/
	}

	public void ShowLeaderboardUI(string id = "")
	{
		return;

		#if UNITY_ANDROID
		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(id);
		#elif UNITY_IOS
		Social.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	}

	public void PostScore(int score, string leaderboardName)
	{
		return;

		if (Application.isEditor)
			return;
		
		long scoreLong = score; // Gamecenter requires a long variable
		Social.ReportScore(scoreLong,leaderboardName,HighScoreCheck);

		Debug.Log ("Score : " + score + ", Name : " + leaderboardName);
	}

	public void OpenAchievements()
	{
		return;

		Social.ShowAchievementsUI();
	}

	public void PostUnlockAchievement(string id = "")
	{
		return;

		#if UNITY_ANDROID
		Social.ReportProgress(id, 100.0f, (bool success) => {
			//	// handle success or failure
		} );


		#endif // UNITY_ANDROID
	}

	public void PostIncrementAchievement(string id, int value)
	{
		return;

		#if UNITY_ANDROID
		//PlayGamesPlatform.Instance.IncrementAchievement(
		//	id, value, (bool success) => {
		// handle success or failure
		//	} );
		#endif // UNITY_ANDROID
	}

	static void HighScoreCheck(bool result) {
		if(result)
			Debug.Log("score submission successful");
		else
			Debug.Log("score submission failed");
	}
}
