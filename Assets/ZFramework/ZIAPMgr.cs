﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

using UnityEngine.Purchasing;

public class ZIAPMgr : ZSingleton<ZIAPMgr>, IStoreListener {
	IStoreController m_StoreController;
	IExtensionProvider m_StoreExtensionProvider;

	void Start() {
		var module = StandardPurchasingModule.Instance();
		ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);
		builder.AddProduct(ZGameMgr.instance.IAP_ID_REMOVEADS, ProductType.NonConsumable);
		builder.AddProduct(ZGameMgr.instance.IAP_ID_COIN1, ProductType.Consumable);
		UnityPurchasing.Initialize(this, builder);
	}

	public void PurchaseRemoveAds() {
		if (m_StoreController != null) {
			// Fetch the currency Product reference from Unity Purchasing
			Product product = m_StoreController.products.WithID(ZGameMgr.instance.IAP_ID_REMOVEADS);
			if (product != null && product.availableToPurchase) {
				m_StoreController.InitiatePurchase(product);
			}
		}
	}

	public void PurchaseCoins() {
		if (m_StoreController != null) {
			// Fetch the currency Product reference from Unity Purchasing
			Product product = m_StoreController.products.WithID(ZGameMgr.instance.IAP_ID_COIN1);
			if (product != null && product.availableToPurchase) {
				m_StoreController.InitiatePurchase(product);
			}
		}
	}

	public void RestorePurchases()
	{
		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions) { 
		m_StoreController = controller; 
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error) {}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) { 
		if (String.Equals(e.purchasedProduct.definition.id, ZGameMgr.instance.IAP_ID_REMOVEADS, StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Removeads", 1);
			ZAdsMgr.Instance.removeAds = 1;
			ZAdsMgr.Instance.HideBannerAd ();
			ZAnalytics.Instance.SendBusinessEvent ("archer", 1, "ads", ZGameMgr.instance.IAP_ID_REMOVEADS, "shop");
			PlayerPrefs.Save ();
		}
		if (String.Equals(e.purchasedProduct.definition.id, ZGameMgr.instance.IAP_ID_COIN1, StringComparison.Ordinal)) {
			GameScene.instance.AddCoins (1000);
			ZAnalytics.Instance.SendBusinessEvent ("archer", 1, "coin", ZGameMgr.instance.IAP_ID_COIN1, "shop");
			PlayerPrefs.Save ();
		}
		return PurchaseProcessingResult.Complete; 
	}

	public void OnPurchaseFailed(Product item, PurchaseFailureReason r) {}
}
