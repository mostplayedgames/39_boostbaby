﻿using UnityEngine;
using System.Collections;

public abstract class ZGameScene : MonoBehaviour {

	public abstract void OnButtonUp(ZButton button);

	public abstract void OnButtonDown(ZButton button);

}
