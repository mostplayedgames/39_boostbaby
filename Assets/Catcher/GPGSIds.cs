// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///

namespace Catcher
{
public static class GPGSIds
{
        public const string leaderboard_half_court_leaderboard = "CggIy6nL60EQAhAC"; // <GPGSID>
        public const string leaderboard_level_leaderboard = "CggIy6nL60EQAhAA"; // <GPGSID>
        public const string leaderboard_shootout_leaderboard = "CggIy6nL60EQAhAB"; // <GPGSID>
        public const string leaderboard_full_court_leaderboard = "CggIy6nL60EQAhAF"; // <GPGSID>
        public const string leaderboard_3pt_shootout_leaderboard = "CggIy6nL60EQAhAE"; // <GPGSID>
        public const string leaderboard_shooter_leaderboard = "CggIy6nL60EQAhAG"; // <GPGSID>

}
}
