﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsBtn : MonoBehaviour {

	[SerializeField]
	private Image m_settingsBtn;

	[SerializeField]
	private Image m_settingsChildBn;

	[SerializeField]
	private Sprite m_sprEnabledBtn;
	[SerializeField]
	private Sprite m_sprDisabledBtn;
	[SerializeField]
	private Sprite m_sprMuteIcon;
	[SerializeField]
	private Sprite m_sprUnmuteIcon;

	[SerializeField]
	private AudioClip m_audioPlayBtn;

	private bool m_isMuted;

	public void Awake()
	{
		if (!PlayerPrefs.HasKey ("isSfxEnabled")) {
			PlayerPrefs.SetInt ("isSfxEnabled", 1);
		}

		m_isMuted = (PlayerPrefs.GetInt ("isSfxEnabled") == 0) ? false : true;
		BtnCall_MuteButton ();
	}

	public void BtnCall_MuteButton()
	{
		m_isMuted = (m_isMuted) ? false : true;

		Debug.Log ("Muted: " + m_isMuted);

		int saveFile = 0;//(m_isMuted) ? 1 : 0;
		Sprite tempSprite = m_sprDisabledBtn;//(m_isMuted) ? m_sprDisabledBtn : m_sprEnabledBtn; 
		Sprite tempChildSprite = m_sprMuteIcon;//(m_isMuted) ? m_sprMuteIcon : m_sprUnmuteIcon;

		if (m_isMuted) {
            saveFile = 0;
			tempSprite = m_sprDisabledBtn;
			tempChildSprite = m_sprMuteIcon;
            ZAudioMgr.Instance.SetVolume (0);

		} else {
            saveFile = 1;
			tempSprite = m_sprEnabledBtn;
			tempChildSprite = m_sprUnmuteIcon;
            ZAudioMgr.Instance.SetVolume (1);
		}

		m_settingsBtn.sprite = tempSprite;
		m_settingsChildBn.sprite = tempChildSprite;

		ZAudioMgr.Instance.PlaySFX (m_audioPlayBtn);
		PlayerPrefs.SetInt ("isSfxEnabled", saveFile);
	}

    //private void SetVolume(int p_volume)
    //{
    //    AudioSource[] audioList = ZAudioMgr.Instance.GetComponents<AudioSource>();
    //    foreach (AudioSource audioS in audioList)
    //    {
    //        audioS.volume = p_volume;
    //    }
    //}
}
