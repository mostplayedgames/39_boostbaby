﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBCar : ZSingleton<BBCar> {

	public GameObject m_boostEffect;
	public GameObject m_prefabScore;
	public GameObject m_objectBoost;

    public GameObject m_objCelebrateParticle;

    public GameObject m_objFakeTrailLeft;
    public GameObject m_objFakeTrailRight;
    public GameObject m_objFakeBoost;

    public GameObject m_objInGameTrailLeft;
    public GameObject m_objInGameTrailRight;
    public GameObject m_objInGameBoost;

    public GameObject m_objBoostModeBoost;

    public GameObject m_objPhantomCar;

    public CarShakeEffect m_carHolder;

    public AudioClip m_audioBoostSfx;
    public AudioClip m_audioNormalEngine;
    public AudioClip m_audioBoostEngine;
    public AudioClip m_audioExplosion;
    public AudioClip m_audioCelebrate;

    public AudioClip m_audioCarHit;

    public List<GameObject> m_listOfCars;

    private GameObject m_currentCar;
    public GameObject m_objLastCarHit;

   //public bool isAlive;

	// Use this for initialization
	void Start () 
    {
		m_boostEffect.SetActive (false);
		//PlayerPrefs.DeleteAll ();
	}

	public bool m_isBoost = false;
	float m_boostTime = 0;
	float m_boostCamera = 0;
	float m_boostInvincibleTime = 0;

   public bool m_reachedFinishLine;

	// Update is called once per frame
	void Update () {

        if(GameScene.instance.m_eState != GAME_STATE.IN_GAME)
        {
            return;
        }



        if (m_reachedFinishLine)
        {
            return;
        }

        Vector3 camPosition = ZCameraMgr.instance.transform.localPosition;
        ZCameraMgr.instance.transform.localPosition = new Vector3(camPosition.x, camPosition.y, this.transform.position.z - 90f);

        //if (GameScene.instance.GetCurrentMode() == 2)
        //{
        //    ZCameraMgr.instance.GetComponent<Camera>().fieldOfView = 57.1f;
        //}
        //else
        {
            ZCameraMgr.instance.GetComponent<Camera>().fieldOfView = 37.1f + m_boostCamera;
        }

		if (m_isBoost) {

			m_boostCamera += Time.deltaTime * 50;
			if (m_boostCamera >= 20)
				m_boostCamera = 20;
			this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,800f);
			m_boostTime -= Time.deltaTime;
	

			if (m_boostTime <= 0) 
            {
                DeactivateBoost();
			}
		} 
        else 
        {
			if (GameScene.instance.GetScore () > 200) 
            {
				this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 700f);
			}
            else if (GameScene.instance.GetScore () > 100) 
            {
				this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 600f);
			} 
            else 
            {
				this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 500f);
			}

			m_boostCamera -= Time.deltaTime * 50;
			if (m_boostCamera <= 0)
				m_boostCamera = 0;

			m_boostInvincibleTime -= Time.deltaTime;
		}


	}

    public void ActivateBoost()
    {
        m_isBoost = true;
        m_boostTime = 6;
        m_boostEffect.SetActive(true);
        m_boostCamera = 0;
        m_boostInvincibleTime = 1;

        ZAudioMgr.Instance.PlaySFX(m_audioBoostSfx);
        ZAudioMgr.Instance.StopSFX(m_audioNormalEngine);
        ZAudioMgr.Instance.PlaySFXVolume(m_audioNormalEngine, 0.5f, true);

        SpawnerManager.Instance.EnableHighlight();

        m_objInGameBoost.gameObject.SetActive(false);
        m_objBoostModeBoost.gameObject.SetActive(true);

        m_objectBoost.SetActive(true);
    }

    public void DeactivateBoost()
    {
        m_isBoost = false;
        m_boostEffect.SetActive(false);
        ZAudioMgr.Instance.StopSFX(m_audioNormalEngine);
        ZAudioMgr.Instance.PlaySFXVolume(m_audioNormalEngine, 0.5f, true);
        //ZAudioMgr.Instance.StopSFX(m_audioBoostEngine);
        SpawnerManager.Instance.DisableHighlight();

        m_objInGameBoost.gameObject.SetActive(true);
        m_objBoostModeBoost.gameObject.SetActive(false);

        m_objectBoost.SetActive(false);
    }


    public void StartCar()
    {
        this.GetComponent<Rigidbody>().isKinematic = false;
        ZAudioMgr.Instance.PlaySFXVolume(m_audioNormalEngine, 0.5f, true);

        m_objFakeBoost.gameObject.SetActive(false);

        m_objInGameBoost.gameObject.SetActive(true);
        m_objInGameTrailLeft.gameObject.SetActive(true);
        m_objInGameTrailRight.gameObject.SetActive(true);
    }

    public void SetCar(int p_carIdx)
    {
        foreach(GameObject car in m_listOfCars)
        {
            car.gameObject.SetActive(false);
        }
        m_currentCar = m_listOfCars[p_carIdx];
        m_currentCar.gameObject.SetActive(true);
    }



    private void OnCollisionStay(Collision collision)
    {
        if ((m_isBoost || m_boostInvincibleTime > 0) && collision.gameObject.GetComponent<Rigidbody>())
        {
            collision.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-1000, 1000), 5000, -0000));
            collision.gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(2000, 2000, 2000));
            GameScene.instance.AddCoins(1);
        } 
    }

    void OnCollisionEnter(Collision collision)
	{
        if(GameScene.instance.m_eState != GAME_STATE.IN_GAME)
            return;
        
		if ((m_isBoost || m_boostInvincibleTime > 0) && collision.gameObject.GetComponent<Rigidbody> ()) 
        {
            Vector3 spawnvec = new Vector3(collision.transform.localPosition.x, collision.transform.localPosition.y, collision.transform.localPosition.z);
            Debug.Log("BoostSpawnVec"+ spawnvec);

            collision.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-1000, 1000), 5000, -0000));
            collision.gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(2000, 2000, 2000));

            if (GameScene.instance.GetMode() == 1)
            {
                GameScene.instance.Score(-1);
            }
            else
            {
                GameScene.instance.Score(1);
            }			
            GameScene.instance.AddCoins (1);
            //Vector3 spawnvec = new Vector3(collision.transform.localPosition.x, collision.transform.localPosition.y, collision.transform.localPosition.z);

            //SpawnerManager.Instance.Spawn(OBJ_TYPE.COIN_HIT_EFFECT, collision.transform.localPosition);
            SpawnerManager.Instance.SpawnCoinParticle(collision.transform.localPosition);

            float impactVolume= Random.Range(0.15f, 0.3f);

            ZAudioMgr.Instance.PlaySFXVolume(m_audioCarHit,impactVolume);
		}
		else if (collision.gameObject.tag == "Enemy") 
        {

            if (this.gameObject.layer != LayerMask.NameToLayer("DeadCar"))
            {
                this.gameObject.layer = LayerMask.NameToLayer("DeadCar");
            }

            SpawnerManager.Instance.Spawn(OBJ_TYPE.EXPLOSION, collision.transform.localPosition);
            ZCameraMgr.instance.DoShake();
            ZAudioMgr.Instance.PlaySFX(m_audioExplosion);
            m_objInGameTrailLeft.gameObject.SetActive(false);
            m_objInGameTrailRight.gameObject.SetActive(false);

            GameScene.instance.ShowWhiteFlash();

            collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            collision.gameObject.GetComponent<Rigidbody>().useGravity = true;
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            collision.gameObject.GetComponent<Rigidbody>().isKinematic = false;

            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-500, 500), Random.Range(500, 1000), Random.Range(1000, 3000)));
            collision.gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-2500, 2500), Random.Range(-2500, 2500), Random.Range(-2500, 2500)));
            collision.gameObject.GetComponent<Renderer>().material.color = new Color(0.25f, 0.25f, 0.25f);
            m_objLastCarHit = collision.gameObject;

            ZAudioMgr.Instance.StopSFX(m_audioNormalEngine);
            m_objInGameBoost.gameObject.SetActive(false);
            m_currentCar.GetComponent<Renderer>().material.color = new Color(0.25f, 0.25f, 0.25f);

            if (ContinueManager.Instance.CanContinueGame())
            {
                GameScene.instance.m_eState = GAME_STATE.WATCH_TO_CONTINUE;
                ActivateRagdoll(true);

                ContinueManager.Instance.ShowButtons(true);
                m_objPhantomCar.transform.localPosition = this.transform.localPosition;
                m_objPhantomCar.gameObject.SetActive(true);

                GameScene.instance.WatchAdsToContinue();
            }
            else
            {
                ActivateRagdoll(false);
                GameScene.instance.m_eState = GAME_STATE.RESULTS;
                GameScene.instance.Die();
                m_carHolder.StopAnimation();
            }
		}
	}

    public void ActivateRagdoll(bool p_willContinueGame)
    {
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().useGravity = true;
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        this.GetComponent<Rigidbody>().isKinematic = false;

        if (p_willContinueGame)
        {
            this.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-500, 500), Random.Range(2000, 4500), Random.Range(2000, 6000)));
        }
        else
        {
            this.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-500, 500), Random.Range(1000, 3000), Random.Range(1000, 3000)));
        }
        this.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-2500, 2500), Random.Range(-2500, 2500), Random.Range(-2500, 2500)));

    }

    public void StartShowcase()
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.rotateAround(this.gameObject, new Vector3(0, 1, 0), 360, 5f).setLoopClamp();

        m_objInGameTrailLeft.gameObject.SetActive(false);
        m_objInGameTrailRight.gameObject.SetActive(false);
        m_objInGameBoost.gameObject.SetActive(false);

        this.transform.localPosition = Vector3.zero;
        this.GetComponent<Rigidbody>().isKinematic = true;

        m_objFakeBoost.gameObject.SetActive(false);
        m_carHolder.StopAnimation();
    }

    public void StopShowCase()
    {
        LeanTween.cancel(this.gameObject);
        m_objFakeBoost.gameObject.SetActive(true);
        m_carHolder.StartAnimation();
    }

    public void ReviveCar()
    {
       this.transform.localPosition = m_objPhantomCar.transform.localPosition;
        Reset();

        m_objFakeBoost.gameObject.SetActive(false);
        m_objInGameBoost.gameObject.SetActive(true);
        m_objInGameTrailLeft.gameObject.SetActive(true);
        m_objInGameTrailRight.gameObject.SetActive(true);
        m_objBoostModeBoost.gameObject.SetActive(false);
    }

	public void Reset()
	{
        this.gameObject.layer = LayerMask.NameToLayer("Default");
        m_objPhantomCar.gameObject.SetActive(false);
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.transform.localEulerAngles = Vector3.zero;

        m_currentCar.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f);
        if(m_objLastCarHit != null)
        {
            m_objLastCarHit.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f);
        }

        m_objCelebrateParticle.gameObject.SetActive(false);

        m_reachedFinishLine = false;
        //isAlive = true;

        ZAudioMgr.Instance.StopSFX(m_audioNormalEngine);
        m_objectBoost.gameObject.SetActive(false);

        m_carHolder.StartAnimation();


        if (GameScene.instance.GetMode() == 1)
        {
            m_objFakeBoost.gameObject.SetActive(false);
        }
        else
        {
            m_objFakeBoost.gameObject.SetActive(true);
        }

        m_objInGameBoost.gameObject.SetActive(false);
        m_objInGameTrailLeft.gameObject.SetActive(false);
        m_objInGameTrailRight.gameObject.SetActive(false);
        m_objBoostModeBoost.gameObject.SetActive(false);
	}

    private void LoadLevel()
    {
        GameScene.instance.SetupLevel();
    }

	private void OnTriggerEnter(Collider collision)
	{
        if (GameScene.instance.m_eState != GAME_STATE.IN_GAME)
            return;

		if (collision.gameObject.tag == "Safe") 
        {
            if(GameScene.instance.GetMode() == 1)
            {
                GameScene.instance.Score(-1);
            }
            else
            {
                GameScene.instance.Score(1);
            }
		}
        if(collision.gameObject.tag == "Coin")
        {
            collision.gameObject.SetActive(false);
            GameScene.instance.AddCoins(1);
            Vector3 spawnvec = new Vector3(collision.transform.localPosition.x, collision.transform.localPosition.y, collision.transform.localPosition.z);

            //SpawnerManager.Instance.Spawn(OBJ_TYPE.COIN_HIT_EFFECT, collision.transform.localPosition);
            SpawnerManager.Instance.SpawnCoinParticle(spawnvec);
        }
        if(collision.gameObject.tag == "FinishLine")
        {
            GameScene.instance.ShowWhiteFlash();
            //LeanTween.cancel(this.gameObject);
            m_reachedFinishLine = true;

           // m_objCelebrateParticle.transform.position = collision.transform.position;

            //int playerPrefs = PlayerPrefs.GetInt("BoostLevel") + 1;
            //PlayerPrefs.SetInt("BoostLevel", playerPrefs);
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 100);
            LeanTween.rotateY(this.gameObject, 230f, 0.3f).setEase(LeanTweenType.easeInSine);
            m_objCelebrateParticle.gameObject.SetActive(true);

            Invoke("AnimateWin", 0.3f);
            ZAudioMgr.Instance.StopSFX(m_audioNormalEngine);
            ZAudioMgr.Instance.PlaySFX(m_audioCelebrate);

            m_objInGameBoost.gameObject.SetActive(false);
            //m_objInGameTrailLeft.gameObject.SetActive(false);
            //m_objInGameTrailRight.gameObject.SetActive(false);

            BBLevelMode.instance.ReachedFinishLine();

           // Invoke("LoadLevel", 3);
        }
		if (collision.gameObject.tag == "Boost") 
        {
            if(m_isBoost)
            {
                m_boostTime = 6;
            }
            else
            {
                ActivateBoost();
            }

            GameScene.instance.ShowWhiteFlash();
            collision.gameObject.SetActive(false);
		}
	}

    private void AnimateWin()
    {
        //float posZ = BBCar.Instance.transform.localPosition.z - 100f;
        //LeanTween.moveLocalZ(this.gameObject, posZ, 0.5f);

        this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }
}
