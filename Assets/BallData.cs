﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BallData : MonoBehaviour 
{
	public string m_ballName;
	public string m_ballDescription;
	public BallMaterial m_physicMaterial;
	public int m_rarity;
	public List<AudioClip> m_audioBounce;	
	public float minVolumeRange;
	public float maxVolumeRange;
}
