﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FDShooterMode : FDGamesceneInstance {

	//public List<int> m_listOfRandomLevels = new List<int>();
	public List<Dictionary<string,object>> m_data 	= new List<Dictionary<string,object>>();

	public static FDShooterMode instance;
	public FDShootyEnum m_eState;

	public GameObject m_spawnPoint;

	// TODO Create a seperate arrow object handler
	public GameObject m_objArrowChild;
	public GameObject m_objArrowParent;

	public Text m_textScoreBanner;

	private int m_curPatternIdx;
	private List<string> m_listOfCurPattern;
	private List<string> m_listOfStartPattern = new List<string>()
	{
		"TUTS", "TUTS", "TUTS", "EASY", "MED", "EASY", "MED", "MED"
	};

	private List<string> m_listOfMedPattern = new List<string> () {
		"MED", "EASY", "MED", "EASY", "MED", "HARD", "MED", "HARD"
	};

	private List<string> m_listOfHardPattern = new List<string> () {
		"MED", "HARD", "HARD", "EASY", "CHOKE", "HARD", "EASY", "HARD"
	};

//	private List<string> m_listOfStartPattern02 = new List<string>()
//	{
//		"EASY", "EASY", "EASY", "EASY", "MED", "MED", "HARD", "MED"
//	};
//
//	private List<string> m_listOfStartPattern03 = new List<string>()
//	{
//		"EASY", "EASY", "MED", "EASY", "MED", "MED", "HARD", "MED"
//	};


//	private List<string> m_listOfMedPattern01 = new List<string>()
//	{
//		"MED", "MED", "HARD", "EASY", "MED", "MED", "HARD"
//	};
//
//	private List<string> m_listOfMedPattern02 = new List<string> () {
//
//	};
//
//	private List<string> m_listOfMedPattern03 = new List<string> () {
//
//	};
//
//	private List<string> 

	private List<Vector2> m_listOfShotAngle = new List<Vector2>()
	{
		new Vector2(1,1),			// NORMAL
		new Vector2(0.7f,1.2f),		// HIGH ANGLE
		new Vector2(2f, 0.9f) 		// LOW ANGLE
	};

	public Text m_textTries;

	[SerializeField]
	private SBBall m_curBall;
	private bool m_gameStarted;
	private string m_prefabBallName;
	private float m_currentDeltaTime;
	private float m_animMovementSpeed;
	private float m_currentSensitivity;
	private Vector2 m_currentShotStrength;

	void Awake () 
	{
		instance = this;
	}

	public override void Gamescene_Update ()
	{
		Controls();
	}

	void Controls()
	{
		if (IsButtonPressed()) 
		{
			return;
		}

		if (Input.GetMouseButtonUp (0) && m_eState == FDShootyEnum.CHARGE) 
		{
			float timeDiff = Time.time - m_currentDeltaTime;
			Vector2 shotStrength = new Vector2 (m_currentSensitivity * m_currentShotStrength.x * 45000 * timeDiff, 
												m_currentSensitivity * m_currentShotStrength.y * 160000 * timeDiff);
			if (m_curBall != null) {
				m_curBall.ShootBall (shotStrength);
			}
			m_eState = FDShootyEnum.SHOOT;
			m_objArrowParent.gameObject.SetActive (false);
		} 
		else if (Input.GetMouseButtonDown (0) && m_eState == FDShootyEnum.IDLE) 
		{
			if (m_curBall != null) {
				m_curBall.AnimateBall (m_currentSensitivity);
			}
			m_currentDeltaTime = Time.time;
			m_eState = FDShootyEnum.CHARGE;
		}
	}

	public override void Gamescene_ButtonUp ()
	{
		GameStart ();
	}

	public override void Gamescene_Score ()
	{ 	
		UpdateAttempts (0);
		GameScene.instance.m_textTries2.text = "";
		GameScene.instance.PostScoreToLeaderboards ();
		BoardManager.Instance.AnimateNet ();
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, new Vector3 (this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z), 1f).setOnComplete (DelayLoadLevel);
	}

	public override void Gamescene_Die ()
	{
	}


	public override void Gamescene_Setuplevel(bool hardreset) 
	{
		BoardManager.Instance.transform.position = new Vector3 (123, 54, 2);
		BoardManager.Instance.transform.eulerAngles = new Vector3 (0, 270, 0);
		BoardManager.Instance.transform.localScale = new Vector3 (110, 110, 110);
		BoardManager.Instance.EnableBoard();
		UpdateCamera (CameraStats);
		m_curPatternIdx = 0;
		GetNewPattern (true);
		LoadLevel ();

		m_objArrowParent.gameObject.SetActive (true);

		//BoardManager.Instance.transform.localScale = new Vector3 (90, 90, 90);

		GetNewBall ();
		m_gameStarted = false;
		m_objArrowParent.transform.position = m_curBall.transform.position;
		m_textTries.gameObject.SetActive (true);

		m_eState = FDShootyEnum.IDLE;
	}

	public override void Gamescene_Unloadlevel() 
	{
		this.gameObject.SetActive (false);
		LeanTween.cancel (this.gameObject);
		m_textTries.gameObject.SetActive (false);
		if (m_curBall != null) {
			m_curBall.ResetBall ();
		}
	}

	private void DelayLoadLevel()
	{
		ZObjectMgr.Instance.ResetAll ();
		m_eState = FDShootyEnum.IDLE;
		m_objArrowParent.gameObject.SetActive (true);
		LoadLevel ();
		GetNewBall ();
	}

	private void GetNewPattern(bool p_isStartOfGame = false)
	{
		m_curPatternIdx = 0;
		int score = (p_isStartOfGame) ? 0 : GameScene.instance.GetScore ();

		if (score < 7) {
			m_listOfCurPattern = m_listOfStartPattern;
		} else if (score < 14) {
			m_listOfCurPattern = m_listOfMedPattern;
		} else if (score < 21) {
			m_listOfCurPattern = m_listOfHardPattern;
		}
	}

	private void GetNewBall()
	{
		m_prefabBallName = GameScene.instance.m_prefabBall.name;
		GameObject objBall = ZObjectMgr.Instance.Spawn2D (m_prefabBallName, m_spawnPoint.transform.position);
		m_curBall = objBall.GetComponent<SBBall>();
		int currentBall = GameScene.instance.GetCurrentSelectedBird ();
		LeanTween.cancel (m_curBall.gameObject);
		m_curBall.ResetBall ();
		m_curBall.EnableBall (currentBall);
	}

	private void LoadLevel()
	{
		if (m_data.Count <= 0) 
		{
			m_data = CSVReaderPlus.Read ("shooter_data");
		}

		int levelToLoad = 0;
		string curDiff = m_listOfCurPattern [m_curPatternIdx];

		if (curDiff == "TUTS") {
			levelToLoad = Random.Range (0, 6);
		} else if (curDiff == "EASY") {
			levelToLoad = Random.Range (7, 15);
		} else if (curDiff == "MED") {
			levelToLoad = Random.Range (16, 30);
		} else if (curDiff == "HARD") {
			levelToLoad = Random.Range (31, 45);
		}

		// BoardPosition
		float BoardYPos = float.Parse("" + m_data [levelToLoad] ["BoardY"]);
		//	if (p_newLevel) 
		{
			BoardManager.Instance.transform.localPosition = new Vector3 (123, BoardYPos, BoardManager.Instance.transform.localPosition.z);
		}

		float XPos = float.Parse("" + m_data [levelToLoad] ["BallX"]);
		m_spawnPoint.transform.localPosition = new Vector3(XPos, m_spawnPoint.transform.localPosition.y, m_spawnPoint.transform.localPosition.z);
		m_objArrowParent.transform.localPosition = new Vector3(XPos, m_objArrowParent.transform.localPosition.y, m_objArrowParent.transform.localPosition.z); 

		// BoardSize
		float boardSize = int.Parse("" + m_data [levelToLoad] ["BoardSize"]);
		UpdateBoardScale (boardSize);

		// Sensitivity;
		float Sensitivity = float.Parse("" + m_data[levelToLoad]["Sensitivity"]);
		SetArrowSensitivityIndicator (Sensitivity);

		// ANGLE
		string Angle = "" + m_data [levelToLoad] ["Angle"];
		SetArrowAngle (Angle);

		// MOVEMENT SPEED 
		string movementSpeedStr = "" + m_data [levelToLoad] ["MovementSpeed"];
		m_animMovementSpeed = float.Parse (movementSpeedStr);

		// IS MOVING
		string isMovingDir = "" + m_data [levelToLoad] ["IsMoving"];
		BoardManager.Instance.AnimateBoard (isMovingDir, m_animMovementSpeed);
		//MakeBoardMove (isMovingDir, m_hasChanged);

		// IS BOARDLESS
		string isBoardlessStr = "" + m_data[levelToLoad]["Boardless"];
		bool isBoardless = (isBoardlessStr == "TRUE") ? true : false;
		BoardManager.Instance.IsBoardless (isBoardless);

		if (m_curPatternIdx > 8) {
			GetNewPattern ();
		} else {
			m_curPatternIdx++;
		}
	}

	private void SetArrowAngle(string p_angle)
	{
		switch (p_angle) {
		case "Normal":
			{
				m_objArrowParent.transform.localEulerAngles = new Vector3 (0, 0, -20);
				m_currentShotStrength = m_listOfShotAngle[0];
			}
			break;

		case "High Angle":
			{
				m_objArrowParent.transform.localEulerAngles = new Vector3 (0, 0, -10);
				m_currentShotStrength = m_listOfShotAngle [1];
			}
			break;

		case "Low Angle":
			{
				m_objArrowParent.transform.localEulerAngles = new Vector3 (0, 0, -55);
				m_currentShotStrength = m_listOfShotAngle[2];
			}
			break;
		}
	}

	private void SetArrowSensitivityIndicator(float p_sensitivity)
	{
		m_currentSensitivity = p_sensitivity;

		if (m_currentSensitivity < 1) 
		{
			m_objArrowChild.GetComponent<SpriteRenderer> ().color = Color.red;		
		} else if (m_currentSensitivity == 1) {
			m_objArrowChild.GetComponent<SpriteRenderer> ().color = Color.yellow;
		} else if (m_currentSensitivity > 1) {
			m_objArrowChild.GetComponent<SpriteRenderer> ().color = Color.green;
		}
	}

	private void UpdateBoardScale(float p_scale)
	{
		BoardManager.Instance.transform.localScale = new Vector3 (p_scale, p_scale, p_scale);
	}

	private void GameStart()
	{
		if (!m_gameStarted) 
		{
			m_gameStarted = true;
			GameScene.instance.m_currentScore = 0;
			GameScene.instance.m_textScoreShadow.text = "" + GameScene.instance.m_currentScore.ToString ("00");
			m_textScoreBanner.text = "SCORE";
		}
	}
}