﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBCarClearObstacles : MonoBehaviour {

	public void OnTriggerEnter2D(Collider2D p_other)
	{
		if (p_other.gameObject.tag == "Enemy") 
		{
			p_other.gameObject.SetActive (false);
		}
	}
}
