﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModesEntry : MonoBehaviour 
{
	public string m_strSaveFile;
	public Text m_textBestScore;
	public GameObject m_objectImage;

	public void Initialize()
	{
		int loadedScore = PlayerPrefs.GetInt (m_strSaveFile);
		m_textBestScore.text = "BEST " + loadedScore;
		LeanTween.cancel (this.gameObject);
	}

	public void HighlightMode(Color p_color)
	{
		m_objectImage.GetComponent<Image> ().color = p_color;
	}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
