﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZCrossPromoDemo : MonoBehaviour {

	List<AppData> m_listOfAppData = new List<AppData>();
	public Text m_textInstalledApps;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowInstalledApps()
	{
		ZCrossPromo.Instance.RequestGameList ();
		ZCrossPromo.Instance.onRequestReceived -= RequestRecieved;
		ZCrossPromo.Instance.onRequestReceived += RequestRecieved;
		m_textInstalledApps.text = "";

	}

	private void RequestRecieved(REQUEST_TYPE p_type)
	{
		switch (p_type) {
		case REQUEST_TYPE.GET_ALL_GAMES:
			foreach (AppData game in ZCrossPromo.Instance.m_listOfAllGames) 
			{
				m_textInstalledApps.text += "" + game.app_name + " : " + game.isAppInstalled + "\n"; 
			}
			break;

		case REQUEST_TYPE.GET_CROSS_PROMO:
	
			break;
		}
	}
}
