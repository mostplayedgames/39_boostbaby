﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPromoShake : MonoBehaviour {

	// Use this for initialization
	void Awake () {

		StartShake ();

	}

	private void StartShake()
	{
		LeanTween.cancel (this.gameObject);
		LeanTween.rotateZ (this.gameObject, 0, 0.1f).setLoopPingPong ().setLoopCount (4).setOnComplete (ShakeDelay);
	}

	private void ShakeDelay()
	{
		LeanTween.cancel (this.gameObject);
		LeanTween.scaleX (this.gameObject, this.transform.localScale.x, 1).setOnComplete(StartShake);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
