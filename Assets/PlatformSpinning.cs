﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpinning : MonoBehaviour 
{
	public GameObject m_objectToSpin;
	public Vector3 m_vectorReset;

	// Use this for initialization
	void OnEnable () 
	{
//		LeanTween.reset ();
//		Rotate1 ();
	}

	public void MakeSpin()
	{
		Rotate180 ();
	}

	private void Rotate180()
	{
		//LeanTween.rotateZ (m_targetPlatform.gameObject, 350, 1.0f).setLoopClamp().setOnComplete (Pause1);
		LeanTween.rotateLocal (this.gameObject, new Vector3 (0, 0, 180), 1.0f).setOnComplete (Pause1);

		//LeanTween.rotateAroundLocal (m_targetPlatform.gameObject, new Vector3 (0, 0, 1), 360, 1.0f).setOnComplete (Pause1);
		//LeanTween.rotateZ (m_targetPlatform.gameObject, 180, 1.0f).setOnComplete(Pause1);
	}

	private void Pause1()
	{
		LeanTween.delayedCall (1.0f, Rotate360);
	}

	private void Rotate360()
	{
		// .setEase(LeanTweenType.easeInBack)
		//LeanTween.rotateLocal (m_targetPlatform.gameObject, new Vector3 (0, 0, 360), 1.0f).setOnComplete (Pause2);
		//LeanTween.rotateAroundLocal (m_targetPlatform.gameObject, new Vector3 (0, 0, 1), 360, 1.0f).setOnComplete (Pause1);
		LeanTween.rotateLocal (this.gameObject, new Vector3 (0, 0, 360), 1.0f).setOnComplete (Pause2);

		//LeanTween.rotateZ (m_targetPlatform.gameObject, 0, 1.0f).setOnComplete(Pause2);
	}

	private void Pause2()
	{
		LeanTween.delayedCall (1.0f, Rotate180);
	}

}
