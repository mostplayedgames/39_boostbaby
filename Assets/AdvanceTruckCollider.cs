﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvanceTruckCollider : MonoBehaviour {

    public MovingCar m_obTruck;

	public void OnTriggerEnter(Collider p_other)
	{
		if (p_other.gameObject.tag == "Player") 
		{
			//Debug.Log ("AdvanceColliderHit");
			m_obTruck.MoveTruck (); 
		}
	}
}
