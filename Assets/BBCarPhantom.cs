﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBCarPhantom : MonoBehaviour {

	public List<GameObject> m_listOfPhantomCars;
	//public GameObject m_objPhantomCar;
	private int m_curCarSelected;

	public void OnEnable()
	{
		ShowPhantomCar ();
	}

	public void OnDisable()
	{

	}

	public void ShowPhantomCar()
	{
		m_curCarSelected = GameScene.instance.GetCurrentSelectedBird ();
		foreach (GameObject obj in m_listOfPhantomCars) {
			obj.gameObject.SetActive (false);
		}

		m_listOfPhantomCars[m_curCarSelected].gameObject.SetActive (true);
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 0.5f).setOnComplete (HidePhantomCar);
	}

	public void HidePhantomCar()
	{
		m_listOfPhantomCars[m_curCarSelected].gameObject.SetActive (false);
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 0.5f).setOnComplete (ShowPhantomCar);
	}
}
