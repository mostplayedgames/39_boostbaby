﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarHiglight : MonoBehaviour {

    public Texture m_normalTexture;
    public Texture m_highlightTexture;
    public Renderer m_renderer;
	
    public void Highlight()
    {
        m_renderer.sharedMaterial.mainTexture = m_highlightTexture;
    }

    public void Reset()
    {
        m_renderer.sharedMaterial.mainTexture = m_normalTexture;
    }
}
